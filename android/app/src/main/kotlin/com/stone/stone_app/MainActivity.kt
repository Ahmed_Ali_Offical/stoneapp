package com.stone.stone_app

import android.graphics.Typeface
import android.util.Log
import androidx.annotation.NonNull
import company.tap.gosellapi.GoSellSDK
import company.tap.gosellapi.internal.api.callbacks.GoSellError
import company.tap.gosellapi.internal.api.models.Authorize
import company.tap.gosellapi.internal.api.models.Charge
import company.tap.gosellapi.internal.api.models.Token
import company.tap.gosellapi.open.controllers.SDKSession
import company.tap.gosellapi.open.controllers.ThemeObject
import company.tap.gosellapi.open.delegate.SessionDelegate
import company.tap.gosellapi.open.enums.AppearanceMode
import company.tap.gosellapi.open.enums.CardType
import company.tap.gosellapi.open.models.*
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import java.math.BigDecimal


class MainActivity: FlutterActivity(),SessionDelegate {

    private lateinit var sdkSession: SDKSession
    private val CHANNEL = "flutter.native/helper"

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
//        super.configureFlutterEngine(flutterEngine)
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler {
            call, result ->
            if (call.method.equals("helloFromNativeCode")) {
                val greetings: String = "Hello From Native"
                startSDK()
                result.success(greetings)
            }
        }
    }

    override fun savedCardsList(cardsList: CardsList) {
        TODO("Not yet implemented")
    }

    override fun cardSavingFailed(charge: Charge) {
        TODO("Not yet implemented")
    }

    override fun backendUnknownError(message: String?) {
        TODO("Not yet implemented")
    }

    override fun userEnabledSaveCardOption(saveCardEnabled: Boolean) {
        TODO("Not yet implemented")
    }

    override fun sessionHasStarted() {
        TODO("Not yet implemented")
    }

    override fun sessionCancelled() {
        Log.i("Session","sessionCancelled")
    }

    override fun sessionIsStarting() {
        Log.i("Session","sessionIsStarting")
    }

    override fun invalidCardDetails() {
        Log.i("Session","invalidCardDetails")
    }

    override fun cardSaved(charge: Charge) {
        Log.i("Session","cardSaved")
    }

    override fun paymentSucceed(charge: Charge) {
        Log.i("Session","paymentSucceed")
    }

    override fun authorizationFailed(authorize: Authorize?) {
        Log.i("Session","authorizationFailed")
    }

    override fun cardTokenizedSuccessfully(token: Token) {
        Log.i("Session","cardTokenizedSuccessfully")
    }

    override fun authorizationSucceed(authorize: Authorize) {
        Log.i("Session","authorizationSucceed")
    }

    override fun invalidTransactionMode() {
        Log.i("Session","invalidTransactionMode")
    }

    override fun sdkError(goSellError: GoSellError?) {
        Log.e("Session",goSellError?.errorBody)
    }

    override fun sessionFailedToStart() {
        Log.i("Session","sessionFailedToStart")
    }

    override fun paymentFailed(charge: Charge?) {
        Log.i("Session","paymentFailed")
    }

    override fun invalidCustomerID() {
        Log.i("Session","invalidCustomerID")
    }

    /**
     * Integrating SDK.
     */
    private fun startSDK() {
        /**
         * Required step.
         * Configure SDK with your Secret API key and App Bundle name registered with tap company.
         */
        configureApp()
        /**
         * Optional step
         * Here you can configure your app theme (Look and Feel).
         */
        configureSDKThemeObject()
        /**
         * Required step.
         * Configure SDK Session with all required data.
         */
        configureSDKSession()
        /**
         * Required step.
         * Choose between different SDK modes
         */
//        configureSDKMode()
        /**
         * If you included Tap Pay Button then configure it first, if not then ignore this step.
         */
//        initPayButton()
    }

    private fun configureApp() {
        GoSellSDK.init(this, "pk_test_IQ8Lrpv2heowqdcbUNXPk9DG", "com.almusand.stone_app") // to be replaced by merchant, you can contact tap support team to get you credentials
        GoSellSDK.setLocale("ar") //  if you dont pass locale then default locale EN will be used
    }

    private fun configureSDKThemeObject() {
        ThemeObject.getInstance() // set Appearance mode [Full Screen Mode - Windowed Mode]
                .setAppearanceMode(AppearanceMode.WINDOWED_MODE) // **Required**
                .setSdkLanguage("ar") //if you dont pass locale then default locale EN will be used
                // Setup header font type face **Make sure that you already have asset folder with required fonts**
//                .setHeaderFont(Typeface.createFromAsset(assets, "fonts/roboto_light.ttf")) //**Optional**
                //Setup header text color
                .setHeaderTextColor(resources.getColor(R.color.black1)) // **Optional**
                // Setup header text size
                .setHeaderTextSize(17) // **Optional**
                // setup header background
                .setHeaderBackgroundColor(resources.getColor(R.color.french_gray_new)) //**Optional**
                // setup card form input font type
//                .setCardInputFont(Typeface.createFromAsset(assets, "fonts/roboto_light.ttf")) //**Optional**
                // setup card input field text color
                .setCardInputTextColor(resources.getColor(R.color.black)) //**Optional**
                // setup card input field text color in case of invalid input
                .setCardInputInvalidTextColor(resources.getColor(R.color.red)) //**Optional**
                // setup card input hint text color
                .setCardInputPlaceholderTextColor(resources.getColor(R.color.black)) //**Optional**
                // setup Switch button Thumb Tint Color in case of Off State
                .setSaveCardSwitchOffThumbTint(resources.getColor(R.color.gray)) // **Optional**
                // setup Switch button Thumb Tint Color in case of On State
                .setSaveCardSwitchOnThumbTint(resources.getColor(R.color.vibrant_green)) // **Optional**
                // setup Switch button Track Tint Color in case of Off State
                .setSaveCardSwitchOffTrackTint(resources.getColor(R.color.gray)) // **Optional**
                // setup Switch button Track Tint Color in case of On State
                .setSaveCardSwitchOnTrackTint(resources.getColor(android.R.color.holo_green_dark)) // **Optional**
                // change scan icon
                .setScanIconDrawable(resources.getDrawable(R.drawable.btn_card_scanner_normal)) // **Optional**
                // setup pay button selector [ background - round corner ]
                .setPayButtonResourceId(R.drawable.btn_pay_selector) // setup pay button font type face
//                .setPayButtonFont(Typeface.createFromAsset(assets, "fonts/roboto_light.ttf")) // **Optional**
                // setup pay button disable title color
                .setPayButtonDisabledTitleColor(resources.getColor(R.color.black)) // **Optional**
                // setup pay button enable title color
                .setPayButtonEnabledTitleColor(resources.getColor(android.R.color.white)) // **Optional**
                //setup pay button text size
                .setPayButtonTextSize(14) // **Optional**
                // show/hide pay button loader
                .setPayButtonLoaderVisible(true) // **Optional**
                // show/hide pay button security icon
                .setPayButtonSecurityIconVisible(true) // **Optional**
                // setup dialog textcolor and textsize
                .setDialogTextColor(resources.getColor(R.color.black1)).dialogTextSize = 17 // **Optional**
    }

    private fun configureSDKSession() {

        // Instantiate SDK Session
        sdkSession = SDKSession() //** Required **

        // pass your activity as a session delegate to listen to SDK internal payment process follow
        sdkSession.addSessionDelegate(this) //** Required **

        // initiate PaymentDataSource
        sdkSession.instantiatePaymentDataSource() //** Required **

        // set transaction currency associated to your account
        sdkSession.setTransactionCurrency(TapCurrency("KWD")) //** Required **

        // Using static CustomerBuilder method available inside TAP Customer Class you can populate TAP Customer object and pass it to SDK
        sdkSession.setCustomer(getCustomer()) //** Required **

        // Set Total Amount. The Total amount will be recalculated according to provided Taxes and Shipping
        sdkSession.setAmount(BigDecimal(40)) //** Required **

        // Set Payment Items array list
        sdkSession.setPaymentItems(ArrayList<PaymentItem>()) // ** Optional ** you can pass empty array list

        // Set Taxes array list
        sdkSession.setTaxes(ArrayList<Tax>()) // ** Optional ** you can pass empty array list

        // Set Shipping array list
        sdkSession.setShipping(ArrayList()) // ** Optional ** you can pass empty array list

        // Post URL
        sdkSession.setPostURL("") // ** Optional **

        // Payment Description
        sdkSession.setPaymentDescription("") //** Optional **

        // Payment Extra Info
        sdkSession.setPaymentMetadata(HashMap()) // ** Optional ** you can pass empty array hash map

        // Payment Reference
        sdkSession.setPaymentReference(null) // ** Optional ** you can pass null

        // Payment Statement Descriptor
        sdkSession.setPaymentStatementDescriptor("") // ** Optional **

        // Enable or Disable Saving Card
        sdkSession.isUserAllowedToSaveCard(true) //  ** Required ** you can pass boolean

        // Enable or Disable 3DSecure
        sdkSession.isRequires3DSecure(true)

        //Set Receipt Settings [SMS - Email ]
        sdkSession.setReceiptSettings(null) // ** Optional ** you can pass Receipt object or null

        // Set Authorize Action
        sdkSession.setAuthorizeAction(null) // ** Optional ** you can pass AuthorizeAction object or null
        sdkSession.setDestination(null) // ** Optional ** you can pass Destinations object or null
        sdkSession.setMerchantID(null) // ** Optional ** you can pass merchant id or null
        sdkSession.setPaymentType("CARD") //** Merchant can customize payment options [WEB/CARD] for each transaction or it will show all payment options granted to him.
        sdkSession.setCardType(CardType.CREDIT) // ** Optional ** you can pass which cardType[CREDIT/DEBIT] you want.By default it loads all available cards for Merchant.
        /**
         * Use this method where ever you want to show TAP SDK Main Screen.
         * This method must be called after you configured SDK as above
         * This method will be used in case of you are not using TAP PayButton in your activity.
         */
        sdkSession.start(this)
    }

    private fun configureSDKMode() {
        /**
         * You have to choose only one Mode of the following modes:
         * Note:-
         * - In case of using PayButton, then don't call sdkSession.start(this); because the SDK will start when user clicks the tap pay button.
         */
        //////////////////////////////////////////////////////    SDK with UI //////////////////////
        /**
         * 1- Start using  SDK features through SDK main activity (With Tap CARD FORM)
         */
//        startSDKWithUI()
    }

    private fun getCustomer(): Customer? {
        return Customer.CustomerBuilder(null).firstName("Test").email("a@a.a").build()
    }
}
