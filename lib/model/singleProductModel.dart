class SingleProductModel {
  bool success;
  Data data;
  String message;

  SingleProductModel({this.success, this.data, this.message});

  SingleProductModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Data {
  int id;
  Title title;
  String itemNo;
  String photo;
  Title content;
  int priceAfterDis;
  int price;
  String stock;
  String status;
  int categoryId;
  String createdAt;
  String updatedAt;
  List<_Colors> colors;
  List<Sizes> sizes;
  Category category;

  Data(
      {this.id,
        this.title,
        this.itemNo,
        this.photo,
        this.content,
        this.priceAfterDis,
        this.price,
        this.stock,
        this.status,
        this.categoryId,
        this.createdAt,
        this.updatedAt,
        this.colors,
        this.sizes,
        this.category});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'] != null ? new Title.fromJson(json['title']) : null;
    itemNo = json['item_no'];
    photo = json['photo'];
    content =
    json['content'] != null ? new Title.fromJson(json['content']) : null;
    priceAfterDis = json['price_after_dis'];
    price = json['price'];
    stock = json['stock'];
    status = json['status'];
    categoryId = json['category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['colors'] != null) {
      colors = new List<_Colors>();
      json['colors'].forEach((v) {
        colors.add(new _Colors.fromJson(v));
      });
    }
    if (json['sizes'] != null) {
      sizes = new List<Sizes>();
      json['sizes'].forEach((v) {
        sizes.add(new Sizes.fromJson(v));
      });
    }
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.title != null) {
      data['title'] = this.title.toJson();
    }
    data['item_no'] = this.itemNo;
    data['photo'] = this.photo;
    if (this.content != null) {
      data['content'] = this.content.toJson();
    }
    data['price_after_dis'] = this.priceAfterDis;
    data['price'] = this.price;
    data['stock'] = this.stock;
    data['status'] = this.status;
    data['category_id'] = this.categoryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.colors != null) {
      data['colors'] = this.colors.map((v) => v.toJson()).toList();
    }
    if (this.sizes != null) {
      data['sizes'] = this.sizes.map((v) => v.toJson()).toList();
    }
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    return data;
  }
}

class Title {
  String ar;
  String en;

  Title({this.ar, this.en});

  Title.fromJson(Map<String, dynamic> json) {
    ar = json['ar'];
    en = json['en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ar'] = this.ar;
    data['en'] = this.en;
    return data;
  }
}

class _Colors {
  int id;
  Title name;
  String image;
  int categoryId;
  String createdAt;
  String updatedAt;
  Pivot pivot;

  _Colors(
      {this.id,
        this.name,
        this.image,
        this.categoryId,
        this.createdAt,
        this.updatedAt,
        this.pivot});

  _Colors.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'] != null ? new Title.fromJson(json['name']) : null;
    image = json['image'];
    categoryId = json['category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['image'] = this.image;
    data['category_id'] = this.categoryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.pivot != null) {
      data['pivot'] = this.pivot.toJson();
    }
    return data;
  }
}

class Pivot {
  String productId;
  String colorId;

  Pivot({this.productId, this.colorId});

  Pivot.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
    colorId = json['color_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_id'] = this.productId;
    data['color_id'] = this.colorId;
    return data;
  }
}

class Sizes {
  int id;
  double length;
  double width;
  String createdAt;
  String updatedAt;
  String totalArea;
  _Pivot pivot;

  Sizes(
      {this.id,
        this.length,
        this.width,
        this.createdAt,
        this.updatedAt,
        this.totalArea,
        this.pivot});

  Sizes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    length = json['length'];
    width = json['width'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    totalArea = json['total_area'];
    pivot = json['pivot'] != null ? new _Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['length'] = this.length;
    data['width'] = this.width;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['total_area'] = this.totalArea;
    if (this.pivot != null) {
      data['pivot'] = this.pivot.toJson();
    }
    return data;
  }
}

class _Pivot {
  String productId;
  String sizeId;

  _Pivot({this.productId, this.sizeId});

  _Pivot.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
    sizeId = json['size_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_id'] = this.productId;
    data['size_id'] = this.sizeId;
    return data;
  }
}

class Category {
  int id;
  Title name;
  String image;
  int parentId;
  String createdAt;
  String updatedAt;

  Category(
      {this.id,
        this.name,
        this.image,
        this.parentId,
        this.createdAt,
        this.updatedAt});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'] != null ? new Title.fromJson(json['name']) : null;
    image = json['image'];
    parentId = json['parent_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['image'] = this.image;
    data['parent_id'] = this.parentId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
