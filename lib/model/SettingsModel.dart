class SettingsModel {
  bool success;
  Data data;
  String message;

  SettingsModel({this.success, this.data, this.message});

  SettingsModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Data {
  String contactPhone;
  String contactAddress;
  String facebook;
  String twitter;
  String instagram;
  String contactEmail;
  String introEn;
  String introAr;
  String intro2En;
  String intro2Ar;
  String intro3En;
  String intro3Ar;
  String terms;
  String privacyEn;
  String privacyAr;

  Data(
      {this.contactPhone,
        this.contactAddress,
        this.facebook,
        this.twitter,
        this.instagram,
        this.contactEmail,
        this.introEn,
        this.introAr,
        this.intro2En,
        this.intro2Ar,
        this.intro3En,
        this.intro3Ar,
        this.terms,
        this.privacyEn,
        this.privacyAr});

  Data.fromJson(Map<String, dynamic> json) {
    contactPhone = json['contact_phone'];
    contactAddress = json['contact_address'];
    facebook = json['facebook'];
    twitter = json['twitter'];
    instagram = json['instagram'];
    contactEmail = json['contact_email'];
    introEn = json['intro_en'];
    introAr = json['intro_ar'];
    intro2En = json['intro_2_en'];
    intro2Ar = json['intro_2_ar'];
    intro3En = json['intro_3_en'];
    intro3Ar = json['intro_3_ar'];
    terms = json['terms'];
    privacyEn = json['privacy_en'];
    privacyAr = json['privacy_ar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['contact_phone'] = this.contactPhone;
    data['contact_address'] = this.contactAddress;
    data['facebook'] = this.facebook;
    data['twitter'] = this.twitter;
    data['instagram'] = this.instagram;
    data['contact_email'] = this.contactEmail;
    data['intro_en'] = this.introEn;
    data['intro_ar'] = this.introAr;
    data['intro_2_en'] = this.intro2En;
    data['intro_2_ar'] = this.intro2Ar;
    data['intro_3_en'] = this.intro3En;
    data['intro_3_ar'] = this.intro3Ar;
    data['terms'] = this.terms;
    data['privacy_en'] = this.privacyEn;
    data['privacy_ar'] = this.privacyAr;
    return data;
  }
}
