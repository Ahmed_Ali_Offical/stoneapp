class ProductModel {
  int _id;
  Title _title;
  String _itemNo;
  String _photo;
  Title _content;
  var _priceAfterDis;
  var _price;
  String _stock;
  String _status;
  int _categoryId;
  String _createdAt;
  String _updatedAt;

  ProductModel(
      {int id,
        Title title,
        String itemNo,
        String photo,
        Title content,
        var priceAfterDis,
        var price,
        String stock,
        String status,
        int categoryId,
        String createdAt,
        String updatedAt}) {
    this._id = id;
    this._title = title;
    this._itemNo = itemNo;
    this._photo = photo;
    this._content = content;
    this._priceAfterDis = priceAfterDis;
    this._price = price;
    this._stock = stock;
    this._status = status;
    this._categoryId = categoryId;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  Title get title => _title;
  set title(Title title) => _title = title;
  String get itemNo => _itemNo;
  set itemNo(String itemNo) => _itemNo = itemNo;
  String get photo => _photo;
  set photo(String photo) => _photo = photo;
  Title get content => _content;
  set content(Title content) => _content = content;
  double get priceAfterDis => _priceAfterDis;
  set priceAfterDis(double priceAfterDis) => _priceAfterDis = priceAfterDis;
  int get price => _price;
  set price(int price) => _price = price;
  String get stock => _stock;
  set stock(String stock) => _stock = stock;
  String get status => _status;
  set status(String status) => _status = status;
  int get categoryId => _categoryId;
  set categoryId(int categoryId) => _categoryId = categoryId;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  ProductModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'] != null ? new Title.fromJson(json['title']) : null;
    _itemNo = json['item_no'];
    _photo = json['photo'];
    _content =
    json['content'] != null ? new Title.fromJson(json['content']) : null;
    _priceAfterDis = json['price_after_dis'];
    _price = json['price'];
    _stock = json['stock'];
    _status = json['status'];
    _categoryId = json['category_id'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    if (this._title != null) {
      data['title'] = this._title.toJson();
    }
    data['item_no'] = this._itemNo;
    data['photo'] = this._photo;
    if (this._content != null) {
      data['content'] = this._content.toJson();
    }
    data['price_after_dis'] = this._priceAfterDis;
    data['price'] = this._price;
    data['stock'] = this._stock;
    data['status'] = this._status;
    data['category_id'] = this._categoryId;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}

class Title {
  String _ar;
  String _en;

  Title({String ar, String en}) {
    this._ar = ar;
    this._en = en;
  }

  String get ar => _ar;
  set ar(String ar) => _ar = ar;
  String get en => _en;
  set en(String en) => _en = en;

  Title.fromJson(Map<String, dynamic> json) {
    _ar = json['ar'];
    _en = json['en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ar'] = this._ar;
    data['en'] = this._en;
    return data;
  }
}
