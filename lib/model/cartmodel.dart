class CartModel {
  List<Inner> _inner;

  CartModel({List<Inner> inner}) {
    this._inner = inner;
  }

  List<Inner> get inner => _inner;

  set inner(List<Inner> inner) => _inner = inner;

  CartModel.fromJson(Map<String, dynamic> json) {
    if (json['inner'] != null) {
      _inner = new List<Inner>();
      json['inner'].forEach((v) {
        _inner.add(new Inner.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._inner != null) {
      data['inner'] = this._inner.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Inner {
  int _id;
  String _image;
  String _name;
  String _desc;
  String _width;
  String _height;
  String _area;
  var _price;
  int _count;

  Inner(
      int id,
      String image,
      String name,
      String desc,
      String width,
      String height,
      String area,
      var price,
      int count) {
    this._id = id;
    this._image = image;
    this._name = name;
    this._desc = desc;
    this._width = width;
    this._height = height;
    this._area = area;
    this._price = price;
    this._count = count;
  }

  int get id => _id;

  set id(int id) => _id = id;

  String get image => _image;

  set image(String image) => _image = image;

  String get name => _name;

  set name(String name) => _name = name;

  String get desc => _desc;

  set desc(String desc) => _desc = desc;


  String get width => _width;

  set width(String value) {
    _width = value;
  }

  double get price => double.parse(_price.toString());

  set price(double price) => _price = price;

  int get count => _count;

  set count(int count) => _count = count;

  Inner.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _image = json['image'];
    _name = json['name'];
    _desc = json['desc'];
    _width = json['width'];
    _height = json['height'];
    _area = json['area'];
    _price = json['price'];
    _count = json['count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['image'] = this._image;
    data['name'] = this._name;
    data['desc'] = this._desc;
    data['width'] = this._width;
    data['height'] = this._height;
    data['area'] = this._area;
    data['price'] = this._price;
    data['count'] = this._count;
    return data;
  }

  String get height => _height;

  set height(String value) {
    _height = value;
  }

  String get area => _area;

  set area(String value) {
    _area = value;
  }
}
