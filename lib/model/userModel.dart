class UserModel {
  _User user;
  List<UserAddress> userAddress;

  UserModel({this.user, this.userAddress});

  UserModel.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new _User.fromJson(json['user']) : null;
    if (json['user_address'] != null) {
      userAddress = new List<UserAddress>();
      json['user_address'].forEach((v) {
        userAddress.add(new UserAddress.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.userAddress != null) {
      data['user_address'] = this.userAddress.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class _User {
  int id;
  String name;
  String email;
  String phone;
  String companyName;
  String commercial;
  String identity;
  String license;
  String certificate;
  String avatar;
  String level;
  String emailVerifiedAt;
  String phoneVerifiedAt;
  String status;
  String createdAt;
  String updatedAt;

  _User(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.companyName,
        this.commercial,
        this.identity,
        this.license,
        this.certificate,
        this.avatar,
        this.level,
        this.emailVerifiedAt,
        this.phoneVerifiedAt,
        this.status,
        this.createdAt,
        this.updatedAt});

  _User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    companyName = json['company_name'] ?? "";
    commercial = json['commercial'];
    identity = json['identity'];
    license = json['license'];
    certificate = json['certificate'];
    avatar = json['avatar'];
    level = json['level'];
    emailVerifiedAt = json['email_verified_at'];
    phoneVerifiedAt = json['phone_verified_at'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['company_name'] = this.companyName;
    data['commercial'] = this.commercial;
    data['identity'] = this.identity;
    data['license'] = this.license;
    data['certificate'] = this.certificate;
    data['avatar'] = this.avatar;
    data['level'] = this.level;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['phone_verified_at'] = this.phoneVerifiedAt;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class UserAddress {
  int id;
  String additionalPhone;
  int userId;
  String lat;
  String lng;
  int countryId;
  int cityId;
  String address;
  String createdAt;
  String updatedAt;

  UserAddress(
      {this.id,
        this.additionalPhone,
        this.userId,
        this.lat,
        this.lng,
        this.countryId,
        this.cityId,
        this.address,
        this.createdAt,
        this.updatedAt});

  UserAddress.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    additionalPhone = json['additional_phone'];
    userId = json['user_id'];
    lat = json['lat'];
    lng = json['lng'];
    countryId = json['country_id'];
    cityId = json['city_id'];
    address = json['address'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['additional_phone'] = this.additionalPhone;
    data['user_id'] = this.userId;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['country_id'] = this.countryId;
    data['city_id'] = this.cityId;
    data['address'] = this.address;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
