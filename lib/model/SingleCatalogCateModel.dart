class SingleCatalogCateModel {
  bool success;
  Data data;
  String message;

  SingleCatalogCateModel({this.success, this.data, this.message});

  SingleCatalogCateModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Data {
  int id;
  Name name;
  String image;
  String createdAt;
  String updatedAt;
  List<Catalogs> catalogs;

  Data(
      {this.id,
        this.name,
        this.image,
        this.createdAt,
        this.updatedAt,
        this.catalogs});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['catalogs'] != null) {
      catalogs = new List<Catalogs>();
      json['catalogs'].forEach((v) {
        catalogs.add(new Catalogs.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['image'] = this.image;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.catalogs != null) {
      data['catalogs'] = this.catalogs.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Name {
  String ar;
  String en;

  Name({this.ar, this.en});

  Name.fromJson(Map<String, dynamic> json) {
    ar = json['ar'];
    en = json['en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ar'] = this.ar;
    data['en'] = this.en;
    return data;
  }
}

class Catalogs {
  int id;
  Name title;
  String image;
  String pdfFile;
  String createdAt;
  String updatedAt;
  int catalogCategoryId;

  Catalogs(
      {this.id,
        this.title,
        this.image,
        this.pdfFile,
        this.createdAt,
        this.updatedAt,
        this.catalogCategoryId});

  Catalogs.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'] != null ? new Name.fromJson(json['title']) : null;
    image = json['image'];
    pdfFile = json['pdf_file'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    catalogCategoryId = json['catalog_category_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.title != null) {
      data['title'] = this.title.toJson();
    }
    data['image'] = this.image;
    data['pdf_file'] = this.pdfFile;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['catalog_category_id'] = this.catalogCategoryId;
    return data;
  }
}
