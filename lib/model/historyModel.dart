class HistoryModel {
  bool status;
  List<OrderHistory> orderHistory;

  HistoryModel({this.status, this.orderHistory});

  HistoryModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['order_history'] != null) {
      orderHistory = new List<OrderHistory>();
      json['order_history'].forEach((v) {
        orderHistory.add(new OrderHistory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.orderHistory != null) {
      data['order_history'] = this.orderHistory.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderHistory {
  int id;
  String orderNo;
  int userId;
  int cityId;
  int shipId;
  String payment;
  int shipCompanyId;
  String orderStatus;
  String createdAt;
  String updatedAt;
  var totalPrice;
  var shipPrice;
  var totalAfterDiscount;
  var discount;
  String tax;
  ShipCompany shipCompany;
  City city;
  Ship ship;
  _User user;
  List<Products> products;

  OrderHistory(
      {this.id,
        this.orderNo,
        this.userId,
        this.cityId,
        this.shipId,
        this.payment,
        this.shipCompanyId,
        this.orderStatus,
        this.createdAt,
        this.updatedAt,
        this.totalPrice,
        this.shipPrice,
        this.totalAfterDiscount,
        this.discount,
        this.tax,
        this.shipCompany,
        this.city,
        this.ship,
        this.user,
        this.products});

  OrderHistory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderNo = json['order_no'];
    userId = json['user_id'];
    cityId = json['city_id'];
    shipId = json['ship_id'];
    payment = json['payment'];
    shipCompanyId = json['ship_company_id'];
    orderStatus = json['order_status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    totalPrice = json['total_price'];
    shipPrice = json['ship_price'];
    totalAfterDiscount = json['total_after_discount'];
    discount = json['discount'];
    tax = json['tax'];
    shipCompany = json['ship_company'] != null
        ? new ShipCompany.fromJson(json['ship_company'])
        : null;
    city = json['city'] != null ? new City.fromJson(json['city']) : null;
    ship = json['ship'] != null ? new Ship.fromJson(json['ship']) : null;
    user = json['user'] != null ? new _User.fromJson(json['user']) : null;
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_no'] = this.orderNo;
    data['user_id'] = this.userId;
    data['city_id'] = this.cityId;
    data['ship_id'] = this.shipId;
    data['payment'] = this.payment;
    data['ship_company_id'] = this.shipCompanyId;
    data['order_status'] = this.orderStatus;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['total_price'] = this.totalPrice;
    data['ship_price'] = this.shipPrice;
    data['total_after_discount'] = this.totalAfterDiscount;
    data['discount'] = this.discount;
    data['tax'] = this.tax;
    if (this.shipCompany != null) {
      data['ship_company'] = this.shipCompany.toJson();
    }
    if (this.city != null) {
      data['city'] = this.city.toJson();
    }
    if (this.ship != null) {
      data['ship'] = this.ship.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Name {
  String ar;
  String en;

  Name({this.ar, this.en});

  Name.fromJson(Map<String, dynamic> json) {
    ar = json['ar'];
    en = json['en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ar'] = this.ar;
    data['en'] = this.en;
    return data;
  }
}

class City {
  int id;
  Name name;
  int countryId;
  String createdAt;
  String updatedAt;

  City({this.id, this.name, this.countryId, this.createdAt, this.updatedAt});

  City.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    countryId = json['country_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['country_id'] = this.countryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Ship {
  int id;
  String additionalPhone;
  int userId;
  String lat;
  String lng;
  int countryId;
  int cityId;
  String address;
  String createdAt;
  String updatedAt;

  Ship(
      {this.id,
        this.additionalPhone,
        this.userId,
        this.lat,
        this.lng,
        this.countryId,
        this.cityId,
        this.address,
        this.createdAt,
        this.updatedAt});

  Ship.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    additionalPhone = json['additional_phone'];
    userId = json['user_id'];
    lat = json['lat'];
    lng = json['lng'];
    countryId = json['country_id'];
    cityId = json['city_id'];
    address = json['address'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['additional_phone'] = this.additionalPhone;
    data['user_id'] = this.userId;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['country_id'] = this.countryId;
    data['city_id'] = this.cityId;
    data['address'] = this.address;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class _User {
  int id;
  String name;
  String email;
  String phone;
  String companyName;
  String commercial;
  String identity;
  String license;
  String certificate;
  String avatar;
  String level;
  String emailVerifiedAt;
  String phoneVerifiedAt;
  String status;
  String createdAt;
  String updatedAt;

  _User(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.companyName,
        this.commercial,
        this.identity,
        this.license,
        this.certificate,
        this.avatar,
        this.level,
        this.emailVerifiedAt,
        this.phoneVerifiedAt,
        this.status,
        this.createdAt,
        this.updatedAt});

  _User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    companyName = json['company_name'];
    commercial = json['commercial'];
    identity = json['identity'];
    license = json['license'];
    certificate = json['certificate'];
    avatar = json['avatar'];
    level = json['level'];
    emailVerifiedAt = json['email_verified_at'];
    phoneVerifiedAt = json['phone_verified_at'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['company_name'] = this.companyName;
    data['commercial'] = this.commercial;
    data['identity'] = this.identity;
    data['license'] = this.license;
    data['certificate'] = this.certificate;
    data['avatar'] = this.avatar;
    data['level'] = this.level;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['phone_verified_at'] = this.phoneVerifiedAt;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Products {
  int id;
  Title title;
  String itemNo;
  String photo;
  Title content;
  int priceAfterDis;
  int price;
  String stock;
  String status;
  int categoryId;
  String createdAt;
  String updatedAt;
  Pivot pivot;

  Products(
      {this.id,
        this.title,
        this.itemNo,
        this.photo,
        this.content,
        this.priceAfterDis,
        this.price,
        this.stock,
        this.status,
        this.categoryId,
        this.createdAt,
        this.updatedAt,
        this.pivot});

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'] != null ? new Title.fromJson(json['title']) : null;
    itemNo = json['item_no'];
    photo = json['photo'];
    content =
    json['content'] != null ? new Title.fromJson(json['content']) : null;
    priceAfterDis = json['price_after_dis'];
    price = json['price'];
    stock = json['stock'];
    status = json['status'];
    categoryId = json['category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.title != null) {
      data['title'] = this.title.toJson();
    }
    data['item_no'] = this.itemNo;
    data['photo'] = this.photo;
    if (this.content != null) {
      data['content'] = this.content.toJson();
    }
    data['price_after_dis'] = this.priceAfterDis;
    data['price'] = this.price;
    data['stock'] = this.stock;
    data['status'] = this.status;
    data['category_id'] = this.categoryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.pivot != null) {
      data['pivot'] = this.pivot.toJson();
    }
    return data;
  }
}

class Title {
  String ar;
  String en;

  Title({this.ar, this.en});

  Title.fromJson(Map<String, dynamic> json) {
    ar = json['ar'];
    en = json['en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ar'] = this.ar;
    data['en'] = this.en;
    return data;
  }
}

class Pivot {
  String orderId;
  String productId;
  String qty;

  Pivot({this.orderId, this.productId, this.qty});

  Pivot.fromJson(Map<String, dynamic> json) {
    orderId = json['order_id'];
    productId = json['product_id'];
    qty = json['qty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_id'] = this.orderId;
    data['product_id'] = this.productId;
    data['qty'] = this.qty;
    return data;
  }
}

class ShipCompany {
  int id;
  Title name;
  String createdAt;
  String updatedAt;

  ShipCompany({this.id, this.name, this.createdAt, this.updatedAt});

  ShipCompany.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'] != null ? new Title.fromJson(json['name']) : null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
