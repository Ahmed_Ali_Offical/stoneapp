import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/model/user.dart';
import 'package:stone_app/register/registerComp.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class IndiEdit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return IndiEditState();
  }
}

String username, mail, pass, phone;
String _image = "";
bool indi_loading = false;

class IndiEditState extends State<IndiEdit> {
  double screenWidth;
  double screenheight;
  User user;

  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    screenWidth = _width;
    screenheight = _height;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    Future getImage() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        _image = image.path;
      });
    }

    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        new FallbackCupertinoLocalisationsDelegate(),
        //app-specific localization
        _specificLocalizationDelegate
      ],
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
        body: GestureDetector(child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Center(
                  child: GestureDetector(
                    child: Stack(
                      children: <Widget>[
                        Container(
                          child: Padding(
                            child: _image != null && _image.isNotEmpty
                                ? CircleAvatar(
                              backgroundImage:
                              FileImage(new File(_image)),
                              radius: _width * 0.12,
                            )
                                : CircleAvatar(
                              backgroundImage: NetworkImage(user.image),
                              radius: _width * 0.12,
                            ),
                            padding: EdgeInsets.all(_width * 0.02),
                          ),
                          decoration: BoxDecoration(
                              color: Colors.grey[300], shape: BoxShape.circle),
                        ),
                      ],
                    ),
                    onTap: getImage,
                  ),
                ),
                Container(
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    initialValue: user.name,
                    style: TextStyle(
                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                    ),
                    decoration: InputDecoration(
                        labelText: appLocalizations.Username,
                        hintText: appLocalizations.Username_Enter,
                        border: InputBorder.none,
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: const BorderSide(
                              color: MyColors.appYellow, width: 0.0),
                        ),
                        prefixIcon: Icon(
                          Icons.person,
                          color: MyColors.appYellow,
                        ),
                        suffixIcon: Icon(
                          Icons.edit,
                          color: MyColors.appYellow,
                        )),
                    onChanged: (value) {
                      username = value;
                    },
                  ),
                  margin: EdgeInsets.only(
                      top: _height * 0.05,
                      left: _width * 0.08,
                      right: _width * 0.08),
                ),
                Container(
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    initialValue: user.email,
                    style: TextStyle(
                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                    ),
                    decoration: InputDecoration(
                        labelText: appLocalizations.Email,
                        hintText: appLocalizations.Email_Enter,
                        border: InputBorder.none,
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: const BorderSide(
                              color: MyColors.appYellow, width: 0.0),
                        ),
                        prefixIcon: Icon(
                          Icons.mail_outline,
                          color: MyColors.appYellow,
                        ),
                        suffixIcon: Icon(
                          Icons.edit,
                          color: MyColors.appYellow,
                        )),
                    onChanged: (value) {
                      mail = value;
                    },
                  ),
                  margin: EdgeInsets.only(
                      top: _height * 0.02,
                      left: _width * 0.08,
                      right: _width * 0.08),
                ),
                Container(
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    style: TextStyle(
                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                    ),
                    decoration: InputDecoration(
                        labelText: appLocalizations.Password,
                        hintText: appLocalizations.Password_Enter,
                        border: InputBorder.none,
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: const BorderSide(
                              color: MyColors.appYellow, width: 0.0),
                        ),
                        prefixIcon: Icon(
                          Icons.lock_open,
                          color: MyColors.appYellow,
                        ),
                        suffixIcon: Icon(
                          Icons.edit,
                          color: MyColors.appYellow,
                        )),
                    onChanged: (value) {
                      pass = value;
                    },
                  ),
                  margin: EdgeInsets.only(
                      top: _height * 0.02,
                      left: _width * 0.08,
                      right: _width * 0.08),
                ),
                Container(
                  child: TextFormField(
                    keyboardType: TextInputType.phone,
                    initialValue: user.phone,
                    style: TextStyle(
                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                    ),
                    decoration: InputDecoration(
                        labelText: appLocalizations.Phone,
                        hintText: appLocalizations.Phone_Enter,
                        border: InputBorder.none,
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: const BorderSide(
                              color: MyColors.appYellow, width: 0.0),
                        ),
                        prefixIcon: Icon(
                          Icons.phone,
                          color: MyColors.appYellow,
                        ),
                        suffixIcon: Icon(
                          Icons.edit,
                          color: MyColors.appYellow,
                        )),
                    onChanged: (value) {
                      phone = value;
                    },
                  ),
                  margin: EdgeInsets.only(
                      top: _height * 0.02,
                      left: _width * 0.08,
                      right: _width * 0.08),
                ),
                indi_loading
                    ? Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                  margin: EdgeInsets.all(8.0),
                )
                    : GestureDetector(
                  child: Container(
                    width: double.infinity,
                    height: _height * 0.07,
                    margin: EdgeInsets.only(
                        left: _width * 0.08,
                        right: _width * 0.08,
                        top: _height * 0.03),
                    decoration: BoxDecoration(
                        color: MyColors.appYellow,
                        borderRadius:
                        BorderRadius.all(Radius.circular(30.0))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          child: AutoSizeText(appLocalizations.Edit,
                              textAlign: TextAlign.center,
                              maxFontSize: 16.0,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.white,
                                  fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.none)),
                          padding: EdgeInsets.only(left: 3.0, right: 3.0),
                        ),
                        Icon(
                          Icons.exit_to_app,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                  onTap: _edit,
                ),
//              GestureDetector(
//                child: Center(
//                    child: Container(
//                      child: AutoSizeText(
//                        "Register as Company",
//                        style: TextStyle(
//                            fontSize: 13.0,
//                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
//                            decoration: TextDecoration.none),
//                      ),
//                      margin: EdgeInsets.all(8.0),
//                    )),
//                onTap: () {
//                  Navigator.pushReplacement(context,
//                      MaterialPageRoute(builder: (context) => RegisterCompany()));
//                },
//              )
              ],
            ),
            Reusable.showLoader(loading)
          ],
        ),onTap: (){
          FocusScope.of(context).requestFocus(new FocusNode());
        },),
      ),
    );
  }

  void _edit() async {
    setState(() {
      loading = true;
    });
    var uri = Uri.parse(Urls.updateUser);
    print("Url $uri");
    Map<String, String> headers = {
      "Authorization": "Bearer ${user.token}",
      "Accept": "application/json"
    };
    if (_image.isEmpty &&
        commertial == null &&
        identity == null &&
        shop == null &&
        tax == null) {
      var requestWithoutPhoto =
          await post(uri.toString(), headers: headers, body: {
        'name': username,
        'email': mail,
        'password': pass,
        'phone': phone,
        'level': "user"
      });
      setState(() {
        loading = false;
      });
      if (json.decode(requestWithoutPhoto.body)['success']) {
        var userVal = json.decode(requestWithoutPhoto.body);
        saveUser(userVal);
        Toast.show(appLocalizations.Edit_Done, context, duration: Toast.LENGTH_LONG);
      } else {
        List<dynamic> errors = json.decode(requestWithoutPhoto.body)['message'];
        List<String> error = [];
        for (String i in errors) {
          error.add(i);
        }
        showDialog(
            context: this.context,
            builder: (BuildContext context) {
              return Reusable.errorDialog(
                  context, screenWidth, screenheight, error);
            });
      }
    } else {
      var request = http.MultipartRequest('POST', uri)
        ..fields['name'] = username
        ..fields['email'] = mail
        ..fields['password'] = pass
        ..fields['phone'] = phone
        ..fields['level'] = "user"
        ..headers.addAll(headers);

      if (_image.isNotEmpty)
        request..files.add(await http.MultipartFile.fromPath('avatar', _image));

      print("Headers ${request.headers.toString()}");
      print("Params ${request.fields.toString()}");

      var response = await request.send();
      print("Status Code ${response.statusCode}");
//      if (response.statusCode == 200)
      await response.stream.transform(utf8.decoder).listen((value) {
        print(value);
        setState(() {
          loading = false;
        });
        if (json.decode(value)['success']) {
          var userVal = json.decode(value);
          saveUser(userVal);
          Toast.show(appLocalizations.Edit_Done, context, duration: Toast.LENGTH_LONG);
        } else {
          List<dynamic> errors = json.decode(value)['message'];
          List<String> error = [];
          for (String i in errors) {
            error.add(i);
          }
          showDialog(
              context: this.context,
              builder: (BuildContext context) {
                return Reusable.errorDialog(
                    context, screenWidth, screenheight, error);
              });
        }
      });
    }
  }

  void getUser() async {
    var prefs = await SharedPreferences.getInstance();
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        user = User(
            prefs.getInt("id") ?? 0,
            prefs.getString("name") ?? "",
            prefs.getString("email") ?? "",
            prefs.getString("phone") ?? "",
            prefs.getString("level") ?? "",
            prefs.getString("token") ?? "",
            prefs.getString("image") ?? "");
        username = user.name;
        mail = user.email;
        phone = user.phone;
      });
  }

  void saveUser(userVal) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt("id", userVal['user']['id']);
    prefs.setString("name", userVal['user']['name']);
    prefs.setString("email", userVal['user']['email']);
    prefs.setString("phone", userVal['user']['phone']);
    prefs.setString("level", userVal['user']['level']);
    prefs.setString("token", userVal['token']);
    prefs.setString("image", userVal['user']['avatar']);
  }
}
