import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/applocalizations.dart';
import 'package:stone_app/newPass/NewPassword.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';

class ForgetPass extends StatefulWidget {
  bool codeSent = false;

  String verificationId;

  @override
  State<StatefulWidget> createState() {
    return ForgetPassState();
  }
}

class ForgetPassState extends State<ForgetPass> {
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  bool loading = false;
  String phone,olPhone;
  String code ;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
  }

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    ScreenUtil.init(context, width: _width, height: _height);

    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        new FallbackCupertinoLocalisationsDelegate(),
        //app-specific localization
        _specificLocalizationDelegate
      ],
      supportedLocales: [Locale('en'), Locale('ar')],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: new IconThemeData(color: Colors.black),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Center(
            child: Text(
              appLocalizations.No_PASS,
              style: TextStyle(
                color: Colors.black,
                fontSize: ScreenUtil().setSp(15.0),
                fontFamily: lan == "en" ? "Poppins" : "Arabic",
              ),
            ),
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              appLocalizations.Phone_Enter,
              style: TextStyle(
                color: Colors.black,
                fontSize: ScreenUtil().setSp(15.0),
                fontFamily: lan == "en" ? "Poppins" : "Arabic",
              ),
            ),
            Container(
              child: TextFormField(
                keyboardType: TextInputType.phone,
                style: TextStyle(
                  fontFamily: lan == "en" ? "Poppins" : "Arabic",
                ),
                decoration: InputDecoration(
                    labelText: appLocalizations.Phone,
                    hintText: appLocalizations.Phone_Enter,
                    border: InputBorder.none,
                    enabledBorder: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                      borderSide: const BorderSide(
                          color: MyColors.appYellow, width: 0.0),
                    ),
                    prefixIcon: Icon(
                      Icons.phone,
                      color: MyColors.appYellow,
                    ),
                    suffixIcon: Icon(
                      Icons.edit,
                      color: MyColors.appYellow,
                    )),
                onChanged: (value) {
                  setState(() {
                    phone = value;
                    olPhone = value;
                  });
                },
              ),
              margin: EdgeInsets.only(
                  top: _height * 0.02,
                  left: _width * 0.08,
                  right: _width * 0.08),
            ),
            loading
                ? Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                    margin: EdgeInsets.all(8.0),
                  )
                : GestureDetector(
                    child: Container(
                      width: double.infinity,
                      height: _height * 0.07,
                      margin: EdgeInsets.only(
                          left: _width * 0.08,
                          right: _width * 0.08,
                          top: _height * 0.03),
                      decoration: BoxDecoration(
                          color: MyColors.appYellow,
                          borderRadius:
                              BorderRadius.all(Radius.circular(16.0))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            child: Text(appLocalizations.Send,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(15),
                                    color: Colors.white,
                                    fontFamily:
                                        lan == "en" ? "Poppins" : "Arabic",
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.none)),
                            padding: EdgeInsets.only(left: 3.0, right: 3.0),
                          )
                        ],
                      ),
                    ),
                    onTap: _sendCode,
                  )
          ],
        ),
      ),
    );
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }

  void _sendCode() async {
    setState(() {
      loading = true;
    });
    var loginRequst = await post(Urls.forgetpass, body: {
      'phone': phone,
    });
    setState(() {
      loading = false;
    });
    print(loginRequst.body);
    bool status = json.decode(loginRequst.body)['success'];
    if (status) {
      await FirebaseAuth.instance.verifyPhoneNumber(
          phoneNumber: !phone.contains("+2")
              ? phone = "+2${phone}"
              : phone,
          timeout: const Duration(seconds: 10),
          verificationCompleted: (AuthCredential user) {
            print("Verified");
          },
          verificationFailed: (AuthException exception) {
            print("Exception ${exception.message}");
          },
          codeSent: (String verId, [int forceCodeResend]) {
            setState(() {
              widget.codeSent = true;
              widget.verificationId = verId;
            });
          },
          codeAutoRetrievalTimeout: (String verId) {
            widget.verificationId = verId;
          });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0)),
                //this right here
                child: Container(
                    height: MediaQuery.of(context).size.width * 0.8,
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: Padding(
                        child: Stack(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    style: TextStyle(
                                      fontFamily:
                                          lan == "en" ? "Poppins" : "Arabic",
                                    ),
                                    decoration: InputDecoration(
                                      labelText: appLocalizations.Code,
                                      hintText: appLocalizations.Enter_Code,
                                      border: InputBorder.none,
                                      enabledBorder: const OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30.0)),
                                        borderSide: const BorderSide(
                                            color: MyColors.appYellow,
                                            width: 0.0),
                                      ),
                                    ),
                                    onChanged: (value) {
                                      setState(() {
                                        code = value;
                                      });
                                    },
                                  ),
                                  margin: EdgeInsets.only(
                                      top: ScreenUtil().setHeight(8),
                                      left: ScreenUtil().setWidth(16),
                                      right: ScreenUtil().setWidth(16)),
                                ),
                                GestureDetector(
                                  child: Container(
                                    width: double.infinity,
                                    height: ScreenUtil().setHeight(32),
                                    margin: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(32),
                                        right: ScreenUtil().setWidth(32),
                                        top: ScreenUtil().setHeight(8)),
                                    decoration: BoxDecoration(
                                        color: MyColors.appYellow,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(16.0))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Padding(
                                          child: Text(appLocalizations.Confirm,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize:
                                                      ScreenUtil().setSp(16),
                                                  color: Colors.white,
                                                  fontFamily: lan == "en"
                                                      ? "Poppins"
                                                      : "Arabic",
                                                  fontWeight: FontWeight.bold,
                                                  decoration:
                                                      TextDecoration.none)),
                                          padding: EdgeInsets.only(
                                              left: 3.0, right: 3.0),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap:(){
                                    verifyCode(json.decode(loginRequst.body)['data']['code'].toString());
                                  },
                                )
                              ],
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(
                            MediaQuery.of(context).size.width * 0.03))));
          });
    }else{
      Toast.show(json.decode(loginRequst.body)['message'], context,duration: Toast.LENGTH_LONG);
    }
  }

  void verifyCode(String apicode) async{
    if (code.isNotEmpty) {
      AuthCredential authCreds = PhoneAuthProvider.getCredential(
          verificationId: widget.verificationId, smsCode: code);
      if (authCreds != null) {
        FirebaseAuth.instance.signInWithCredential(authCreds).then((value) => {
          if (value.user != null)
            {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                    return NewPassword(apicode,olPhone);
                  })),
            }
          else
            {Toast.show(appLocalizations.Code_Error, context, duration: Toast.LENGTH_LONG)}
        });
      } else {
        Toast.show(appLocalizations.Code_Error, context, duration: Toast.LENGTH_LONG);
      }
    } else {
      Toast.show(appLocalizations.Enter_Received, context, duration: Toast.LENGTH_LONG);
    }
  }
}
