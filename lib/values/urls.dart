class Urls{
  static const String _domain = "http://stoneveneersworld.com/api/";

  static const String categories = "${_domain}categories";
  static const String catalogcategories = "${_domain}catalog-categories";
  static const String products = "${_domain}products";
  static const String login = "${_domain}login";
  static const String register = "${_domain}register";
  static const String updateUser = "${_domain}user/update";
  static const String sliders = "${_domain}sliders";
  static const String contact = "${_domain}contacts";
  static const String verify = "${_domain}verify";
  static const String countries = "${_domain}countries";
  static const String addAddress = "${_domain}ships";
  static const String ships = "${_domain}ships-company-by-city";
  static const String history = "${_domain}my-orders";
  static const String user = "${_domain}user";
  static const String orders = "${_domain}orders";
  static const String settings = "${_domain}settings";
  static const String forgetpass = "${_domain}forgot_password";
  static const String resetpass = "${_domain}change_password";
  static const String sendmail = "${_domain}send-invoice-email";
}