class Clicks{
  static const int MoreProducts = 100;
  static const int MoreCategories = 101;
  static const int SingleCategory = 102;
  static const int SingleProduct = 103;
  static const int AddToCart = 104;
  static const int LocginClick = 105;
  static const int MainScreen = 106;
}