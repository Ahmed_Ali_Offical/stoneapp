import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/model/SingleCatalogCateModel.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:url_launcher/url_launcher.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class SingleCatalogCategory extends StatefulWidget{
  String barTitle;
  String id;

  SingleCatalogCategory(this.barTitle, this.id);

  @override
  State<StatefulWidget> createState() {
    return SingleCateCategoryState();
  }

}

class SingleCateCategoryState extends State<SingleCatalogCategory> {
  int cartItemsCount = 0;
  SingleCatalogCateModel singleCategoryModel;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }
  bool _load = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    updateCartIcon();
    getChilds(widget.id);
  }
  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    return MaterialApp(localizationsDelegates: [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      new FallbackCupertinoLocalisationsDelegate(),
      //app-specific localization
      _specificLocalizationDelegate
    ],
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home:Scaffold(
      appBar: Reusable.getAppBar(context, _height, _width, cartItemsCount, widget.barTitle),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: singleCategoryModel != null
            ?
        GridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 8,
          mainAxisSpacing: 8,
          childAspectRatio: 0.92,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          children: List.generate(singleCategoryModel.data.catalogs.length, (index) {
            return GestureDetector(
              child: Card(
                child: Wrap(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Stack(
                          children: [
                            Container(
                              width: double.infinity,
                              height: _height * 0.2,
                              child: ClipRRect(
                                child: Image.network(
                                  singleCategoryModel.data.catalogs[index].image,
                                  height: double.infinity,
                                  fit: BoxFit.cover,
                                  loadingBuilder: (BuildContext context, Widget child,
                                      ImageChunkEvent loadingProgress) {
                                    if (loadingProgress == null) return child;
                                    return Center(
                                      child: CircularProgressIndicator(
                                        value: loadingProgress.expectedTotalBytes != null
                                            ? loadingProgress.cumulativeBytesLoaded /
                                            loadingProgress.expectedTotalBytes
                                            : null,
                                      ),
                                    );
                                  },
                                ),
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(8.0),
                                    topRight: Radius.circular(8.0)),
                              ),
                            ),
                            Container(
                              height: _height * 0.02,
                              margin: EdgeInsets.all(_height * 0.01),
                              child: FittedBox(
                                child: AutoSizeText(
                                  lan == "en" ? singleCategoryModel.data.catalogs[index].title.en :singleCategoryModel.data.catalogs[index].title.ar,
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      color: Colors.white,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.none),
                                ),
                                fit: BoxFit.fitHeight,
                              ),
                            )
                          ],alignment: Alignment.center,),
                        Container(
                          child: Padding(
                            child: RaisedButton(
                              color: MyColors.appYellow,
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                children: <Widget>[
                                  AutoSizeText(
                                    appLocalizations.Open,
                                    style: TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.white,
                                        fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                        fontWeight: FontWeight.bold,
                                        decoration: TextDecoration.none),
                                  ),
                                  Padding(
                                    child: Icon(
                                      Icons.picture_as_pdf,
                                      color: Colors.white,
                                    ),
                                    padding:
                                    EdgeInsets.all(_width * 0.001),
                                  )
                                ],
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(9.0),
                                    bottomRight: Radius.circular(9.0)),
                              ),
                              onPressed: () {

                              },
                            ),
                            padding:
                            EdgeInsets.only(bottom: _height * 0.005),
                          ),
                          height: _height * 0.04,
                        )
                      ],
                    )
                  ],
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(9.0)),
                ),
              ),
              onTap: () {
                startPdf(singleCategoryModel.data.catalogs[index].pdfFile);
              },
            );
          }),
        )
            : Reusable.showLoader(_load),
      ),
    ) ,);
  }

  void updateCartIcon() async {
    var prefs = await SharedPreferences.getInstance();
    CartModel cartModel =
    new CartModel.fromJson(json.decode(prefs.getString("items")));
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        cartItemsCount = cartModel.inner.length;
      });
  }

  void getChilds(String id) async {
    var childsReques = await get("${Urls.catalogcategories}/$id");
    singleCategoryModel =
        SingleCatalogCateModel.fromJson(jsonDecode(childsReques.body));
    setState(() {
      singleCategoryModel;
      _load = false;
    });
  }

  startPdf(String pdfFile) async{
    if(await canLaunch(pdfFile)){
      await launch(pdfFile);
    }
  }
}