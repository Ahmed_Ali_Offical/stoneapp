import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/model/user.dart';
import 'package:stone_app/model/userModel.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import '../applocalizations.dart';

class CompanyEdit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CompanyEditState();
  }
}

String username = "", mail = "", pass = "", phone = "";
String _image = "";
File identity, commertial, tax, shop;
bool loading = false;
bool pageloading = false;
bool edited = false;

class CompanyEditState extends State<CompanyEdit> {
  final FocusNode _identityFocusNode = FocusNode();
  final FocusNode _commercialFocusNode = FocusNode();
  final FocusNode _shopFocusNode = FocusNode();
  final FocusNode _taxFocusNode = FocusNode();
  double screenWidth;
  double screenheight;
  User user;
  UserModel detailedUser;
  BuildContext buildContext;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;

  // String lan = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
    _identityFocusNode.addListener(() {
      getIdentity();
    });
    _commercialFocusNode.addListener(() {
      getCommertail();
    });
    _shopFocusNode.addListener(() {
      getShop();
    });
    _taxFocusNode.addListener(() {
      getTax();
    });
  }

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  Widget build(BuildContext buildContext) {
    this.buildContext = buildContext;
    double _height = MediaQuery.of(buildContext).size.height;
    double _width = MediaQuery.of(buildContext).size.width;
    screenWidth = _width;
    screenheight = _height;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    Future getImage() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        _image = image.path;
        edited = true;
      });
    }

    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        new FallbackCupertinoLocalisationsDelegate(),
        //app-specific localization
        _specificLocalizationDelegate
      ],
      supportedLocales: [Locale('en'), Locale('ar')],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
        body: GestureDetector(
          child: pageloading
              ? Reusable.showLoader(pageloading)
              : SingleChildScrollView(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Center(
                          child: GestureDetector(
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  child: Padding(
                                    child: _image != null && _image.isNotEmpty
                                        ? CircleAvatar(
                                            backgroundImage:
                                                FileImage(new File(_image)),
                                            radius: _width * 0.12,
                                          )
                                        : detailedUser.user.avatar != null
                                            ? CircleAvatar(
                                                backgroundImage: NetworkImage(
                                                    detailedUser.user.avatar),
                                                radius: _width * 0.12,
                                              )
                                            : CircleAvatar(
                                                backgroundColor: Colors.white,
                                                backgroundImage:
                                                    AssetImage("assets/images/person.png"),
                                                radius: _width * 0.12,
                                              ),
                                    padding: EdgeInsets.all(_width * 0.02),
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      shape: BoxShape.circle),
                                ),
                              ],
                            ),
                            onTap: getImage,
                          ),
                        ),
                        Container(
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            initialValue: detailedUser.user.companyName,
                            style: TextStyle(
                              fontFamily: lan == "en" ? "Poppins" : "Arabic",
                            ),
                            decoration: InputDecoration(
                                labelText: appLocalizations.Company_Name,
                                hintText: appLocalizations.Company_Enter,
                                border: InputBorder.none,
                                enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30.0)),
                                  borderSide: const BorderSide(
                                      color: MyColors.appYellow, width: 0.0),
                                ),
                                prefixIcon: Icon(
                                  Icons.person,
                                  color: MyColors.appYellow,
                                ),
                                suffixIcon: Icon(
                                  Icons.edit,
                                  color: MyColors.appYellow,
                                )),
                            onChanged: (value) {
                              username = value;
                              edited = true;
                            },
                          ),
                          margin: EdgeInsets.only(
                              top: _height * 0.05,
                              left: _width * 0.08,
                              right: _width * 0.08),
                        ),
                        Container(
                          child: TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            initialValue: detailedUser.user.email,
                            style: TextStyle(
                              fontFamily: lan == "en" ? "Poppins" : "Arabic",
                            ),
                            decoration: InputDecoration(
                                labelText: appLocalizations.Email,
                                hintText: appLocalizations.Email_Enter,
                                border: InputBorder.none,
                                enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30.0)),
                                  borderSide: const BorderSide(
                                      color: MyColors.appYellow, width: 0.0),
                                ),
                                prefixIcon: Icon(
                                  Icons.mail_outline,
                                  color: MyColors.appYellow,
                                ),
                                suffixIcon: Icon(
                                  Icons.edit,
                                  color: MyColors.appYellow,
                                )),
                            onChanged: (value) {
                              mail = value;
                              edited = true;
                            },
                          ),
                          margin: EdgeInsets.only(
                              top: _height * 0.02,
                              left: _width * 0.08,
                              right: _width * 0.08),
                        ),
                        Container(
                          child: TextFormField(
                            obscureText: true,
                            style: TextStyle(
                              fontFamily: lan == "en" ? "Poppins" : "Arabic",
                            ),
                            decoration: InputDecoration(
                                labelText: appLocalizations.Password,
                                hintText: appLocalizations.Password_Enter,
                                border: InputBorder.none,
                                enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30.0)),
                                  borderSide: const BorderSide(
                                      color: MyColors.appYellow, width: 0.0),
                                ),
                                prefixIcon: Icon(
                                  Icons.lock_open,
                                  color: MyColors.appYellow,
                                ),
                                suffixIcon: Icon(
                                  Icons.edit,
                                  color: MyColors.appYellow,
                                )),
                            onChanged: (value) {
                              setState(() {
                                pass = value;
                                edited = true;
                              });
                            },
                          ),
                          margin: EdgeInsets.only(
                              top: _height * 0.02,
                              left: _width * 0.08,
                              right: _width * 0.08),
                        ),
                        Container(
                          child: TextFormField(
                            style: TextStyle(
                              fontFamily: lan == "en" ? "Poppins" : "Arabic",
                            ),
                            keyboardType: TextInputType.phone,
                            initialValue: detailedUser.user.phone,
                            decoration: InputDecoration(
                                labelText: appLocalizations.Phone,
                                hintText: appLocalizations.Phone_Enter,
                                border: InputBorder.none,
                                enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30.0)),
                                  borderSide: const BorderSide(
                                      color: MyColors.appYellow, width: 0.0),
                                ),
                                prefixIcon: Icon(
                                  Icons.phone,
                                  color: MyColors.appYellow,
                                ),
                                suffixIcon: Icon(
                                  Icons.edit,
                                  color: MyColors.appYellow,
                                )),
                            onChanged: (value) {
                              setState(() {
                                phone = value;
                                edited = true;
                              });
                            },
                          ),
                          margin: EdgeInsets.only(
                              top: _height * 0.02,
                              left: _width * 0.08,
                              right: _width * 0.08),
                        ),
                        GestureDetector(
                          child: Container(
                            child: Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.credit_card,
                                    color: MyColors.appYellow,
                                  ),
                                  Flexible(
                                    child: Container(
                                      height: _height * 0.02,
                                      width: double.infinity,
                                      alignment: lan == "en"
                                          ? Alignment.centerLeft
                                          : Alignment.centerRight,
                                      child: FittedBox(
                                        child: AutoSizeText(
                                          identity != null
                                              ? basename(identity.path)
                                              : basename(detailedUser
                                                      .user.identity) ??
                                                  appLocalizations.Identity,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.grey,
                                              fontFamily: lan == "en"
                                                  ? "Poppins"
                                                  : "Arabic",
                                              decoration: TextDecoration.none),
                                        ),
                                        fit: BoxFit.fitHeight,
                                      ),
                                      margin: EdgeInsets.only(
                                          left: _width * 0.03,
                                          right: _width * 0.3),
                                    ),
                                    flex: 1,
                                  ),
                                  Icon(
                                    Icons.attachment,
                                    color: MyColors.appYellow,
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.all(_width * 0.04),
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: MyColors.appYellow,
                                  width: _width * 0.0012),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                            ),
                            margin: EdgeInsets.only(
                                top: _height * 0.02,
                                left: _width * 0.08,
                                right: _width * 0.08),
                          ),
                          onTap: getIdentity,
                        ),
                        GestureDetector(
                          child: Container(
                            child: Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.insert_drive_file,
                                    color: MyColors.appYellow,
                                  ),
                                  Flexible(
                                    child: Container(
                                      height: _height * 0.02,
                                      width: double.infinity,
                                      alignment: lan == "en"
                                          ? Alignment.centerLeft
                                          : Alignment.centerRight,
                                      child: FittedBox(
                                        child: AutoSizeText(
                                          commertial != null
                                              ? basename(commertial.path)
                                              : basename(detailedUser
                                                      .user.commercial) ??
                                                  appLocalizations
                                                      .Commercial_Register,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.grey,
                                              fontFamily: lan == "en"
                                                  ? "Poppins"
                                                  : "Arabic",
                                              decoration: TextDecoration.none),
                                        ),
                                        fit: BoxFit.fitHeight,
                                      ),
                                      margin: EdgeInsets.only(
                                          left: _width * 0.03,
                                          right: _width * 0.3),
                                    ),
                                    flex: 1,
                                  ),
                                  Icon(
                                    Icons.attachment,
                                    color: MyColors.appYellow,
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.all(_width * 0.04),
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: MyColors.appYellow,
                                  width: _width * 0.0012),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                            ),
                            margin: EdgeInsets.only(
                                top: _height * 0.02,
                                left: _width * 0.08,
                                right: _width * 0.08),
                          ),
                          onTap: getCommertail,
                        ),
                        GestureDetector(
                          child: Container(
                            child: Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.content_copy,
                                    color: MyColors.appYellow,
                                  ),
                                  Flexible(
                                    child: Container(
                                      height: _height * 0.02,
                                      width: double.infinity,
                                      alignment: lan == "en"
                                          ? Alignment.centerLeft
                                          : Alignment.centerRight,
                                      child: FittedBox(
                                        child: AutoSizeText(
                                          shop != null
                                              ? basename(shop.path)
                                              : basename(detailedUser
                                                      .user.license) ??
                                                  appLocalizations.Shop_Licance,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.grey,
                                              fontFamily: lan == "en"
                                                  ? "Poppins"
                                                  : "Arabic",
                                              decoration: TextDecoration.none),
                                        ),
                                        fit: BoxFit.fitHeight,
                                      ),
                                      margin: EdgeInsets.only(
                                          left: _width * 0.03,
                                          right: _width * 0.3),
                                    ),
                                    flex: 1,
                                  ),
                                  Icon(
                                    Icons.attachment,
                                    color: MyColors.appYellow,
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.all(_width * 0.04),
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: MyColors.appYellow,
                                  width: _width * 0.0012),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                            ),
                            margin: EdgeInsets.only(
                                top: _height * 0.02,
                                left: _width * 0.08,
                                right: _width * 0.08),
                          ),
                          onTap: getShop,
                        ),
                        GestureDetector(
                          child: Container(
                            child: Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.content_copy,
                                    color: MyColors.appYellow,
                                  ),
                                  Flexible(
                                    child: Container(
                                      height: _height * 0.02,
                                      width: double.infinity,
                                      alignment: lan == "en"
                                          ? Alignment.centerLeft
                                          : Alignment.centerRight,
                                      child: FittedBox(
                                        child: AutoSizeText(
                                          tax != null
                                              ? basename(tax.path)
                                              : basename(detailedUser
                                                      .user.certificate) ??
                                                  appLocalizations
                                                      .Tax_certificate,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.grey,
                                              fontFamily: lan == "en"
                                                  ? "Poppins"
                                                  : "Arabic",
                                              decoration: TextDecoration.none),
                                        ),
                                        fit: BoxFit.fitHeight,
                                      ),
                                      margin: EdgeInsets.only(
                                          left: _width * 0.03,
                                          right: _width * 0.3),
                                    ),
                                    flex: 1,
                                  ),
                                  Icon(
                                    Icons.attachment,
                                    color: MyColors.appYellow,
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.all(_width * 0.04),
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: MyColors.appYellow,
                                  width: _width * 0.0012),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                            ),
                            margin: EdgeInsets.only(
                                top: _height * 0.02,
                                left: _width * 0.08,
                                right: _width * 0.08),
                          ),
                          onTap: getTax,
                        ),
                        loading
                            ? Container(
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                                margin: EdgeInsets.all(8.0),
                              )
                            : GestureDetector(
                                child: Container(
                                  width: double.infinity,
                                  height: _height * 0.07,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.08,
                                      right: _width * 0.08,
                                      top: _height * 0.03),
                                  decoration: BoxDecoration(
                                      color: MyColors.appYellow,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(30.0))),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        child: AutoSizeText(
                                            appLocalizations.Tax_Edit,
                                            textAlign: TextAlign.center,
                                            maxFontSize: 16.0,
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                color: Colors.white,
                                                fontFamily: lan == "en"
                                                    ? "Poppins"
                                                    : "Arabic",
                                                fontWeight: FontWeight.bold,
                                                decoration:
                                                    TextDecoration.none)),
                                        padding: EdgeInsets.only(
                                            left: 3.0, right: 3.0),
                                      ),
                                      Icon(
                                        Icons.exit_to_app,
                                        color: Colors.white,
                                      )
                                    ],
                                  ),
                                ),
                                onTap: _edit,
                              ),
//                GestureDetector(
//                  child: Center(
//                      child: Container(
//                        child: AutoSizeText(
//                          "Register as Individual",
//                          style: TextStyle(
//                              fontSize: 13.0,
//                              fontFamily: lan == "en" ? "Poppins" : "Arabic",
//                              decoration: TextDecoration.none),
//                        ),
//                        margin: EdgeInsets.all(8.0),
//                      )),
//                  onTap: () {
//                    Navigator.pushReplacement(
//                        this.context,
//                        MaterialPageRoute(
//                            builder: (context) => RegisiterIndividual()));
//                  },
//                )
                      ],
                    ),
                    margin: EdgeInsets.fromLTRB(
                        0, _height * 0.1, 0, _height * 0.01),
                  ),
                ),
          onTap: () {
            FocusScope.of(buildContext).requestFocus(new FocusNode());
          },
        ),
      ),
    );
  }

  void saveUser(userVal) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt("id", userVal['user']['id']);
    prefs.setString("name", userVal['user']['name']);
    prefs.setString("email", userVal['user']['email']);
    prefs.setString("phone", userVal['user']['phone']);
    prefs.setString("level", userVal['user']['level']);
    prefs.setString("token", userVal['token']);
    prefs.setString("image", userVal['user']['avatar']);
  }

  @override
  void dispose() {
    _identityFocusNode.dispose();
    _commercialFocusNode.dispose();
    _shopFocusNode.dispose();
    _taxFocusNode.dispose();
    super.dispose();
  }

  void getIdentity() async {
    identity = await FilePicker.getFile();
    setState(() {
      identity;
      edited = true;
    });
  }

  void getCommertail() async {
    commertial = await FilePicker.getFile();
    setState(() {
      commertial;
      edited = true;
    });
  }

  void getShop() async {
    shop = await FilePicker.getFile();
    setState(() {
      shop;
      edited = true;
    });
  }

  void getTax() async {
    tax = await FilePicker.getFile();
    setState(() {
      tax;
      edited = true;
    });
  }

  void getUser() async {
    var prefs = await SharedPreferences.getInstance();
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        user = User(
            prefs.getInt("id") ?? 0,
            prefs.getString("name") ?? "",
            prefs.getString("email") ?? "",
            prefs.getString("phone") ?? "",
            prefs.getString("level") ?? "",
            prefs.getString("token") ?? "",
            prefs.getString("image") ?? "");
        pageloading = true;
        getDetailedUser();
      });
  }

  void _edit() async {
    if (edited) {
      setState(() {
        loading = true;
      });
      var uri = Uri.parse(Urls.updateUser);
      print("Url $uri");
      Map<String, String> headers = {
        "Authorization": "Bearer ${user.token}",
        "Accept": "application/json"
      };
      if (_image.isEmpty &&
          commertial == null &&
          identity == null &&
          shop == null &&
          tax == null) {
        var requestWithoutPhoto =
            await post(uri.toString(), headers: headers, body: {
          'name': username,
          'company_name': username,
          'email': mail,
          'password': pass,
          'phone': phone,
          'level': "company"
        });
        setState(() {
          loading = false;
        });
        if (json.decode(requestWithoutPhoto.body)['success']) {
          var userVal = json.decode(requestWithoutPhoto.body);
          saveUser(userVal);
          Toast.show(appLocalizations.Edit_Done, buildContext,
              duration: Toast.LENGTH_LONG);
        } else {
          List<dynamic> errors =
              json.decode(requestWithoutPhoto.body)['message'];
          List<String> error = [];
          for (String i in errors) {
            error.add(i);
          }
          showDialog(
              context: this.context,
              builder: (BuildContext context) {
                return Reusable.errorDialog(
                    context, screenWidth, screenheight, error);
              });
        }
      } else {
        var request = http.MultipartRequest('POST', uri)
          ..fields['name'] = username
          ..fields['company_name'] = username
          ..fields['email'] = mail
          ..fields['password'] = pass
          ..fields['phone'] = phone
          ..fields['level'] = "company"
          ..headers.addAll(headers);

        if (_image.isNotEmpty)
          request
            ..files.add(await http.MultipartFile.fromPath('avatar', _image));

        if (commertial != null)
          request
            ..files.add(await http.MultipartFile.fromPath(
                'commercial', commertial.path));
        if (identity != null)
          request
            ..files.add(
                await http.MultipartFile.fromPath('identity', identity.path));
        if (shop != null)
          request
            ..files
                .add(await http.MultipartFile.fromPath('license', shop.path));
        if (tax != null)
          request
            ..files.add(
                await http.MultipartFile.fromPath('certificate', tax.path));

        print("Headers ${request.headers.toString()}");
        print("Params ${request.fields.toString()}");

        var response = await request.send();
        print("Status Code ${response.statusCode}");
//      if (response.statusCode == 200)
        await response.stream.transform(utf8.decoder).listen((value) {
          print(value);
          setState(() {
            loading = false;
          });
          if (json.decode(value)['success']) {
            var userVal = json.decode(value);
            saveUser(userVal);
            Toast.show(appLocalizations.Edit_Done, buildContext,
                duration: Toast.LENGTH_LONG);
          } else {
            List<dynamic> errors = json.decode(value)['message'];
            List<String> error = [];
            for (String i in errors) {
              error.add(i);
            }
            showDialog(
                context: this.context,
                builder: (BuildContext context) {
                  return Reusable.errorDialog(
                      context, screenWidth, screenheight, error);
                });
          }
        });
      }
    } else {
      Toast.show(appLocalizations.Edit_Warning, buildContext,
          duration: Toast.LENGTH_LONG);
    }
  }

  void getDetailedUser() async {
    var addressesRequest = await get(Urls.user,
        headers: {'Authorization': "Bearer ${user.token}"});
    detailedUser = new UserModel.fromJson(jsonDecode(addressesRequest.body));

    setState(() {
      username = detailedUser.user.companyName ?? "";
      phone = detailedUser.user.phone ?? "";
      mail = detailedUser.user.email ?? "";
      pageloading = false;
    });
  }
}
