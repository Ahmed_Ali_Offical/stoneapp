import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/cart/cart.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/SettingsModel.dart';
import 'package:stone_app/model/contactResponseModel.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class ContactUs extends StatefulWidget {
  var callback = Reusable.loginCallBack;

  ContactUs(this.callback);

  @override
  State<StatefulWidget> createState() {
    return ContactUsState();
  }
}

int cartItemsCount = 0;
String username = "";
String mail = "";
String msg = "";

class ContactUsState extends State<ContactUs> {
  SettingsModel settingsModel;
  var nameController = TextEditingController();
  var mailController = TextEditingController();
  var msgController = TextEditingController();
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";
  bool load = true;
  bool requestLoad = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
    getSettings();
  }

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    ScreenUtil.init(context, width: _width, height: _height);

    return WillPopScope(
        child: MaterialApp(
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              new FallbackCupertinoLocalisationsDelegate(),
              //app-specific localization
              _specificLocalizationDelegate
            ],
            supportedLocales: [
              Locale('en'),
              Locale('ar')
            ],
            locale: _specificLocalizationDelegate.overriddenLocale,
            theme: ThemeData(primarySwatch: MyColors.primary),
            home: Scaffold(
              body: settingsModel != null
                  ? SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            'assets/images/contactus.png',
                            fit: BoxFit.fitWidth,
                            width: double.infinity,
                            height: _height * 0.23,
                          ),
                          Reusable.flexibleText(
                              _height * 0.02,
                              appLocalizations.Contact_Us,
                              EdgeInsets.only(
                                  top: _height * 0.01,
                                  right: _height * 0.04,
                                  left: _height * 0.04)),
                          Container(
                            child: Container(
                              width: double.infinity,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Row(
                                    children: [
                                      AutoSizeText(
                                        appLocalizations.Phone_,
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.black,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none),
                                      ),
                                      AutoSizeText(
                                        settingsModel.data.contactPhone,
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.grey,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            decoration: TextDecoration.none),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      AutoSizeText(
                                        appLocalizations.Address_,
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.black,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none),
                                      ),
                                      AutoSizeText(
                                        settingsModel.data.contactAddress,
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.grey,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            decoration: TextDecoration.none),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      AutoSizeText(
                                        appLocalizations.Email_,
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.black,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none),
                                      ),
                                      AutoSizeText(
                                        settingsModel.data.contactEmail,
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            color: Colors.grey,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            decoration: TextDecoration.none),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.all(_width * 0.04),
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.grey, width: _width * 0.0012),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                            ),
                            margin: EdgeInsets.only(
                                top: _height * 0.02,
                                left: _width * 0.08,
                                right: _width * 0.08),
                          ),
                          Container(
                            height: ScreenUtil().setHeight(45),
                            child: Padding(
                              child: TextField(
                                keyboardType: TextInputType.text,
                                maxLines: 1,
                                onChanged: (value) {
                                  username = value;
                                },
                                style:
                                    TextStyle(fontSize: ScreenUtil().setSp(13)),
                                controller: nameController,
                                decoration: InputDecoration(
                                    labelText: appLocalizations.Company_Name,
                                    border: InputBorder.none,
                                    enabledBorder: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(30.0)),
                                      borderSide: const BorderSide(
                                          color: MyColors.appYellow,
                                          width: 0.0),
                                    ),
                                    prefixIcon: Icon(
                                      Icons.person,
                                      color: MyColors.appYellow,
                                    ),
                                    suffixIcon: Icon(
                                      Icons.edit,
                                      color: MyColors.appYellow,
                                    )),
                              ),
                              padding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(5),
                                  right: ScreenUtil().setWidth(5)),
                            ),
                            margin: EdgeInsets.only(
                                top: _height * 0.05,
                                left: _width * 0.08,
                                right: _width * 0.08),
                          ),
                          Container(
                            height: ScreenUtil().setHeight(45),
                            child: Padding(
                              child: TextField(
                                keyboardType: TextInputType.emailAddress,
                                maxLines: 1,
                                controller: mailController,
                                style:
                                    TextStyle(fontSize: ScreenUtil().setSp(13)),
                                onChanged: (value) {
                                  mail = value;
                                },
                                decoration: InputDecoration(
                                    labelText: appLocalizations.Email,
                                    border: InputBorder.none,
                                    enabledBorder: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(30.0)),
                                      borderSide: const BorderSide(
                                          color: MyColors.appYellow,
                                          width: 0.0),
                                    ),
                                    prefixIcon: Icon(
                                      Icons.mail_outline,
                                      color: MyColors.appYellow,
                                    ),
                                    suffixIcon: Icon(
                                      Icons.edit,
                                      color: MyColors.appYellow,
                                    )),
                              ),
                              padding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(5),
                                  right: ScreenUtil().setWidth(5)),
                            ),
                            margin: EdgeInsets.only(
                                top: _height * 0.02,
                                left: _width * 0.08,
                                right: _width * 0.08),
                          ),
                          Container(
                            height: _height * 0.2,
                            child: Padding(
                              child: TextField(
                                keyboardType: TextInputType.text,
                                maxLines: 5,
                                minLines: 5,
                                onChanged: (value) {
                                  msg = value;
                                },
                                controller: msgController,
                                decoration: InputDecoration(
                                  hintText: appLocalizations.Write_Msg,
                                  border: InputBorder.none,
                                  enabledBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30.0)),
                                    borderSide: const BorderSide(
                                        color: MyColors.appYellow, width: 0.0),
                                  ),
                                ),
                              ),
                              padding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(5),
                                  right: ScreenUtil().setWidth(5)),
                            ),
                            margin: EdgeInsets.only(
                                top: _height * 0.02,
                                left: _width * 0.08,
                                right: _width * 0.08),

//                  TextField(
//                    keyboardType: TextInputType.emailAddress,
//                    maxLines: 5,
//                    minLines: 5,
//                    textAlign: TextAlign.start,
//                    decoration: InputDecoration(
//                      hintText: "Write your message here",
//                      border: InputBorder.none,
//                      enabledBorder: const OutlineInputBorder(
//                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
//                        borderSide: const BorderSide(
//                            color: MyColors.appYellow, width: 0.0),
//                      ),
//                    ),
//                    onChanged: (value) {
//                      msg = value;
//                    },
//                  ),
                          ),
                          !requestLoad ? GestureDetector(
                            child: Container(
                              width: double.infinity,
                              height: _height * 0.06,
                              margin: EdgeInsets.only(
                                top: _width * 0.02,
                                left: _width * 0.08,
                                right: _width * 0.08,
                              ),
                              decoration: BoxDecoration(
                                  color: MyColors.appYellow,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30.0))),
                              child: Center(
                                child: AutoSizeText(appLocalizations.Send,
                                    textAlign: TextAlign.center,
                                    maxFontSize: 16.0,
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.white,
                                        fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                        fontWeight: FontWeight.bold,
                                        decoration: TextDecoration.none)),
                              ),
                            ),
                            onTap: sendMessage,
                          ):Center(child: CircularProgressIndicator(),)
                        ],
                      ),
                    )
                  : Reusable.showLoader(load),
            )),
        onWillPop: () {
          return Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return HomeScreen();
          }));
        });
  }

  void sendMessage() async {
    setState(() {
      requestLoad = true;
    });
    var response = await post(Urls.contact,
        headers: {"Accept": "application/json"},
        body: {'email': mail, 'name': username, 'message': msg});
    ContactResponseModel responseModel =
        ContactResponseModel.fromJson(json.decode(response.body));
    print(response.body);
    setState(() {
      requestLoad = false;
    });
    Toast.show(json.decode(response.body)['message'], context, duration: Toast.LENGTH_LONG);
    if (responseModel.success) {
      setState(() {
        mail = "";
        username = "";
        msg = "";
        nameController.clear();
        mailController.clear();
        msgController.clear();
      });
    }
  }

  void getSettings() async {
    var response = await get(Urls.settings);
    setState(() {
      load = false;
      settingsModel = new SettingsModel.fromJson(json.decode(response.body));
    });
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }
}
