import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/main.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class SettingsScreen extends StatefulWidget {
  var callback = Reusable.loginCallBack;

  SettingsScreen(this.callback);

  @override
  State<StatefulWidget> createState() {
    return SettingsState();
  }
}

bool notificationEnable = false;

class SettingsState extends State<SettingsScreen> {
  String facebook = "";
  String twitter = "";
  String instagram = "";

  BuildContext buildContext;

  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getSettings();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    buildContext = context;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));
    return WillPopScope(
        child: MaterialApp(
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            new FallbackCupertinoLocalisationsDelegate(),
            //app-specific localization
            _specificLocalizationDelegate
          ],
          supportedLocales: [
            Locale('en'),
            Locale('ar')
          ],
          locale: _specificLocalizationDelegate.overriddenLocale,
          theme: ThemeData(primarySwatch: MyColors.primary),
          home: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(
                  'assets/images/settings.png',
                  width: double.infinity,
                  height: _height * 0.23,
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      GestureDetector(child: Container(
                        child: Row(
                          children: [
                            Expanded(
                                child: Align(
                                  child: Reusable.flexibleText(
                                      _height * 0.02,
                                      appLocalizations.Language,
                                      EdgeInsets.only(
                                          top: _height * 0.01,
                                          right: _height * 0.01,
                                          left: _height * 0.01)),
                                  alignment: lan == "en" ? Alignment.centerLeft:Alignment.centerRight,
                                )),
                            Reusable.flexibleText(
                                _height * 0.018,
                                appLocalizations.English,
                                EdgeInsets.only(
                                    top: _height * 0.01,
                                    right: _height * 0.01,
                                    left: _height * 0.01)),
                          ],
                        ),
                        height: _height * 0.06,
                      ),onTap: changeLang,),
                      Container(
                        width: double.infinity,
                        height: 0.2,
                        color: Colors.grey,
                        margin: EdgeInsets.only(
                            top: _height * 0.01, bottom: _height * 0.01),
                      ),
//                      Container(
//                        child: Row(
//                          children: [
//                            Expanded(
//                                child: Align(
//                              child: Reusable.flexibleText(
//                                  _height * 0.02,
//                                  "Notifications",
//                                  EdgeInsets.only(
//                                      right: _height * 0.01,
//                                      left: _height * 0.01)),
//                              alignment: lan == "en" ? Alignment.centerLeft:Alignment.centerRight,
//                            )),
//                            Switch(
//                              value: notificationEnable,
//                              onChanged: (val) {
//                                setState(() {
//                                  notificationEnable = val;
//                                });
//                              },
//                              activeColor: MyColors.appYellow,
//                            ),
//                          ],
//                        ),
//                        height: _height * 0.06,
//                      ),
//                      Container(
//                        width: double.infinity,
//                        height: 0.2,
//                        color: Colors.grey,
//                        margin: EdgeInsets.only(
//                            top: _height * 0.01, bottom: _height * 0.01),
//                      ),
                      GestureDetector(child: Container(
                        child: Row(
                          children: [
                            Expanded(
                                child: Align(
                                  child: Reusable.flexibleText(
                                      _height * 0.02,
                                      appLocalizations.Rate_App,
                                      EdgeInsets.only(
                                          right: _height * 0.01,
                                          left: _height * 0.01)),
                                  alignment: lan == "en" ? Alignment.centerLeft:Alignment.centerRight,
                                )),
                            Icon(Icons.chevron_right),
                          ],
                        ),
                        height: _height * 0.06,
                      ),onTap: (){
                        if (Platform.isAndroid) {
                          rateAndroid();
                        } else if (Platform.isIOS) {
                          rateIOS();
                        }
                      },),
                      Container(
                        width: double.infinity,
                        height: 0.2,
                        color: Colors.grey,
                        margin: EdgeInsets.only(
                            top: _height * 0.01, bottom: _height * 0.01),
                      ),
                      GestureDetector(child: Container(
                        child: Row(
                          children: [
                            Expanded(
                                child: Align(
                                  child: Reusable.flexibleText(
                                      _height * 0.02,
                                      appLocalizations.Share_App,
                                      EdgeInsets.only(
                                          bottom: _height * 0.01,
                                          right: _height * 0.01,
                                          left: _height * 0.01)),
                                  alignment: lan == "en" ? Alignment.centerLeft:Alignment.centerRight,
                                )),
                            Icon(Icons.chevron_right),
                          ],
                        ),
                        height: _height * 0.06,
                      ),onTap: (){
                        if (Platform.isAndroid) {
                          shareAndroid();
                        } else if (Platform.isIOS) {
                          shareIOS();
                        }
                      },),
                    ],
                  ),
                  decoration: BoxDecoration(
                    border:
                    Border.all(color: Colors.grey, width: _width * 0.0012),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  ),
                  margin: EdgeInsets.only(
                      top: _height * 0.02,
                      left: _width * 0.05,
                      right: _width * 0.05),
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Reusable.flexibleText(
                          _height * 0.025,
                          appLocalizations.Contact_Us,
                          EdgeInsets.only(
                              top: _height * 0.01, bottom: _height * 0.01)),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            child: Container(
                              child: Image.asset(
                                'assets/images/facebook.png',
                                width: _width * 0.08,
                                height: _width * 0.08,
                              ),
                              margin: EdgeInsets.all(_height * 0.01),
                            ),
                            onTap: () {
                              launch(facebook);
                            },
                          ),
                          GestureDetector(
                            child: Container(
                              child: Image.asset(
                                'assets/images/twitter.png',
                                width: _width * 0.08,
                                height: _width * 0.08,
                              ),
                              margin: EdgeInsets.all(_height * 0.01),
                            ),
                            onTap: () {
                              launch(twitter);
                            },
                          ),
                          GestureDetector(
                            child: Container(
                              child: Image.asset(
                                'assets/images/insta.png',
                                width: _width * 0.08,
                                height: _width * 0.08,
                              ),
                              margin: EdgeInsets.all(_height * 0.01),
                            ),
                            onTap: () {
                              launch(instagram);
                            },
                          ),
                          Container(
                            child: Image.asset(
                              'assets/images/linkdin.png',
                              width: _width * 0.08,
                              height: _width * 0.08,
                            ),
                            margin: EdgeInsets.all(_height * 0.01),
                          ),
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                    border:
                    Border.all(color: Colors.grey, width: _width * 0.0012),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  ),
                  margin: EdgeInsets.only(
                      top: _height * 0.02,
                      left: _width * 0.05,
                      right: _width * 0.05),
                )
              ],
            ),
          ),
        ),),
        onWillPop: () {
          return Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return HomeScreen();
          }));
        });
  }

  void getSettings() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
      facebook = prefs.getString("facebook");
      twitter = prefs.getString("twitter");
      instagram = prefs.getString("instagram");
    });
  }

  void changeLang() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("en", lan == "en" ? "ar":"en");
    Navigator.pushAndRemoveUntil(buildContext, MaterialPageRoute(builder: (BuildContext context) => SplashScreen()), (Route<dynamic> route) => false);
  }

  void shareAndroid() {
    Toast.show(" Android المشاركه سوف تكون متاحه عند الرفع", context,duration: Toast.LENGTH_LONG);
  }

  void shareIOS() {
    Toast.show(" IOS المشاركه سوف تكون متاحه عند الرفع", context,duration: Toast.LENGTH_LONG);
  }

  void rateIOS() {
    Toast.show(" Android التقييمات سوف تكون متاحه عند الرفع", context,duration: Toast.LENGTH_LONG);
  }

  void rateAndroid() {
    Toast.show(" IOS التقييمات سوف تكون متاحه عند الرفع", context,duration: Toast.LENGTH_LONG);
  }
}
