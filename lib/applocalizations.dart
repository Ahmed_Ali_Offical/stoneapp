import 'dart:async';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'l10n/messages_all.dart';

class AppLocalizations {
  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Future<AppLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new AppLocalizations();
    });
  }

  String get Add_Address {
    return Intl.message('Add Address', name: 'Add_Address');
  }

  String get Choose_Address {
    return Intl.message('Choose Address', name: 'Choose_Address');
  }

  String get Address {
    return Intl.message('Address', name: 'Address');
  }

  String get Address_Required {
    return Intl.message('Address Required', name: 'Address_Required');
  }

  String get Phone {
    return Intl.message('Phone', name: 'Phone');
  }

  String get Complete_Purchase {
    return Intl.message('Complete Purchase', name: 'Complete_Purchase');
  }

  String get address_msg {
    return Intl.message('Type valid Address', name: 'address_msg');
  }

  String get Cart {
    return Intl.message('Cart', name: 'Cart');
  }

  String get no_orders {
    return Intl.message('No Orders Here', name: 'no_orders');
  }

  String get Quantity {
    return Intl.message('Quantity ', name: 'Quantity');
  }

  String get Price {
    return Intl.message('Price', name: 'Price');
  }

  String get Total {
    return Intl.message('Total :', name: 'Total');
  }

  String get Company_Name {
    return Intl.message('Co. Name', name: 'Company_Name');
  }

  String get Company_Enter {
    return Intl.message('Enter Your Company Name', name: 'Company_Enter');
  }

  String get Email {
    return Intl.message('Email', name: 'Email');
  }

  String get Email_Enter {
    return Intl.message('Enter Your Email', name: 'Email_Enter');
  }

  String get Password {
    return Intl.message('Password', name: 'Password');
  }

  String get Password_Enter {
    return Intl.message('Enter Password', name: 'Password_Enter');
  }

  String get Phone_Enter {
    return Intl.message('Enter Your Phone', name: 'Phone_Enter');
  }

  String get Identity {
    return Intl.message('Identity', name: 'Identity');
  }

  String get Commercial_Register {
    return Intl.message('Commercial Register', name: 'Commercial_Register');
  }

  String get Shop_Licance {
    return Intl.message('Shop Licance', name: 'Shop_Licance');
  }

  String get Tax_certificate {
    return Intl.message('Tax certificate', name: 'Tax_certificate');
  }

  String get Tax_Edit {
    return Intl.message('Tax Edit', name: 'Tax_Edit');
  }

  String get Edit_Done {
    return Intl.message('Edit Done', name: 'Edit_Done');
  }

  String get Edit_Warning {
    return Intl.message('You have to edit fields before apply',
        name: 'Edit_Warning');
  }

  String get Contact_Us {
    return Intl.message('Contact Us', name: 'Contact_Us');
  }

  String get Phone_ {
    return Intl.message('Phone: ', name: 'Phone_');
  }

  String get Address_ {
    return Intl.message('Address: ', name: 'Address_');
  }

  String get Email_ {
    return Intl.message('Email: ', name: 'Email_');
  }

  String get Write_Msg {
    return Intl.message('Write your message here', name: 'Write_Msg');
  }

  String get Send {
    return Intl.message('Send', name: 'Send');
  }

  String get Order_Number {
    return Intl.message('Order Number :', name: 'Order_Number');
  }

  String get No_Orders {
    return Intl.message('No Orders yet', name: 'No_Orders');
  }

  String get Login {
    return Intl.message('Login', name: 'Login');
  }

  String get No_Account {
    return Intl.message("Don't have Account ?", name: 'No_Account');
  }

  String get No_PASS {
    return Intl.message("Forget Password ?", name: 'No_PASS');
  }

  String get Categories {
    return Intl.message("Categories", name: 'Categories');
  }

  String get Products {
    return Intl.message("Products", name: 'Products');
  }

  String get More {
    return Intl.message("More", name: 'More');
  }

  String get Item_No {
    return Intl.message("Item No :", name: 'Item_No');
  }

  String get AddToCart {
    return Intl.message("Add to Cart", name: 'AddToCart');
  }

  String get id {
    return Intl.message("Id :", name: 'id');
  }

  String get Added {
    return Intl.message("Added to Cart", name: 'Added');
  }

  String get Username {
    return Intl.message("Username", name: 'Username');
  }

  String get Username_Enter {
    return Intl.message("Enter Your Username", name: 'Username_Enter');
  }

  String get Edit {
    return Intl.message("Edit", name: 'Edit');
  }

  String get World_Stones {
    return Intl.message("World of Stones Plates", name: 'World_Stones');
  }

  String get Category_ {
    return Intl.message("Category :", name: 'Category_');
  }

  String get Description {
    return Intl.message("Description", name: 'Description');
  }

  String get Available_Sizes {
    return Intl.message("Available Sizes", name: 'Available_Sizes');
  }

  String get Unit_Dimension {
    return Intl.message("Unit Dimension", name: 'Unit_Dimension');
  }

  String get Horizontal {
    return Intl.message("Horizontal", name: 'Horizontal');
  }

  String get Vertical {
    return Intl.message("Vertical", name: 'Vertical');
  }

  String get Wall_Width {
    return Intl.message("Wall Width", name: 'Wall_Width');
  }

  String get Wall_Height {
    return Intl.message("Wall Height", name: 'Wall_Height');
  }

  String get Required_Quantity {
    return Intl.message("Required Quantity", name: 'Required_Quantity');
  }

  String get Required_Quantity_Planning {
    return Intl.message("Required Quantity for Planning",
        name: 'Required_Quantity_Planning');
  }

  String get Required_Quantity_Enter {
    return Intl.message("Enter Required Quantity",
        name: 'Required_Quantity_Enter');
  }

  String get Total_Price_ {
    return Intl.message("Total Price :", name: 'Total_Price_');
  }

  String get Available_now {
    return Intl.message("Available now", name: 'Available_now');
  }

  String get Days_Info {
    return Intl.message(
        "only , you can order and the rest will be available after 25 days",
        name: 'Days_Info');
  }

  String get Share {
    return Intl.message("Share", name: 'Share');
  }

  String get Payment_Enter {
    return Intl.message("Enter Payment Details", name: 'Payment_Enter');
  }

  String get On_Delivery {
    return Intl.message("On Delivery", name: 'On_Delivery');
  }

  String get Online {
    return Intl.message("Online(Coming Soon)", name: 'Online');
  }

  String get Finish {
    return Intl.message("Finish", name: 'Finish');
  }

  String get Accept_Privacy {
    return Intl.message("By Clicking Finish you accept the privacy policy",
        name: 'Accept_Privacy');
  }

  String get Receipt {
    return Intl.message("Receipt", name: 'Receipt');
  }

  String get Discount_Percent {
    return Intl.message("Discount Percent", name: 'Discount_Percent');
  }

  String get Quantity_total {
    return Intl.message("Quantity for 1m to total quantity in one receipt",
        name: 'Quantity_total');
  }

  String get Greater {
    return Intl.message("Greater", name: 'Greater');
  }

  String get Discount_Tax {
    return Intl.message(
        "Discount is Applied before Applying the Additional Tax 15%",
        name: 'Discount_Tax');
  }

  String get Order_No {
    return Intl.message("Order No", name: 'Order_No');
  }

  String get Order_Date {
    return Intl.message("Order Date", name: 'Order_Date');
  }

  String get Customer_Name {
    return Intl.message("Customer Name", name: 'Customer_Name');
  }

  String get Shipment_Co {
    return Intl.message("Shipment Co", name: 'Shipment_Co');
  }

  String get Shipment_Price {
    return Intl.message("Shipment Price", name: 'Shipment_Price');
  }

  String get Card_Type {
    return Intl.message("Card Type", name: 'Card_Type');
  }

  String get Product {
    return Intl.message("Product", name: 'Product');
  }

  String get Required_Quantity_ {
    return Intl.message("Required Quantity:", name: 'Required_Quantity_');
  }

  String get Receipt_Discount {
    return Intl.message("Receipt Discount", name: 'Receipt_Discount');
  }

  String get After_Discount {
    return Intl.message("Total after Discount", name: 'After_Discount');
  }

  String get Additional_Value {
    return Intl.message("Additional Value", name: 'Additional_Value');
  }

  String get Total_Price {
    return Intl.message("Total Price", name: 'Total_Price');
  }

  String get Register_as_Company {
    return Intl.message("Register as Company", name: 'Register_as_Company');
  }

  String get Register_as_Individual {
    return Intl.message("Register as Individual",
        name: 'Register_as_Individual');
  }

  String get Register {
    return Intl.message("Register", name: 'Register');
  }

  String get Choose_Image {
    return Intl.message("Please Choose Image", name: 'Choose_Image');
  }

  String get MissingFields {
    return Intl.message("Please Enter Missing Fields", name: 'MissingFields');
  }

  String get Language {
    return Intl.message("Language", name: 'Language');
  }

  String get English {
    return Intl.message("العربية", name: 'English');
  }

  String get Rate_App {
    return Intl.message("Rate App", name: 'Rate_App');
  }

  String get Share_App {
    return Intl.message("Share App", name: 'Share_App');
  }

  String get Open {
    return Intl.message("Open", name: 'Open');
  }

  String get No_Products {
    return Intl.message("No Products", name: 'No_Products');
  }

  String get Enter_Code {
    return Intl.message("Enter the code sent to", name: 'Enter_Code');
  }

  String get Verification_Code {
    return Intl.message("Verification Code", name: 'Verification_Code');
  }

  String get Code {
    return Intl.message("Code", name: 'Code');
  }

  String get Send_Again {
    return Intl.message("Send Code Again ?", name: 'Send_Again');
  }

  String get Confirm {
    return Intl.message("Confirm", name: 'Confirm');
  }

  String get Code_Error {
    return Intl.message("Code Error", name: 'Code_Error');
  }

  String get Enter_Received {
    return Intl.message("Enter Received Code", name: 'Enter_Received');
  }

  String get No_Address {
    return Intl.message("No Addresses Registered", name: 'No_Address');
  }

  String get Loading_Text {
    return Intl.message("Loading Text...", name: 'Loading_Text');
  }

  String get Done {
    return Intl.message("Done", name: 'Done');
  }

  String get Accept {
    return Intl.message("I Accept Privacy Policy", name: 'Accept');
  }

  String get Home {
    return Intl.message("Home", name: 'Home');
  }

  String get Catalogs {
    return Intl.message("Catalogs", name: 'Catalogs');
  }

  String get History {
    return Intl.message("History", name: 'History');
  }

  String get Settings {
    return Intl.message("Settings", name: 'Settings');
  }

  String get Signout {
    return Intl.message("Signout", name: 'Signout');
  }

  String get Welcome {
    return Intl.message("Welcome", name: 'Welcome');
  }

  String get Login_First {
    return Intl.message("Please Login First to Proceed", name: 'Login_First');
  }

  String get Create_Account {
    return Intl.message("Create Account", name: 'Create_Account');
  }

  String get Enter_Wall {
    return Intl.message("Enter the wall width and height", name: 'Enter_Wall');
  }

  String get item {
    return Intl.message("item", name: 'item');
  }
}

class SpecificLocalizationDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  final Locale overriddenLocale;

  SpecificLocalizationDelegate(this.overriddenLocale);

  @override
  bool isSupported(Locale locale) => overriddenLocale != null;

  @override
  Future<AppLocalizations> load(Locale locale) =>
      AppLocalizations.load(overriddenLocale);

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) => true;
}

class FallbackCupertinoLocalisationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
  const FallbackCupertinoLocalisationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ar'].contains(locale.languageCode);

  @override
  Future<CupertinoLocalizations> load(Locale locale) =>
      SynchronousFuture<_DefaultCupertinoLocalizations>(
          _DefaultCupertinoLocalizations(locale));

  @override
  bool shouldReload(FallbackCupertinoLocalisationsDelegate old) => false;
}

class _DefaultCupertinoLocalizations extends DefaultCupertinoLocalizations {
  final Locale locale;

  _DefaultCupertinoLocalizations(this.locale);
}
