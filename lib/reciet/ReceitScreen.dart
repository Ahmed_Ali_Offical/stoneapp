import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/historyModel.dart';
import 'package:stone_app/model/receiptModel.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import '../applocalizations.dart';

class RecietScreen extends StatefulWidget {
  ReceiptModel receiptModel;
  OrderHistory historyReceiptModel;

  RecietScreen.history(this.historyReceiptModel);

  RecietScreen(this.receiptModel);

  @override
  State<StatefulWidget> createState() {
    return RecietScreenState();
  }
}

class RecietScreenState extends State<RecietScreen> {
  var modelData;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;

  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
    if (widget.receiptModel != null) {
      modelData = widget.receiptModel.data;
    } else {
      modelData = widget.historyReceiptModel;
    }
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));
    ScreenUtil.init(context, height: _height, width: _width);
    return WillPopScope(
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          new FallbackCupertinoLocalisationsDelegate(),
          //app-specific localization
          _specificLocalizationDelegate
        ],
        supportedLocales: [Locale('en'), Locale('ar')],
        locale: _specificLocalizationDelegate.overriddenLocale,
        theme: ThemeData(primarySwatch: MyColors.primary),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            brightness: Platform.isAndroid ? Brightness.dark : Brightness.light,
            leading: widget.receiptModel != null
                ? IconButton(
                    icon: Icon(Icons.home),
                    color: Colors.black,
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(context,
                          MaterialPageRoute(builder: (BuildContext context) {
                        return HomeScreen();
                      }), (Route<dynamic> route) => false);
                    },
                  )
                : IconButton(
                    icon: Icon(Icons.arrow_back),
                    color: Colors.black,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
            actions: [IconButton(icon: Icon(Icons.print), onPressed: _print)],
            iconTheme: new IconThemeData(color: Colors.black),
            title: Center(
              child: Text(
                appLocalizations.Receipt,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: ScreenUtil().setSp(15.0),
                  fontFamily: lan == "en" ? "Poppins" : "Arabic",
                ),
              ),
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                modelData.user.level == "company"
                    ? Card(
                        margin: EdgeInsets.all(_width * 0.02),
                        child: Column(
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      height: _height * 0.05,
                                      margin: EdgeInsets.only(
                                          left: _width * 0.02,
                                          right: _width * 0.02),
                                      child: Align(
                                        child: AutoSizeText(
                                          appLocalizations.Discount_Percent,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize:
                                                  ScreenUtil().setSp(15.0),
                                              color: Colors.white,
                                              fontFamily: lan == "en"
                                                  ? "Poppins"
                                                  : "Arabic",
                                              fontWeight: FontWeight.bold,
                                              decoration: TextDecoration.none),
                                        ),
                                        alignment: lan == "en"
                                            ? Alignment.centerLeft
                                            : Alignment.centerRight,
                                      ),
                                    ),
                                    flex: 1,
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: Text(
                                        appLocalizations.Quantity_total,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(12.0),
                                            color: Colors.white,
                                            fontFamily: lan == "en"
                                                ? "Poppins"
                                                : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none),
                                      ),
                                      margin: EdgeInsets.all(_height * 0.01),
                                    ),
                                    flex: 1,
                                  )
                                ],
                              ),
                              color: MyColors.appGray.withOpacity(0.6),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    height: _height * 0.05,
                                    margin: EdgeInsets.only(
                                        left: _width * 0.02,
                                        right: _width * 0.02),
                                    child: Align(
                                      child: AutoSizeText(
                                        "8%",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(15.0),
                                            color: Colors.black,
                                            fontFamily: lan == "en"
                                                ? "Poppins"
                                                : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none),
                                      ),
                                      alignment: Alignment.center,
                                    ),
                                  ),
                                  flex: 1,
                                ),
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      "1.50 - 199.0 % M2",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    margin: EdgeInsets.all(_height * 0.01),
                                  ),
                                  flex: 1,
                                )
                              ],
                            ),
                            Container(
                              height: 1.0,
                              color: MyColors.appGray.withOpacity(0.5),
                              width: double.infinity,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    height: _height * 0.05,
                                    margin: EdgeInsets.only(
                                        left: _width * 0.02,
                                        right: _width * 0.02),
                                    child: Align(
                                      child: AutoSizeText(
                                        "12%",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(15.0),
                                            color: Colors.black,
                                            fontFamily: lan == "en"
                                                ? "Poppins"
                                                : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none),
                                      ),
                                      alignment: Alignment.center,
                                    ),
                                  ),
                                  flex: 1,
                                ),
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      "199.01 - 399.0 % M2",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    margin: EdgeInsets.all(_height * 0.01),
                                  ),
                                  flex: 1,
                                )
                              ],
                            ),
                            Container(
                              height: 1.0,
                              color: MyColors.appGray.withOpacity(0.5),
                              width: double.infinity,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    height: _height * 0.05,
                                    margin: EdgeInsets.only(
                                        left: _width * 0.02,
                                        right: _width * 0.02),
                                    child: Align(
                                      child: AutoSizeText(
                                        "17%",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(15.0),
                                            color: Colors.black,
                                            fontFamily: lan == "en"
                                                ? "Poppins"
                                                : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none),
                                      ),
                                      alignment: Alignment.center,
                                    ),
                                  ),
                                  flex: 1,
                                ),
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      "399.01 - ${appLocalizations.Greater} M2",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    margin: EdgeInsets.all(_height * 0.01),
                                  ),
                                  flex: 1,
                                )
                              ],
                            ),
                          ],
                        ),
                      )
                    : Container(),
                    Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.info,
                              color: MyColors.appYellow,
                              size: _width * 0.04,
                            ),
                            Expanded(
                              child: Container(
                                child: Text(
                                  appLocalizations.Discount_Tax,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(12.0),
                                      color: Colors.black,
                                      fontFamily:
                                          lan == "en" ? "Poppins" : "Arabic",
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.none),
                                ),
                                margin: EdgeInsets.all(_height * 0.01),
                              ),
                              flex: 1,
                            )
                          ],
                        ),
                        margin: EdgeInsets.only(
                            left: _width * 0.02, right: _width * 0.02),
                      ),
                Card(
                  margin: EdgeInsets.all(_width * 0.02),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: _height * 0.05,
                                margin: EdgeInsets.only(
                                    left: _width * 0.02, right: _width * 0.02),
                                child: Align(
                                  child: AutoSizeText(
                                    appLocalizations.Receipt,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(15.0),
                                        color: Colors.white,
                                        fontFamily:
                                            lan == "en" ? "Poppins" : "Arabic",
                                        fontWeight: FontWeight.bold,
                                        decoration: TextDecoration.none),
                                  ),
                                  alignment: lan == "en"
                                      ? Alignment.centerLeft
                                      : Alignment.centerRight,
                                ),
                              ),
                              flex: 1,
                            ),
                            Container(
                              child: Image.asset(
                                'assets/images/logo2.png',
                                height: _height * 0.05,
                              ),
                              margin: EdgeInsets.all(_height * 0.01),
                            )
                          ],
                        ),
                        color: MyColors.appGray.withOpacity(0.6),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  height: _height * 0.04,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      appLocalizations.Order_No,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(15.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                                Container(
                                  height: _height * 0.03,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      modelData.orderNo,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  height: _height * 0.04,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      appLocalizations.Order_Date,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(15.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                                Container(
                                  height: _height * 0.03,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      modelData.user.createdAt,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                      Container(
                        height: 1.0,
                        color: MyColors.appGray.withOpacity(0.5),
                        width: double.infinity,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  height: _height * 0.04,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      modelData.user.level == "company"
                                          ? appLocalizations.Company_Name
                                          : appLocalizations.Customer_Name,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(15.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                                Container(
                                  height: _height * 0.03,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      modelData.user.level == "company"
                                          ? modelData.user.companyName ?? ""
                                          : modelData.user.name,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  height: _height * 0.04,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      appLocalizations.Phone,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(15.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                                Container(
                                  height: _height * 0.03,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      modelData.user.phone,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                      Container(
                        height: 1.0,
                        color: MyColors.appGray.withOpacity(0.5),
                        width: double.infinity,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  height: _height * 0.04,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      appLocalizations.Shipment_Co,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(15.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  height: _height * 0.03,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      lan == "en"
                                          ? modelData.shipCompany.name.en
                                          : modelData.shipCompany.name.ar,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                      Container(
                        height: 1.0,
                        color: MyColors.appGray.withOpacity(0.5),
                        width: double.infinity,
                      ),
                      modelData.shipPrice.toString() != "0"
                          ? Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    children: [
                                      Container(
                                        height: _height * 0.04,
                                        margin: EdgeInsets.only(
                                            left: _width * 0.02,
                                            right: _width * 0.02),
                                        child: Align(
                                          child: AutoSizeText(
                                            appLocalizations.Shipment_Price,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize:
                                                    ScreenUtil().setSp(15.0),
                                                color: Colors.black,
                                                fontFamily: lan == "en"
                                                    ? "Poppins"
                                                    : "Arabic",
                                                fontWeight: FontWeight.bold,
                                                decoration:
                                                    TextDecoration.none),
                                          ),
                                          alignment: lan == "en"
                                              ? Alignment.centerLeft
                                              : Alignment.centerRight,
                                        ),
                                      ),
                                    ],
                                  ),
                                  flex: 1,
                                ),
                                Expanded(
                                  child: Column(
                                    children: [
                                      Container(
                                        height: _height * 0.03,
                                        margin: EdgeInsets.only(
                                            left: _width * 0.02,
                                            right: _width * 0.02),
                                        child: Align(
                                          child: AutoSizeText(
                                            "${modelData.shipPrice.toString()} SR",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize:
                                                    ScreenUtil().setSp(12.0),
                                                color: Colors.black,
                                                fontFamily: lan == "en"
                                                    ? "Poppins"
                                                    : "Arabic",
                                                decoration:
                                                    TextDecoration.none),
                                          ),
                                          alignment: lan == "en"
                                              ? Alignment.centerLeft
                                              : Alignment.centerRight,
                                        ),
                                      ),
                                    ],
                                  ),
                                  flex: 1,
                                ),
                              ],
                            )
                          : Container(),
                      modelData.shipPrice.toString() != "0"
                          ? Container(
                              height: 1.0,
                              color: MyColors.appGray.withOpacity(0.5),
                              width: double.infinity,
                            )
                          : Container(),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  height: _height * 0.04,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      appLocalizations.Card_Type,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(15.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  height: _height * 0.03,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      modelData.payment,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                      Container(
                        height: 1.0,
                        color: MyColors.appGray.withOpacity(0.5),
                        width: double.infinity,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  height: _height * 0.04,
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      appLocalizations.Address,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(15.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                      left: _width * 0.02,
                                      right: _width * 0.02),
                                  child: Align(
                                    child: AutoSizeText(
                                      widget.receiptModel != null
                                          ? modelData.ship.address ??
                                              "Unidentified"
                                          : "Unidentified",
                                      textAlign: TextAlign.start,
                                      maxLines: 4,
                                      softWrap: true,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.0),
                                          color: Colors.black,
                                          fontFamily: lan == "en"
                                              ? "Poppins"
                                              : "Arabic",
                                          decoration: TextDecoration.none),
                                    ),
                                    alignment: lan == "en"
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                  ),
                                ),
                              ],
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                      Card(
                        child: Column(
                          children: [
                            Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: MyColors.appYellow,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(9.0),
                                    topRight: Radius.circular(9.0)),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Container(
                                          height: _height * 0.05,
                                          margin: EdgeInsets.only(
                                              left: _width * 0.02,
                                              right: _width * 0.02),
                                          child: Align(
                                            child: AutoSizeText(
                                              appLocalizations.Product,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize:
                                                      ScreenUtil().setSp(14.0),
                                                  color: Colors.black,
                                                  fontFamily: lan == "en"
                                                      ? "Poppins"
                                                      : "Arabic",
                                                  fontWeight: FontWeight.bold,
                                                  decoration:
                                                      TextDecoration.none),
                                            ),
                                            alignment: Alignment.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                    flex: 1,
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Container(
                                          height: _height * 0.05,
                                          margin: EdgeInsets.only(
                                              left: _width * 0.02,
                                              right: _width * 0.02),
                                          child: Align(
                                            child: AutoSizeText(
                                              appLocalizations.Price,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  fontSize:
                                                      ScreenUtil().setSp(14.0),
                                                  color: Colors.black,
                                                  fontFamily: lan == "en"
                                                      ? "Poppins"
                                                      : "Arabic",
                                                  fontWeight: FontWeight.bold,
                                                  decoration:
                                                      TextDecoration.none),
                                            ),
                                            alignment: Alignment.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                    flex: 1,
                                  ),
                                ],
                              ),
                            ),
                            ListView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: modelData.products.length,
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context, int index) {
                                return Container(
                                  child: Column(
                                    children: [
                                      Padding(
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Column(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                      left: _width * 0.02,
                                                      top: _width * 0.01,
                                                      right: _width * 0.02,
                                                    ),
                                                    child: Align(
                                                      child: AutoSizeText(
                                                        lan == "en"
                                                            ? modelData
                                                                .products[index]
                                                                .title
                                                                .en
                                                            : modelData
                                                                .products[index]
                                                                .title
                                                                .ar,
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(
                                                                        14.0),
                                                            color: Colors.black,
                                                            fontFamily:
                                                                lan == "en"
                                                                    ? "Poppins"
                                                                    : "Arabic",
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            decoration:
                                                                TextDecoration
                                                                    .none),
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: _width * 0.02,
                                                        top: _width * 0.01,
                                                        right: _width * 0.02),
                                                    child: Align(
                                                      child: AutoSizeText(
                                                        "${appLocalizations.Required_Quantity_} ${modelData.products[index].pivot.qty}",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(
                                                                        12.0),
                                                            color: Colors.black,
                                                            fontFamily:
                                                                lan == "en"
                                                                    ? "Poppins"
                                                                    : "Arabic",
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            decoration:
                                                                TextDecoration
                                                                    .none),
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            ),
                                            Expanded(
                                              child: Column(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: _width * 0.02,
                                                        top: _width * 0.01,
                                                        right: _width * 0.02),
                                                    child: Align(
                                                      child: AutoSizeText(
                                                        "${(modelData.products[index].price * int.parse(modelData.products[index].pivot.qty))} SR",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(
                                                                        14.0),
                                                            color: Colors.black,
                                                            fontFamily:
                                                                lan == "en"
                                                                    ? "Poppins"
                                                                    : "Arabic",
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            decoration:
                                                                TextDecoration
                                                                    .none),
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: _width * 0.02,
                                                        top: _width * 0.01,
                                                        right: _width * 0.02),
                                                    child: Align(
                                                      child: AutoSizeText(
                                                        "${modelData.products[index].price.toString()} SR for 1m",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(
                                                                        12.0),
                                                            color: Colors.black,
                                                            fontFamily:
                                                                lan == "en"
                                                                    ? "Poppins"
                                                                    : "Arabic",
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            decoration:
                                                                TextDecoration
                                                                    .none),
                                                      ),
                                                      alignment:
                                                          Alignment.center,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            ),
                                          ],
                                        ),
                                        padding: EdgeInsets.only(
                                            bottom: _height * 0.01),
                                      ),
                                      Container(
                                        height: 1.0,
                                        color:
                                            MyColors.appGray.withOpacity(0.5),
                                        width: double.infinity,
                                      ),
                                    ],
                                  ),
                                  color: MyColors.appYellow.withOpacity(0.2),
                                );
                              },
                            ),
//                      Container(color: MyColors.appYellow.withOpacity(0.2),child: Row(
//                        children: [
//                          Expanded(
//                            child: Column(
//                              children: [
//                                Container(
//                                  height: _height * 0.05,
//                                  margin: EdgeInsets.only(
//                                      left: _width * 0.02,
//                                      right: _width * 0.02),
//                                  child: Align(
//                                    child: AutoSizeText(
//                                      "Receipt Total",
//                                      textAlign: TextAlign.start,
//                                      style: TextStyle(
//                                          fontSize: ScreenUtil().setSp(14.0),
//                                          color: Colors.black,
//                                          fontFamily: lan == "en" ? "Poppins" : "Arabic",
//                                          fontWeight: FontWeight.bold,
//                                          decoration: TextDecoration.none),
//                                    ),
//                                    alignment: Alignment.center,
//                                  ),
//                                ),
//                              ],
//                            ),
//                            flex: 1,
//                          ),
//                          Expanded(
//                            child: Column(
//                              children: [
//                                Container(
//                                  height: _height * 0.05,
//                                  margin: EdgeInsets.only(
//                                      left: _width * 0.02,
//                                      right: _width * 0.02),
//                                  child: Align(
//                                    child: AutoSizeText(
//                                      "${modelData.totalPrice} SR",
//                                      textAlign: TextAlign.start,
//                                      style: TextStyle(
//                                          fontSize: ScreenUtil().setSp(14.0),
//                                          color: Colors.black,
//                                          fontFamily: lan == "en" ? "Poppins" : "Arabic",
//                                          fontWeight: FontWeight.bold,
//                                          decoration: TextDecoration.none),
//                                    ),
//                                    alignment: Alignment.center,
//                                  ),
//                                ),
//                              ],
//                            ),
//                            flex: 1,
//                          ),
//                        ],
//                      ),),
                            modelData.user.level == "company"
                                ? Container(
                                    color: MyColors.appYellow.withOpacity(0.2),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Container(
                                                height: _height * 0.05,
                                                margin: EdgeInsets.only(
                                                    left: _width * 0.02,
                                                    right: _width * 0.02),
                                                child: Align(
                                                  child: AutoSizeText(
                                                    appLocalizations
                                                        .Receipt_Discount,
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        fontSize: ScreenUtil()
                                                            .setSp(14.0),
                                                        color: Colors.black,
                                                        fontFamily: lan == "en"
                                                            ? "Poppins"
                                                            : "Arabic",
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        decoration:
                                                            TextDecoration
                                                                .none),
                                                  ),
                                                  alignment: Alignment.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Container(
                                                height: _height * 0.05,
                                                margin: EdgeInsets.only(
                                                    left: _width * 0.02,
                                                    right: _width * 0.02),
                                                child: Align(
                                                  child: AutoSizeText(
                                                    "${modelData.discount} SR",
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        fontSize: ScreenUtil()
                                                            .setSp(14.0),
                                                        color: Colors.black,
                                                        fontFamily: lan == "en"
                                                            ? "Poppins"
                                                            : "Arabic",
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        decoration:
                                                            TextDecoration
                                                                .none),
                                                  ),
                                                  alignment: Alignment.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                          flex: 1,
                                        ),
                                      ],
                                    ),
                                  )
                                : Container(),
                            modelData.user.level == "company"
                                ? Container(
                                    color: MyColors.appYellow.withOpacity(0.2),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Container(
                                                height: _height * 0.05,
                                                margin: EdgeInsets.only(
                                                    left: _width * 0.02,
                                                    right: _width * 0.02),
                                                child: Align(
                                                  child: AutoSizeText(
                                                    appLocalizations
                                                        .After_Discount,
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        fontSize: ScreenUtil()
                                                            .setSp(14.0),
                                                        color: Colors.black,
                                                        fontFamily: lan == "en"
                                                            ? "Poppins"
                                                            : "Arabic",
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        decoration:
                                                            TextDecoration
                                                                .none),
                                                  ),
                                                  alignment: Alignment.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Container(
                                                height: _height * 0.05,
                                                margin: EdgeInsets.only(
                                                    left: _width * 0.02,
                                                    right: _width * 0.02),
                                                child: Align(
                                                  child: AutoSizeText(
                                                    "${modelData.totalAfterDiscount} SR",
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        fontSize: ScreenUtil()
                                                            .setSp(14.0),
                                                        color: Colors.black,
                                                        fontFamily: lan == "en"
                                                            ? "Poppins"
                                                            : "Arabic",
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        decoration:
                                                            TextDecoration
                                                                .none),
                                                  ),
                                                  alignment: Alignment.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                          flex: 1,
                                        ),
                                      ],
                                    ),
                                  )
                                : Container(),
                            modelData.user.level == "company"
                                ? Container(
                                    color: MyColors.appYellow.withOpacity(0.2),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Container(
                                                height: _height * 0.05,
                                                margin: EdgeInsets.only(
                                                    left: _width * 0.02,
                                                    right: _width * 0.02),
                                                child: Align(
                                                  child: AutoSizeText(
                                                    appLocalizations
                                                        .Additional_Value,
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        fontSize: ScreenUtil()
                                                            .setSp(14.0),
                                                        color: Colors.black,
                                                        fontFamily: lan == "en"
                                                            ? "Poppins"
                                                            : "Arabic",
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        decoration:
                                                            TextDecoration
                                                                .none),
                                                  ),
                                                  alignment: Alignment.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Container(
                                                height: _height * 0.05,
                                                margin: EdgeInsets.only(
                                                    left: _width * 0.02,
                                                    right: _width * 0.02),
                                                child: Align(
                                                  child: AutoSizeText(
                                                    "${modelData.tax} SR",
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                        fontSize: ScreenUtil()
                                                            .setSp(14.0),
                                                        color: Colors.black,
                                                        fontFamily: lan == "en"
                                                            ? "Poppins"
                                                            : "Arabic",
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        decoration:
                                                            TextDecoration
                                                                .none),
                                                  ),
                                                  alignment: Alignment.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                          flex: 1,
                                        ),
                                      ],
                                    ),
                                  )
                                : Container(),
                            Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: MyColors.appGray,
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(9.0),
                                    bottomRight: Radius.circular(9.0)),
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Container(
                                          height: _height * 0.05,
                                          margin: EdgeInsets.only(
                                              left: _width * 0.02,
                                              right: _width * 0.02),
                                          child: Align(
                                            child: AutoSizeText(
                                              appLocalizations.Total_Price,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  fontSize:
                                                      ScreenUtil().setSp(14.0),
                                                  color: Colors.black,
                                                  fontFamily: lan == "en"
                                                      ? "Poppins"
                                                      : "Arabic",
                                                  fontWeight: FontWeight.bold,
                                                  decoration:
                                                      TextDecoration.none),
                                            ),
                                            alignment: Alignment.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                    flex: 1,
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Container(
                                          height: _height * 0.05,
                                          margin: EdgeInsets.only(
                                              left: _width * 0.02,
                                              right: _width * 0.02),
                                          child: Align(
                                            child: AutoSizeText(
                                              "${modelData.totalPrice} SR",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  fontSize:
                                                      ScreenUtil().setSp(14.0),
                                                  color: Colors.black,
                                                  fontFamily: lan == "en"
                                                      ? "Poppins"
                                                      : "Arabic",
                                                  fontWeight: FontWeight.bold,
                                                  decoration:
                                                      TextDecoration.none),
                                            ),
                                            alignment: Alignment.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                    flex: 1,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(9.0)),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      onWillPop: _onBackPressed,
    );
  }

  Future<bool> _onBackPressed() {
    return Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return HomeScreen();
    }), (Route<dynamic> route) => false);
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }

  void _print() async {
     showDialog(
            context: context,
            builder: (BuildContext context){
              return Dialog(child: new Container(
                color: Colors.white,
                width: ScreenUtil().setWidth(32),
                height: ScreenUtil().setHeight(64),
                child: new Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: new Center(child: new CircularProgressIndicator())),
              ),);
            });
    var addressesRequest = await post(Urls.sendmail, headers: {
      'Authorization': "Bearer ${user.token}",
    }, body: {
      'order_id': widget.historyReceiptModel != null
          ? widget.historyReceiptModel.id.toString()
          : widget.receiptModel.data.id.toString(),
    });
     Navigator.of(context, rootNavigator: true).pop();
    Toast.show(json.decode(addressesRequest.body)['message'], context,
        duration: Toast.LENGTH_LONG);
  }
}
