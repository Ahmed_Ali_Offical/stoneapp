import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/addaddress/addaddress.dart';
import 'package:stone_app/model/user.dart';
import 'package:stone_app/model/userModel.dart';
import 'package:stone_app/payment/paymentScreen.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class UserAddresses extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return UserAddressesState();
  }
}

class UserAddressesState extends State<UserAddresses> {
  List<UserAddress> addresses = [];
  int singleAddress = 0;
  bool _load = true;
  User userAdd;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));
    return MaterialApp(localizationsDelegates: [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      new FallbackCupertinoLocalisationsDelegate(),
      //app-specific localization
      _specificLocalizationDelegate
    ],
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.black),
        brightness: Platform.isAndroid? Brightness.dark:Brightness.light,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            appLocalizations.Add_Address,
            style: TextStyle(color: Colors.black, fontFamily: lan == "en" ? "Poppins" : "Arabic",),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
                return AddAddress();
              })).then((value) => {
            setState(() {
              getUser();
            })
          });
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: MyColors.primary,
      ),
      body: _load
          ? Reusable.showLoader(_load)
          : addresses != null && addresses.isNotEmpty
          ? Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: addresses
                    .map((e) => new RadioListTile<UserAddress>(
                  title: Container(
                    height: _height * 0.02,
                    child: AutoSizeText(
                      e.address ?? "Unidentified",
                      style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black,
                          fontFamily: lan == "en" ? "Poppins" : "Arabic",
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.none),
                    ),
                  ),
                  value: e,
                  groupValue: addresses[singleAddress],
                  onChanged: (value) {
                    setState(() {
                      singleAddress =
                          addresses.indexOf(value);
                    });
                  },
                ))
                    .toList(),
              ),
            ),
            flex: 1,
          ),
          GestureDetector(
            child: Container(
              color: MyColors.appYellow,
              height: _height * 0.06,
              child: Center(
                child: AutoSizeText(
                  appLocalizations.Complete_Purchase,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.white,
                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none),
                ),
              ),
              width: double.infinity,
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                    return PaymentScreen(addresses[singleAddress].cityId,
                        addresses[singleAddress].id);
                  }));
            },
          ),
        ],
      )
          : Container(
        width: double.infinity,
        height: double.infinity,
        child: Center(
          child: Reusable.flexibleText(
              _height * 0.02, appLocalizations.No_Address, null),
        ),
      ),
    ),);
  }

  void getUser() async {
    var prefs = await SharedPreferences.getInstance();
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        userAdd = User(
                prefs.getInt("id") ?? 0,
                prefs.getString("name") ?? "",
                prefs.getString("email") ?? "",
                prefs.getString("phone") ?? "",
                prefs.getString("level") ?? "",
                prefs.getString("token") ?? "",
                prefs.getString("image")) ??
            "";
      });

    getUserAddresses();
  }

  void getUserAddresses() async {
    print("User Addresses Bearer ${userAdd.token}");
    _load = true;
    var addressesRequest = await get(Urls.user,
        headers: {'Authorization': "Bearer ${userAdd.token}"});
    UserModel userModel =
        new UserModel.fromJson(jsonDecode(addressesRequest.body));
    setState(() {
      _load = false;
      addresses = userModel.userAddress;
    });
  }
}
