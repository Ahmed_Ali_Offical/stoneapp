import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:stone_app/applocalizations.dart';
import 'package:stone_app/cart/cart.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/model/productmodel.dart';
import 'package:stone_app/register/mainRegister.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/clickes.dart';
import 'package:stone_app/values/colors.dart';

class Reusable {
  static Widget showLoader(bool _load) {
    Widget loadingIndicator = _load
        ? new Container(
            color: Colors.white,
            width: double.infinity,
            height: double.infinity,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();

    return loadingIndicator;
  }

  static Widget loginDialog(BuildContext context, AppLocalizations loc,
      double width, height, Function(int val) callBack) {
    ScreenUtil.init(context, width: width, height: height);
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      //this right here
      child: Container(
        height: width * 0.8,
        width: width * 0.8,
        child: Padding(
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/images/logo.png',
                    width: width * 0.15,
                    height: width * 0.15,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: height * 0.02),
                  ),
                  Text(
                    loc.Welcome,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(15.0),
                        color: Colors.black,
                        fontFamily: lan == "en" ? "Poppins" : "Arabic",
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: height * 0.02),
                  ),
                  Text(
                    loc.Login_First,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(13.0),
                        color: Colors.black,
                        fontFamily: lan == "en" ? "Poppins" : "Arabic",
                        decoration: TextDecoration.none),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: height * 0.02),
                  ),
                  RaisedButton(
                    child: Text(
                      loc.Login,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(12.0),
                          color: Colors.white,
                          fontFamily: lan == "en" ? "Poppins" : "Arabic",
                          decoration: TextDecoration.none),
                    ),
                    color: MyColors.appYellow,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0)),
                    onPressed: () {
                      Navigator.pop(context);
                      callBack.call(Clicks.LocginClick);
                    },
                  ),
                  Container(
                    margin: EdgeInsets.only(top: height * 0.02),
                  ),
                  GestureDetector(
                    child: Text(
                      loc.Create_Account,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(10.0),
                          color: MyColors.appYellow,
                          fontFamily: lan == "en" ? "Poppins" : "Arabic",
                          decoration: TextDecoration.none),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (BuildContext context) {
                        return RegisterScreen();
                      }));
                    },
                  )
                ],
              ),
              Align(
                child: GestureDetector(
                  child: Icon(Icons.close),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                alignment: Alignment.topRight,
              ),
            ],
            alignment: Alignment.center,
          ),
          padding: EdgeInsets.all(width * 0.03),
        ),
      ),
    );
  }

  static Widget showPrivacy(BuildContext context, String text) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: SingleChildScrollView(
          child: Padding(padding: EdgeInsets.all(18.0),child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(12.0),
                color: MyColors.primary,
                fontFamily: lan == "en" ? "Poppins" : "Arabic",
                decoration: TextDecoration.none),
          ),),
      ),
    );
  }

  static Widget errorDialog(
      BuildContext context, double width, height, List<String> error) {
    ScreenUtil.init(context, width: width, height: height);

    List<Widget> errors = [];

    for (String i in error) {
      errors.add(
        Text(
          i,
          style: TextStyle(
              fontSize: ScreenUtil().setSp(12.0),
              color: Colors.red,
              fontFamily: lan == "en" ? "Poppins" : "Arabic",
              decoration: TextDecoration.none),
        ),
      );
    }

    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      //this right here
      child: Container(
        height: width * 0.8,
        width: width * 0.8,
        child: Padding(
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Error",
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(15.0),
                        color: Colors.red,
                        fontFamily: lan == "en" ? "Poppins" : "Arabic",
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none),
                  ),
                  Column(
                    children: errors,
                  ),
                ],
              ),
              Align(
                child: GestureDetector(
                  child: Icon(Icons.close),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                alignment: Alignment.topRight,
              ),
            ],
            alignment: Alignment.center,
          ),
          padding: EdgeInsets.all(width * 0.03),
        ),
      ),
    );
  }

  static PreferredSizeWidget getAppBar(BuildContext context, double _height,
      double _width, int cartItemsCount, String title) {
    ScreenUtil.init(context, width: _width, height: _height);
    return AppBar(
      backgroundColor: Colors.white,
      iconTheme: new IconThemeData(color: Colors.black),
      leading: IconButton(
        icon: Icon(Icons.arrow_back),
        color: Colors.black,
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      actions: <Widget>[
        GestureDetector(
          child: Center(
            child: Container(
              child: new Stack(
                children: <Widget>[
                  Icon(
                    Icons.shopping_cart,
                    color: MyColors.appGray,
                  ),
                  Container(
                    width: _width * 0.035,
                    height: _width * 0.035,
                    child: FittedBox(
                      child: AutoSizeText(
                        cartItemsCount.toString(),
                      ),
                      fit: BoxFit.fitHeight,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(50),
                      ),
                      color: Colors.brown,
                    ),
                  )
                ],
              ),
              margin:
                  EdgeInsets.only(left: _width * 0.05, right: _width * 0.05),
            ),
          ),
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Cart()));
          },
        ),
      ],
      title: Center(
        child: Text(
          title,
          style: TextStyle(
            color: Colors.black,
            fontSize: ScreenUtil().setSp(15.0),
            fontFamily: lan == "en" ? "Poppins" : "Arabic",
          ),
        ),
      ),
    );
  }

  static Widget flexibleText(double height, String text, EdgeInsets margin) {
    return Container(
      height: height,
      margin: margin,
      child: FittedBox(
        child: AutoSizeText(
          text,
          style: TextStyle(
              fontSize: 15.0,
              color: Colors.black,
              fontFamily: lan == "en" ? "Poppins" : "Arabic",
              fontWeight: FontWeight.bold,
              decoration: TextDecoration.none),
        ),
        fit: BoxFit.fitHeight,
      ),
    );
  }

  static bool validateStrings(List<String> inputs) {
    for (String item in inputs) {
      if (item == null || item.isEmpty) return false;
    }
    return true;
  }

  static Widget productsCard(_height, _width, List<ProductModel> productsModel,
      Function(int, Inner) callBack) {
    return Container(
      width: double.infinity,
      child: GridView.count(
        crossAxisCount: 2,
        crossAxisSpacing: 15,
        mainAxisSpacing: 15,
        childAspectRatio: 0.7,
        scrollDirection: Axis.vertical,
        children: List.generate(productsModel.length, (index) {
          return GestureDetector(
            child: Wrap(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      height: _height * 0.2,
                      child: ClipRRect(
                        child: Image.network(
                          productsModel[index].photo,
                          height: double.infinity,
                          fit: BoxFit.cover,
                          loadingBuilder: (BuildContext context, Widget child,
                              ImageChunkEvent loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CircularProgressIndicator(
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        loadingProgress.expectedTotalBytes
                                    : null,
                              ),
                            );
                          },
                        ),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            topRight: Radius.circular(8.0)),
                      ),
                    ),
                    Reusable.flexibleText(
                        _height * 0.015,
                        lan == "en"
                            ? productsModel[index].title.en
                            : productsModel[index].title.ar,
                        EdgeInsets.all(_height * 0.01)),
                    Reusable.flexibleText(_height * 0.015,
                        "Id : ${productsModel[index].id}", null),
                    Container(
                      child: Padding(
                        child: RaisedButton(
                          color: MyColors.appYellow,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              AutoSizeText(
                                "Add to Cart",
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.white,
                                    fontFamily:
                                        lan == "en" ? "Poppins" : "Arabic",
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.none),
                              ),
                              Padding(
                                child: Icon(
                                  Icons.shopping_cart,
                                  color: Colors.white,
                                ),
                                padding: EdgeInsets.all(_width * 0.001),
                              )
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(8.0),
                                bottomRight: Radius.circular(8.0)),
                          ),
                          onPressed: () {
                            var item = productsModel[index];
                            print("Clicked");
                            callBack(
                                4,
                                Inner(
                                    item.id,
                                    item.photo,
                                    lan == "en" ? item.title.en : item.title.ar,
                                    lan == "en"
                                        ? item.content.en
                                        : item.content.ar,
                                    "1.0",
                                    "1.0",
                                    "1.0",
                                    item.price,
                                    1));
                          },
                        ),
                        padding: EdgeInsets.only(bottom: _height * 0.005),
                      ),
                      height: _height * 0.04,
                    )
                  ],
                )
              ],
            ),
            onTap: () {
              var item = productsModel[index];
              callBack.call(
                  14,
                  Inner(
                      item.id,
                      item.photo,
                      lan == "en" ? item.title.en : item.title.ar,
                      lan == "en" ? item.content.en : item.content.ar,
                      "1.0",
                      "1.0",
                      "1.0",
                      item.price,
                      1));
            },
          );
        }),
      ),
      margin:
          EdgeInsets.fromLTRB(_width * 0.01, _height * 0.001, _width * 0.01, 0),
    );
  }

  static Function(int) loginCallBack;
  static Function(int, Inner) cartCallBack;
}
