import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:stone_app/model/singleProductModel.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/views/reusable.dart';

class IndicatorSLider extends StatefulWidget {

  Function(String) callBack;
  IndicatorSLider(this.callBack,this.imgList, this.titleList);

  List<String> imgList = [];
  List<String> titleList = [];

  @override
  _IndicatorSLiderState createState() => _IndicatorSLiderState();
}

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}

class _IndicatorSLiderState extends State<IndicatorSLider> {
  int _current = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    CarouselSlider slider = CarouselSlider(
      items: map<Widget>(
        widget.imgList,
            (index, i) {
          return Column(
            children: [
              Container(
                margin: EdgeInsets.all(5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Stack(children: <Widget>[
                    Image.network(
                      i,
                      fit: BoxFit.fill,
                      width: _width * 0.08,
                      height: _width * 0.08,
                    ),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                      ),
                    )
                  ]),
                ),
              ),
              Reusable.flexibleText(_height * 0.01, widget.titleList[index],
                  EdgeInsets.only(top: _height * 0.001)),
            ],
          );
        },
      ).toList(),
      autoPlay: false,
      enlargeCenterPage: true,
      aspectRatio: 6.0,
      onPageChanged: (index) {
        setState(() {
          _current = index;
        });
      },
    );

    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      GestureDetector(
        child: Icon(
          Icons.chevron_left,
          color: MyColors.appYellow,
        ),
        onTap: () {
          slider.jumpToPage(_current != 0 ? _current-1 : 0);
          widget.callBack.call(widget.imgList[_current]);
        },
      ),
      Container(
        child: slider,
        width: _width * 0.5,
        height: _height * 0.1,
      ),
      GestureDetector(
        child: Icon(
          Icons.chevron_right,
          color: MyColors.appYellow,
        ),
        onTap: () {
          slider.jumpToPage(_current < widget.imgList.length ? _current+1 : widget.imgList.length);
          widget.callBack.call(widget.imgList[_current]);
        },
      ),
    ]);
  }
}

//return Column(
//children: [
//Container(
//margin: EdgeInsets.all(5.0),
//child: ClipRRect(
//borderRadius: BorderRadius.all(Radius.circular(5.0)),
//child: Stack(children: <Widget>[
//Image.network(
//i,
//fit: BoxFit.fill,
//width: widget.withTitle ? widget.width : 1000.0,
//height: _height*0.2,
//),
//Positioned(
//bottom: 0.0,
//left: 0.0,
//right: 0.0,
//child: Container(
//decoration: BoxDecoration(),
//padding: EdgeInsets.symmetric(
//vertical: 10.0, horizontal: 20.0),
//),
//)
//]),
//),
//),
//widget.withTitle
//? Reusable.flexibleText(
//_height * 0.01,
//widget.model.data.colors[index].name.en,
//EdgeInsets.only(top: _height * 0.01))
//: Container(),
//],
//);
