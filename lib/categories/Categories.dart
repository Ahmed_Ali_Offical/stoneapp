import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/model/catemodel.dart';
import 'package:stone_app/singleCategory/SingleCategory.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/clickes.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';

class Categories extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CategoriesState();
  }
}

class CategoriesState extends State<Categories> {
  int cartItemsCount = 0;
  List<CategoryModel> cateModel = [];
  bool _load = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchCategories();
    updateCartIcon();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    return WillPopScope(child:Scaffold(
      body: _load ? Reusable.showLoader(_load) :Container(
          width: double.infinity,
          height: double.infinity,
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: cateModel.length,
            shrinkWrap: true,
            itemBuilder: (context, int index) {
              return GestureDetector(
                child: Container(
                  width: _width * 0.18,
                  height: _height * 0.25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  margin: EdgeInsets.all(_width * 0.01),
                  child: Stack(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          cateModel[index].img,
                          width: double.infinity,
                          height: double.infinity,
                          fit: BoxFit.cover,
                          loadingBuilder: (BuildContext context, Widget child,
                              ImageChunkEvent loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CircularProgressIndicator(
                                value: loadingProgress.expectedTotalBytes != null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                    loadingProgress.expectedTotalBytes
                                    : null,
                              ),
                            );
                          },
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: _height * 0.06,
                        decoration: BoxDecoration(
                            color: MyColors.appGrayDark.withOpacity(0.9),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8.0),
                                topRight: Radius.circular(8.0))),
                        child: Center(
                          child: AutoSizeText(cateModel[index].title,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.white,
                                  fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.none)),
                        ),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                        return SingleCategory(cateModel[index].title, cateModel[index].id.toString());
                      }));
                },
              );
            },
          )),
    ),onWillPop: (){
    return Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) { return HomeScreen(); }));
    });
  }

  void fetchCategories() async {
    var request = await get(Urls.categories);
    bool status = json.decode(request.body)['success'];
    if (status) {
      List<dynamic> data = json.decode(request.body)['data'];
      for (var i in data) {
        cateModel
            .add(new CategoryModel(i['id'], i['image'], i['name'][lan] ?? ""));
      }
      if (mounted) {
        setState(() {
          _load = false;
        });
      }
    }
  }

  void updateCartIcon() async {
    var prefs = await SharedPreferences.getInstance();
    if(prefs.getString("items") != null) {
      CartModel cartModel =
      new CartModel.fromJson(json.decode(prefs.getString("items")));
      if (mounted)
        setState(() {
          cartItemsCount = cartModel.inner.length;
        });
    }
  }
}
