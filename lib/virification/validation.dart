import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/fragments/login.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/singleProductModel.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class VerificationScreen extends StatefulWidget {
  String phone;
  String verificationId;
  bool codeSent = false;
  int userId;

  VerificationScreen(this.phone, this.userId);

  @override
  State<StatefulWidget> createState() {
    return VirifcationState();
  }
}

String code;

class VirifcationState extends State<VerificationScreen> {
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
    !widget.phone.contains("+2")
        ? widget.phone = "+2${widget.phone}"
        : widget.phone;
    verifyPhone();
  }

  Future<void> verifyPhone() async {
    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: widget.phone,
        timeout: const Duration(seconds: 5),
        verificationCompleted: (AuthCredential user) {
          print("Verified");
        },
        verificationFailed: (AuthException exception) {
          print("Exception ${exception.message}");
        },
        codeSent: (String verId, [int forceCodeResend]) {
          setState(() {
            widget.codeSent = true;
            widget.verificationId = verId;
          });
        },
        codeAutoRetrievalTimeout: (String verId) {
          widget.verificationId = verId;
        });
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    return MaterialApp(localizationsDelegates: [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      new FallbackCupertinoLocalisationsDelegate(),
      //app-specific localization
      _specificLocalizationDelegate
    ],
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      theme: ThemeData(primarySwatch: MyColors.primary),
      locale: _specificLocalizationDelegate.overriddenLocale,home: Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Image.asset(
            "assets/images/activation.png",
            height: _height * 0.1,
            width: _height * 0.1,
          ),
          Reusable.flexibleText(_height * 0.03, appLocalizations.Verification_Code,
              EdgeInsets.only(top: _height * 0.05)),
          Container(
            height: _height * 0.02,
            margin: EdgeInsets.only(top: _height * 0.03),
            child: FittedBox(
              child: AutoSizeText(
                appLocalizations.Enter_Code,
                style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.grey,
                    fontFamily: lan == "en" ? "Poppins" : "Arabic",
                    decoration: TextDecoration.none),
              ),
              fit: BoxFit.fitHeight,
            ),
          ),
          Container(
            height: _height * 0.02,
            child: FittedBox(
              child: AutoSizeText(
                widget.phone,
                style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.grey,
                    fontFamily: lan == "en" ? "Poppins" : "Arabic",
                    decoration: TextDecoration.none),
              ),
              fit: BoxFit.fitHeight,
            ),
          ),
          Container(
            child: TextFormField(
              style: TextStyle(
                fontFamily: lan == "en" ? "Poppins" : "Arabic",
              ),
              onChanged: (value) {
                code = value;
              },
              decoration: InputDecoration(
                labelText: appLocalizations.Code,
              ),
            ),
            margin: EdgeInsets.only(left: _width * 0.1, right: _width * 0.1),
          ),
          GestureDetector(
            child: Container(
              height: _height * 0.02,
              margin: EdgeInsets.only(top: _height * 0.03),
              child: FittedBox(
                child: AutoSizeText(
                  appLocalizations.Send_Again,
                  style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.grey,
                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                      decoration: TextDecoration.none),
                ),
                fit: BoxFit.fitHeight,
              ),
            ),
            onTap: () {
              verifyPhone();
            },
          ),
          GestureDetector(
            child: Container(
              width: double.infinity,
              height: _height * 0.05,
              margin: EdgeInsets.only(
                  left: _width * 0.08,
                  right: _width * 0.08,
                  top: _height * 0.03),
              decoration: BoxDecoration(
                  color: MyColors.appYellow,
                  borderRadius: BorderRadius.all(Radius.circular(16.0))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    child: AutoSizeText(appLocalizations.Confirm,
                        textAlign: TextAlign.center,
                        maxFontSize: 16.0,
                        style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.none)),
                    padding: EdgeInsets.only(left: 3.0, right: 3.0),
                  ),
                ],
              ),
            ),
            onTap: verifyCode,
          ),
          GestureDetector(
            child: Center(
              child: Reusable.flexibleText(
                  _height * 0.02, "Skip", EdgeInsets.all(8.0)),
            ),
            onTap: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return HomeScreen();
              }));
            },
          ),
        ],
      ),
    ),);
  }

  void verifyCode() async {
    if (code.isNotEmpty) {
      AuthCredential authCreds = PhoneAuthProvider.getCredential(
          verificationId: widget.verificationId, smsCode: code);
      if (authCreds != null) {
        FirebaseAuth.instance.signInWithCredential(authCreds).then((value) => {
              if (value.user != null)
                {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return HomeScreen();
                  })),
                  verifyUser()
                }
              else
                {Toast.show(appLocalizations.Code_Error, context, duration: Toast.LENGTH_LONG)}
            });
      } else {
        Toast.show(appLocalizations.Code_Error, context, duration: Toast.LENGTH_LONG);
      }
    } else {
      Toast.show(appLocalizations.Enter_Received, context, duration: Toast.LENGTH_LONG);
    }
  }

  verifyUser() async {
    var response = await post(Urls.verify, body: {'user_id': widget.userId});
    print("verify result ${response.body}");
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }
}
