import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/user.dart';
import 'package:stone_app/register/registerComp.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class RegisiterIndividual extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterState();
  }
}

String username, mail, pass, phone;
String _image = "";
bool indi_loading = false;

class RegisterState extends State<RegisiterIndividual> {
  double screenWidth;
  double screenheight;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
//  // String lan = "";


  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));
    screenWidth = _width;
    screenheight = _height;
    Future getImage() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        _image = image.path;
      });
    }


    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        new FallbackCupertinoLocalisationsDelegate(),
        //app-specific localization
        _specificLocalizationDelegate
      ],
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
        body: GestureDetector(child: Stack(children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Center(
                child: GestureDetector(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        child: Padding(
                          child: _image != null && _image.isNotEmpty
                              ? CircleAvatar(
                            backgroundImage: FileImage(new File(_image)),
                            radius: _width * 0.12,
                          )
                              : Image.asset('assets/images/user.png',width: _width*0.3,height: _width*0.3,),
                          padding: EdgeInsets.all(_width * 0.02),
                        ),
                        decoration: BoxDecoration(
                            color: Colors.grey[300], shape: BoxShape.circle),
                      ),
                    ],
                  ),
                  onTap: getImage,
                ),
              ),
              Container(
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  style: TextStyle(
                    fontFamily: lan == "en" ? "Poppins" : "Arabic",
                  ),
                  decoration: InputDecoration(
                      labelText: appLocalizations.Username,
                      hintText: appLocalizations.Username_Enter,
                      border: InputBorder.none,
                      enabledBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
                        borderSide: const BorderSide(
                            color: MyColors.appYellow, width: 0.0),
                      ),
                      prefixIcon: Icon(
                        Icons.person,
                        color: MyColors.appYellow,
                      ),
                      suffixIcon: Icon(
                        Icons.edit,
                        color: MyColors.appYellow,
                      )),
                  onChanged: (value) {
                    username = value;
                  },
                ),
                margin: EdgeInsets.only(
                    top: _height * 0.05,
                    left: _width * 0.08,
                    right: _width * 0.08),
              ),
              Container(
                child: TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  style: TextStyle(
                    fontFamily: lan == "en" ? "Poppins" : "Arabic",
                  ),
                  decoration: InputDecoration(
                      labelText: appLocalizations.Email,
                      hintText: appLocalizations.Email_Enter,
                      border: InputBorder.none,
                      enabledBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
                        borderSide: const BorderSide(
                            color: MyColors.appYellow, width: 0.0),
                      ),
                      prefixIcon: Icon(
                        Icons.mail_outline,
                        color: MyColors.appYellow,
                      ),
                      suffixIcon: Icon(
                        Icons.edit,
                        color: MyColors.appYellow,
                      )),
                  onChanged: (value) {
                    mail = value;
                  },
                ),
                margin: EdgeInsets.only(
                    top: _height * 0.02,
                    left: _width * 0.08,
                    right: _width * 0.08),
              ),
              Container(
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  obscureText: true,
                  style: TextStyle(
                    fontFamily: lan == "en" ? "Poppins" : "Arabic",
                  ),
                  decoration: InputDecoration(
                      labelText: appLocalizations.Password,
                      hintText: appLocalizations.Password_Enter,
                      border: InputBorder.none,
                      enabledBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
                        borderSide: const BorderSide(
                            color: MyColors.appYellow, width: 0.0),
                      ),
                      prefixIcon: Icon(
                        Icons.lock_open,
                        color: MyColors.appYellow,
                      ),
                      suffixIcon: Icon(
                        Icons.edit,
                        color: MyColors.appYellow,
                      )),
                  onChanged: (value) {
                    pass = value;
                  },
                ),
                margin: EdgeInsets.only(
                    top: _height * 0.02,
                    left: _width * 0.08,
                    right: _width * 0.08),
              ),
              Container(
                child: TextFormField(
                  keyboardType: TextInputType.phone,
                  style: TextStyle(
                    fontFamily: lan == "en" ? "Poppins" : "Arabic",
                  ),
                  decoration: InputDecoration(
                      labelText: appLocalizations.Phone,
                      hintText: appLocalizations.Phone_Enter,
                      border: InputBorder.none,
                      enabledBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
                        borderSide: const BorderSide(
                            color: MyColors.appYellow, width: 0.0),
                      ),
                      prefixIcon: Icon(
                        Icons.phone,
                        color: MyColors.appYellow,
                      ),
                      suffixIcon: Icon(
                        Icons.edit,
                        color: MyColors.appYellow,
                      )),
                  onChanged: (value) {
                    phone = value;
                  },
                ),
                margin: EdgeInsets.only(
                    top: _height * 0.02,
                    left: _width * 0.08,
                    right: _width * 0.08),
              ),
              indi_loading ? Container(child: Center(child: CircularProgressIndicator(),),margin: EdgeInsets.all(8.0),) : GestureDetector(
                child: Container(
                  width: double.infinity,
                  height: _height * 0.07,
                  margin: EdgeInsets.only(
                      left: _width * 0.08,
                      right: _width * 0.08,
                      top: _height * 0.03),
                  decoration: BoxDecoration(
                      color: MyColors.appYellow,
                      borderRadius: BorderRadius.all(Radius.circular(30.0))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        child: AutoSizeText(appLocalizations.Register,
                            textAlign: TextAlign.center,
                            maxFontSize: 16.0,
                            style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.white,
                                fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.none)),
                        padding: EdgeInsets.only(left: 3.0, right: 3.0),
                      ),
                      Icon(
                        Icons.exit_to_app,
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
                onTap: _register,
              ),
              GestureDetector(
                child: Center(
                    child: Container(
                      child: AutoSizeText(
                        appLocalizations.Register_as_Company,
                        style: TextStyle(
                            fontSize: 13.0,
                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                            decoration: TextDecoration.none),
                      ),
                      margin: EdgeInsets.all(8.0),
                    )),
                onTap: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => RegisterCompany()));
                },
              )
            ],
          ),
          Reusable.showLoader(loading)
        ],),onTap: (){
          FocusScope.of(context).requestFocus(new FocusNode());
        },),
      ),
    );
  }

  void _register() async {
    List<String> inputs = [username, mail, pass, phone];
    if (Reusable.validateStrings(inputs)) {
//      if (_image.isEmpty) {
//        Toast.show(appLocalizations.Choose_Image, context,
//            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//      } else {
      setState(() {
        indi_loading = true;
      });
      if (_image.isNotEmpty) {
        await registerWithImage();
      }else{
        await registerWithoutImage();
      }
    } else {
      List<String> error = [appLocalizations.MissingFields];
      showDialog(context: this.context,builder: (BuildContext context) {
        return Reusable.errorDialog(context, screenWidth, screenheight, error);
      });
    }
  }

  Future registerWithoutImage() async {
    Map<String, String> headers = {
      "Authorization": "Bearer ${user.token}",
      "Accept": "application/json"
    };
    var uri = Uri.parse(Urls.register);
    var requestWithoutPhoto =
    await post(uri.toString(), headers: headers, body: {
      'name': username,
      'email': mail,
      'password': pass,
      'phone': phone,
      'level': "user"
    });
    setState(() {
      loading = false;
    });
    if (json.decode(requestWithoutPhoto.body)['success']) {
      var userVal = json.decode(requestWithoutPhoto.body);
      saveUser(userVal);
      Navigator.pushReplacement(this.context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else {
      List<dynamic> errors = json.decode(requestWithoutPhoto.body)['message'];
      List<String> error = [];
      for (String i in errors) {
        error.add(i);
      }
      showDialog(
          context: this.context,
          builder: (BuildContext context) {
            return Reusable.errorDialog(
                context, screenWidth, screenheight, error);
          });
    }
  }

  Future registerWithImage() async {
    var uri = Uri.parse(Urls.register);
    var request = http.MultipartRequest('POST', uri)
      ..fields['name'] = username
      ..fields['email'] = mail
      ..fields['password'] = pass
      ..fields['phone'] = phone
      ..fields['level'] = "user"
      ..files.add(await http.MultipartFile.fromPath('avatar', _image));
    var response = await request.send();
    if (response.statusCode == 200) print('Uploaded!');
    await response.stream.transform(utf8.decoder).listen((value){
      print("Response: $value");
      setState(() {
        indi_loading = false;
      });
      if(json.decode(value)['success']) {
        var userVal = json.decode(value);
        saveUser(userVal);
        Navigator.pushReplacement(this.context, MaterialPageRoute(builder: (context) => HomeScreen()));
      }else{
        List<dynamic> errors = json.decode(value)['message'];
        List<String> error = [];
        for(String i in errors){
          error.add(i);
        }
        showDialog(context: this.context,builder: (BuildContext context) {
          return Reusable.errorDialog(context, screenWidth, screenheight, error);
        });
      }
    });
  }

  void saveUser(userVal) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt("id", userVal['user']['id']);
    prefs.setString("name", userVal['user']['name']);
    prefs.setString("email", userVal['user']['email']);
    prefs.setString("phone", userVal['user']['phone']);
    prefs.setString("level", userVal['user']['level']);
    prefs.setString("token", userVal['token']);
    prefs.setString("image", userVal['user']['avatar']);
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }


}
