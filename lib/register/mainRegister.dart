import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/register/registerComp.dart';
import 'package:stone_app/register/registerIndi.dart';
import 'package:stone_app/values/colors.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterState();
  }
}

class RegisterState extends State<RegisterScreen> {

  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
  }
  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));
    // TODO: implement build
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        new FallbackCupertinoLocalisationsDelegate(),
        //app-specific localization
        _specificLocalizationDelegate
      ],
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
        body: Container(
          width: double.infinity,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Image.asset(
                "assets/images/lang.png",
                fit: BoxFit.fitWidth,
              ),
              Align(
                child: Wrap(
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          GestureDetector(
                            child: Container(
                              width: double.infinity,
                              height: _height * 0.07,
                              margin: EdgeInsets.only(
                                  left: _width * 0.08,
                                  right: _width * 0.08,
                                  top: _height * 0.03),
                              decoration: BoxDecoration(
                                  color: MyColors.appYellow,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(16.0))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    child: AutoSizeText(appLocalizations.Register_as_Company,
                                        textAlign: TextAlign.center,
                                        maxFontSize: 16.0,
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.white,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none)),
                                    padding:
                                        EdgeInsets.only(left: 3.0, right: 3.0),
                                  ),
                                  Icon(
                                    Icons.domain,
                                    color: Colors.white,
                                  )
                                ],
                              ),
                            ),
                            onTap: _registerCompany,
                          ),
                          GestureDetector(
                            child: Container(
                              width: double.infinity,
                              height: _height * 0.07,
                              margin: EdgeInsets.only(
                                  left: _width * 0.08,
                                  right: _width * 0.08,
                                  top: _height * 0.03),
                              decoration: BoxDecoration(
                                  color: MyColors.primary,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(16.0))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    child: AutoSizeText(
                                        appLocalizations.Register_as_Individual,
                                        textAlign: TextAlign.center,
                                        maxFontSize: 16.0,
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.white,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none)),
                                    padding:
                                        EdgeInsets.only(left: 3.0, right: 3.0),
                                  ),
                                  Icon(
                                    Icons.person,
                                    color: Colors.white,
                                  )
                                ],
                              ),
                            ),
                            onTap: _registerIndividual,
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(bottom: 8.0),
                    )
                  ],
                ),
                alignment: Alignment.bottomCenter,
              )
            ],
          ),
        ),
      ),
    );
  }

  void _registerCompany() {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => RegisterCompany()));
  }

  void _registerIndividual() {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => RegisiterIndividual()));
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }
}
