import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/userAddresses/userAddresses.dart';
import 'package:stone_app/values/colors.dart';

import '../LocaleHelper.dart';
import '../applocalizations.dart';

class Cart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CartState();
  }
}

class CartState extends State<Cart> {
  double totalPrice = 0.0;
  int totalCount = 0;
  List<Inner> cartItems = [];
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;

  // String lan = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getInnerModels();
    updateCart();
  }

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    print("Lan ${lan}");
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    ScreenUtil.init(context, height: _height, width: _width);
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        new FallbackCupertinoLocalisationsDelegate(),
        //app-specific localization
        _specificLocalizationDelegate
      ],
      supportedLocales: [Locale('en'), Locale('ar')],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: new IconThemeData(color: Colors.black),
          brightness: Platform.isAndroid? Brightness.dark:Brightness.light,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Center(
            child: Text(
              appLocalizations.Cart,
              style: TextStyle(
                  fontFamily: lan == "en" ? "Poppins" : "Arabic",
                  color: Colors.black),
            ),
          ),
        ),
        body: totalPrice == 0.0
            ? Container(
                width: double.infinity,
                height: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/images/zero_cart.png',
                      height: _width * 0.35,
                      width: _width * 0.35,
                    ),
                    Container(
                      child: AutoSizeText(
                        appLocalizations.no_orders,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(15.0),
                            color: Colors.black,
                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.none),
                      ),
                      margin: EdgeInsets.all(8.0),
                    )
                  ],
                ),
              )
            : Column(
                children: <Widget>[
                  Flexible(
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: cartItems.length,
                        itemBuilder: (context, int index) {
                          Inner item = cartItems[index];
                          return Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0))),
                            elevation: 1.0,
                            child: Stack(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Image.network(
                                      cartItems[index].image,
                                      width: _width * 0.25,
                                      height: _height * 0.2,
                                      fit: BoxFit.fill,
                                      loadingBuilder: (BuildContext context,
                                          Widget child,
                                          ImageChunkEvent loadingProgress) {
                                        if (loadingProgress == null)
                                          return child;
                                        return Center(
                                          child: CircularProgressIndicator(
                                            value: loadingProgress
                                                        .expectedTotalBytes !=
                                                    null
                                                ? loadingProgress
                                                        .cumulativeBytesLoaded /
                                                    loadingProgress
                                                        .expectedTotalBytes
                                                : null,
                                          ),
                                        );
                                      },
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        Container(
                                          height: _height * 0.02,
                                          margin:
                                              EdgeInsets.all(_height * 0.004),
                                          child: FittedBox(
                                            child: AutoSizeText(
                                              cartItems[index].name,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  fontSize: 15.0,
                                                  color: Colors.black,
                                                  fontFamily: lan == "en"
                                                      ? "Poppins"
                                                      : "Arabic",
                                                  fontWeight: FontWeight.bold,
                                                  decoration:
                                                      TextDecoration.none),
                                            ),
                                            fit: BoxFit.fitHeight,
                                          ),
                                        ),
                                        Container(
                                          width: _width * 0.7,
                                          margin:
                                              EdgeInsets.all(_height * 0.004),
                                          child: AutoSizeText(
                                            cartItems[index].desc,
                                            textAlign: TextAlign.start,
                                            maxLines: 3,
                                            overflow: TextOverflow.ellipsis,
                                            softWrap: true,
                                            style: TextStyle(
                                                fontSize: 15.0,
                                                color: Colors.grey,
                                                fontFamily: lan == "en"
                                                    ? "Poppins"
                                                    : "Arabic",
                                                decoration:
                                                    TextDecoration.none),
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              height: _height * 0.02,
                                              margin: EdgeInsets.all(
                                                  _height * 0.004),
                                              child: FittedBox(
                                                child: AutoSizeText(
                                                  appLocalizations.Quantity,
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                      fontSize: 15.0,
                                                      color: Colors.grey,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontFamily: lan == "en"
                                                          ? "Poppins"
                                                          : "Arabic",
                                                      decoration:
                                                          TextDecoration.none),
                                                ),
                                                fit: BoxFit.fitHeight,
                                              ),
                                            ),
                                            Card(
                                                child: Row(children: <Widget>[
                                                  GestureDetector(
                                                    child: Icon(
                                                      Icons.add,
                                                      color: Colors.black,
                                                    ),
                                                    onTap: () {
                                                      setState(() {
                                                        cartItems[index]
                                                            .count++;
                                                        updateCart();
                                                      });
                                                    },
                                                  ),
                                                  Container(
                                                    height: _height * 0.02,
                                                    margin: EdgeInsets.all(
                                                        _height * 0.004),
                                                    child: FittedBox(
                                                      child: AutoSizeText(
                                                        cartItems[index]
                                                            .count
                                                            .toString(),
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            fontSize: 15.0,
                                                            color: Colors.grey,
                                                            fontFamily:
                                                                lan == "en"
                                                                    ? "Poppins"
                                                                    : "Arabic",
                                                            decoration:
                                                                TextDecoration
                                                                    .none),
                                                      ),
                                                      fit: BoxFit.fitHeight,
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    child: Icon(
                                                      Icons.remove,
                                                      color: Colors.black,
                                                    ),
                                                    onTap: () {
                                                      setState(() {
                                                        if (cartItems[index]
                                                                .count >
                                                            1) {
                                                          cartItems[index]
                                                              .count--;
                                                        }
                                                        updateCart();
                                                      });
                                                    },
                                                  )
                                                ]),
                                                elevation: 1.0,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                8.0))))
                                          ],
                                        ),
                                        Container(
                                          height: _height * 0.02,
                                          margin:
                                              EdgeInsets.all(_height * 0.004),
                                          child: FittedBox(
                                            child: AutoSizeText(
                                              "${appLocalizations.Price} ${cartItems[index].price * cartItems[index].count}",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  fontSize: 15.0,
                                                  color: MyColors.appYellow,
                                                  fontFamily: lan == "en"
                                                      ? "Poppins"
                                                      : "Arabic",
                                                  decoration:
                                                      TextDecoration.none),
                                            ),
                                            fit: BoxFit.fitHeight,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Align(
                                  child: Padding(
                                    child: GestureDetector(
                                      child: Icon(
                                        Icons.delete,
                                        size: ScreenUtil().setWidth(25),
                                        color: MyColors.appGray,
                                      ),
                                      onTap: () {
                                        setState(() {
                                          cartItems.removeAt(index);
                                          updateCart();
                                        });
                                      },
                                    ),
                                    padding: EdgeInsets.all(8.0),
                                  ),
                                  alignment: lan == "en"
                                      ? Alignment.topRight
                                      : Alignment.topLeft,
                                )
                              ],
                            ),
                          );
                        }),
                    flex: 1,
                  ),
                  Container(
                    height: _height * 0.03,
                    margin: EdgeInsets.all(4.0),
                    child: FittedBox(
                      child: AutoSizeText(
                        "${appLocalizations.Total} ${totalPrice}",
                        style: TextStyle(
                            fontSize: 15.0,
                            color: MyColors.appYellow,
                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.none),
                      ),
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  GestureDetector(
                    child: Container(
                      color: MyColors.appYellow,
                      height: _height * 0.06,
                      child: Center(
                        child: AutoSizeText(
                          appLocalizations.Complete_Purchase,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 15.0,
                              color: Colors.white,
                              fontFamily: lan == "en" ? "Poppins" : "Arabic",
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.none),
                        ),
                      ),
                      width: double.infinity,
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (BuildContext context) {
                        return UserAddresses();
                      }));
                    },
                  ),
                ],
              ),
      ),
    );
  }

  void getInnerModels() async {
    var itemsPrefs = await SharedPreferences.getInstance();
    setState(() {
      lan = itemsPrefs.getString("en") ?? "en";
    });
    print("Items ${lan}");
    if (itemsPrefs.getString("items") != null) {
      CartModel cartModel =
          new CartModel.fromJson(json.decode(itemsPrefs.getString("items")));
      cartItems = cartModel.inner;
      updatePrice(cartModel);
    }
  }

  void updatePrice(CartModel cartModel) {
    totalPrice = 0.0;
    for (Inner i in cartModel.inner) {
      totalPrice += i.price * i.count;
    }

    setState(() {
      cartItems;
      totalPrice;
    });
  }

  void updateCart() async {
    var prefs = await SharedPreferences.getInstance();
    if (cartItems.isEmpty) {
      prefs.remove("items");
      setState(() {
        totalPrice = 0.0;
      });
//      Navigator.pop(context);
    } else {
      prefs.remove("items");
      CartModel model = new CartModel();
      model.inner = cartItems;
      prefs.setString("items", jsonEncode(model.toJson()));
      updatePrice(model);
    }
  }
}
