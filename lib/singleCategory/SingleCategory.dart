import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/innerProducts/InnerProducts.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/model/singleCategoryModel.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class SingleCategory extends StatefulWidget {
  String barTitle;
  String id;

  SingleCategory(this.barTitle, this.id);

  @override
  State<StatefulWidget> createState() {
    return SingleCategoryState();
  }
}

class SingleCategoryState extends State<SingleCategory> {
  int cartItemsCount = 0;
  SingleCategoryModel singleCategoryModel;

  bool _load = true;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    updateCartIcon();
    getChilds(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    ScreenUtil.init(context, height: _height, width: _width);

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        new FallbackCupertinoLocalisationsDelegate(),
        //app-specific localization
        _specificLocalizationDelegate
      ],
      supportedLocales: [Locale('en'), Locale('ar')],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
        appBar: Reusable.getAppBar(
            context, _height, _width, cartItemsCount, widget.barTitle),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          child: singleCategoryModel != null
              ? singleCategoryModel.data.childs.length > 0
                  ? ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: singleCategoryModel.data.childs.length,
                      shrinkWrap: true,
                      itemBuilder: (context, int index) {
                        return GestureDetector(
                          child: Container(
                            height: _height * 0.25,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0))),
                            margin: EdgeInsets.all(_width * 0.01),
                            child: Stack(
                              children: <Widget>[
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(8.0),
                                  child: Image.network(
                                    singleCategoryModel
                                        .data.childs[index].image,
                                    width: double.infinity,
                                    height: double.infinity,
                                    fit: BoxFit.cover,
                                    loadingBuilder: (BuildContext context,
                                        Widget child,
                                        ImageChunkEvent loadingProgress) {
                                      if (loadingProgress == null) return child;
                                      return Center(
                                        child: CircularProgressIndicator(
                                          value: loadingProgress
                                                      .expectedTotalBytes !=
                                                  null
                                              ? loadingProgress
                                                      .cumulativeBytesLoaded /
                                                  loadingProgress
                                                      .expectedTotalBytes
                                              : null,
                                        ),
                                      );
                                    },
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  height: _height * 0.05,
                                  decoration: BoxDecoration(
                                      color:
                                          MyColors.appGrayDark.withOpacity(0.9),
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8.0),
                                          topRight: Radius.circular(8.0))),
                                  child: Center(
                                    child: AutoSizeText(
                                        lan == "en" ? singleCategoryModel
                                            .data.childs[index].name.en:singleCategoryModel
                                            .data.childs[index].name.ar,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 15.0,
                                            color: Colors.white,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none)),
                                  ),
                                )
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                                builder: (BuildContext context) {
                              return InnerProducts(
                                  lan == "en" ? singleCategoryModel
                                      .data.childs[index].name.en:singleCategoryModel
                                      .data.childs[index].name.ar,
                                  singleCategoryModel
                                      .data.childs[index].id.toString());
                            }));
                          },
                        );
                      },
                    )
                  : Center(
                      child: Text(
                        appLocalizations.No_Products,
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(15.0),
                            color: Colors.black,
                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                            decoration: TextDecoration.none),
                      ),
                    )
              : Reusable.showLoader(_load),
        ),
      ),
    );
  }

  void updateCartIcon() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString("items") != null &&
        prefs.getString("items").isNotEmpty) {
      CartModel cartModel =
          new CartModel.fromJson(json.decode(prefs.getString("items")));
      if (mounted)
        setState(() {
          lan = prefs.getString("en") ?? "en";
          cartItemsCount = cartModel.inner.length;
        });
    }
  }

  void getChilds(String id) async {
    var childsReques = await get("${Urls.categories}/$id");
    singleCategoryModel =
        SingleCategoryModel.fromJson(jsonDecode(childsReques.body));
    setState(() {
      singleCategoryModel;
      _load = false;
    });
  }
}
