import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import '../applocalizations.dart';

class NewPassword extends StatefulWidget {
  String code, phone;

  NewPassword(this.code, this.phone);

  @override
  State<StatefulWidget> createState() {
    return NewPasswordState();
  }
}

class NewPasswordState extends State<NewPassword> {
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  bool loading = false;

  String newPass;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
  }

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    ScreenUtil.init(context, width: _width, height: _height);

    return MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          new FallbackCupertinoLocalisationsDelegate(),
          //app-specific localization
          _specificLocalizationDelegate
        ],
        supportedLocales: [
          Locale('en'),
          Locale('ar')
        ],
        locale: _specificLocalizationDelegate.overriddenLocale,
        theme: ThemeData(primarySwatch: MyColors.primary),
        home: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              iconTheme: new IconThemeData(color: Colors.black),
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                color: Colors.black,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              title: Center(
                child: Text(
                  appLocalizations.No_PASS,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: ScreenUtil().setSp(15.0),
                    fontFamily: lan == "en" ? "Poppins" : "Arabic",
                  ),
                ),
              ),
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Enter New Password",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: ScreenUtil().setSp(18.0),
                    fontFamily: lan == "en" ? "Poppins" : "Arabic",
                  ),
                ),
                Container(
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    style: TextStyle(
                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                    ),
                    decoration: InputDecoration(
                        labelText: "New Password",
                        hintText: "Enter New Password",
                        border: InputBorder.none,
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          borderSide: const BorderSide(
                              color: MyColors.appYellow, width: 0.0),
                        ),
                        prefixIcon: Icon(
                          Icons.lock_open,
                          color: MyColors.appYellow,
                        ),
                        suffixIcon: Icon(
                          Icons.edit,
                          color: MyColors.appYellow,
                        )),
                    onChanged: (value) {
                      setState(() {
                        newPass = value;
                      });
                    },
                  ),
                  margin: EdgeInsets.only(
                      top: _height * 0.02,
                      left: _width * 0.08,
                      right: _width * 0.08),
                ),
                loading
                    ? Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                  margin: EdgeInsets.all(8.0),
                )
                    : GestureDetector(
                  child: Container(
                    width: double.infinity,
                    height: _height * 0.07,
                    margin: EdgeInsets.only(
                        left: _width * 0.08,
                        right: _width * 0.08,
                        top: _height * 0.03),
                    decoration: BoxDecoration(
                        color: MyColors.appYellow,
                        borderRadius:
                        BorderRadius.all(Radius.circular(16.0))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          child: Text(appLocalizations.Send,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: ScreenUtil().setSp(15),
                                  color: Colors.white,
                                  fontFamily:
                                  lan == "en" ? "Poppins" : "Arabic",
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.none)),
                          padding: EdgeInsets.only(left: 3.0, right: 3.0),
                        )
                      ],
                    ),
                  ),
                  onTap: _resetPass,
                )
              ],
            )));
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }

  void _resetPass() async{
    setState(() {
      loading = true;
    });
    var resetRequst = await post(Urls.resetpass, body: {
      'phone': widget.phone,
      'code': widget.code,
      'new_password': newPass,
    });
    print("${widget.phone} - ${widget.code} - ${newPass}");
    setState(() {
      loading = false;
    });
    bool status = json.decode(resetRequst.body)['success'];
    Toast.show(json.decode(resetRequst.body)['message'], context,duration: Toast.LENGTH_LONG);
    if(status){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context){
        return HomeScreen();
      }));
    }
  }
}
