import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/colors.dart';

class LangScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LangState();
  }
}

enum Langs { ar, en }

class LangState extends State<LangScreen> {
  static Langs _lang ;

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    ScreenUtil.init(context, width: _width, height: _height);
    var decorationAR = BoxDecoration(
        border:
            Border.all(color: _lang == Langs.ar ? Colors.brown : Colors.grey),
        borderRadius: BorderRadiusGeometry.lerp(
            BorderRadius.circular(8.0), BorderRadius.circular(8.0), 8.0));

    var decorationEN = BoxDecoration(
        border:
            Border.all(color: _lang == Langs.en ? Colors.brown : Colors.grey),
        borderRadius: BorderRadiusGeometry.lerp(
            BorderRadius.circular(8.0), BorderRadius.circular(8.0), 8.0));

    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Image.asset(
                "assets/images/lang.png",
                fit: BoxFit.fill,
              ),
              Align(
                child: Container(child: Wrap(children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(
                        "اختر اللغه",
                        style: TextStyle(
                            fontFamily: "Arabic",
                            fontSize: ScreenUtil().setSp(15.0),
                            color: Colors.black),
                      ),
                      Container(
                          width: _width * 0.7,
                          decoration: decorationAR,
                          margin: EdgeInsets.only(top: _height * 0.01),
                          child: GestureDetector(
                            child: ListTile(
                              trailing: Image.asset(
                                "assets/images/ar.png",
                                width: _width * 0.05,
                                height: _width * 0.05,
                              ),
                              title: Text(
                                "العربية",
                                style: TextStyle(
                                    fontFamily: "Arabic",
                                    fontSize: ScreenUtil().setSp(14.0),
                                    color: Colors.black),
                                textAlign: TextAlign.end,
                              ),
                              leading: Radio(
                                value: Langs.ar,
                                activeColor: Colors.brown,
                                onChanged: (Langs value) {
                                  setState(() {
                                    _lang = value;
                                  });
                                },
                                groupValue: _lang,
                              ),
                            ),
                            onTap: () {
                              setState(() {
                                setLang("ar");
                                _lang = Langs.ar;
                              });
                            },
                          )),
                      Container(
                        width: _width * 0.7,
                        margin: EdgeInsets.only(top: _height * 0.01),
                        decoration: decorationEN,
                        child: GestureDetector(
                          child: ListTile(
                            trailing: Image.asset(
                              "assets/images/en.png",
                              width: _width * 0.05,
                              height: _width * 0.05,
                            ),
                            title: Text(
                              'الانجليزية',
                              style: TextStyle(
                                  fontFamily: "Arabic",
                                  fontSize: ScreenUtil().setSp(14.0),
                                  color: Colors.black),
                              textAlign: TextAlign.end,
                            ),
                            leading: Radio(
                              value: Langs.en,
                              groupValue: _lang,
                              activeColor: Colors.brown,
                              onChanged: (Langs value) {
                                setState(() {
                                  _lang = value;
                                });
                              },
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              setLang("en");
                              _lang = Langs.en;
                            });
                          },
                        ),
                      ),
                    ],
                  )
                ],),margin: EdgeInsets.all(_height*0.1),),
                alignment: Alignment.bottomCenter,
              )
            ],
          ),
        ),
      ),
    );
  }

  void setLang(String s) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("en", s);
    print("Lang Saved");
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) { return SplashScreen(); }));
  }
}
