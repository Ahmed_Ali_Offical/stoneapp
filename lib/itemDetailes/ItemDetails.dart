import 'dart:convert';
import 'dart:ffi';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/cart/cart.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/model/singleProductModel.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/IndicatorSlider.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:stone_app/views/slider.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class ItemDetailes extends StatefulWidget {
  int productId = 0;

  ItemDetailes(this.productId);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ItemDetailsState();
  }
}

enum tiles { HORIZONTAL, VERTICAL }

class ItemDetailsState extends State<ItemDetailes> {
  String mainImg;
  tiles tileVal = tiles.HORIZONTAL;
  int singleSize = 0;
  SingleProductModel productModel;
  int cartItemsCount = 0;
  List<Sizes> sizes = [];
  List<String> imgs = [];
  List<String> titles = [];
  var commentWidgets = List<Widget>();
  double workQty = 1;
  double hight = 1.0;
  double screenHight;

  double width = 1.0;
  double screenWidth;

  double area = 1.0;
  double totalQty = 1.0;
  bool _load = true;
  List<Inner> cartItems = [];

  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    updateCartIcon();
    getProduct(widget.productId);
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    screenHight = _height;
    screenWidth = _width;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    ScreenUtil.init(context, height: _height, width: _width);
    return MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          new FallbackCupertinoLocalisationsDelegate(),
          //app-specific localization
          _specificLocalizationDelegate
        ],
        supportedLocales: [
          Locale('en'),
          Locale('ar')
        ],
        locale: _specificLocalizationDelegate.overriddenLocale,
        theme: ThemeData(primarySwatch: MyColors.primary),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            iconTheme: new IconThemeData(color: Colors.black),
            brightness: Platform.isAndroid? Brightness.dark:Brightness.light,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            actions: <Widget>[
              GestureDetector(
                child: Center(
                  child: Container(
                    child: new Stack(
                      children: <Widget>[
                        Icon(
                          Icons.shopping_cart,
                          color: MyColors.appGray,
                        ),
                        Container(
                          width: _width * 0.035,
                          height: _width * 0.035,
                          child: FittedBox(
                            child: AutoSizeText(
                              cartItemsCount.toString(),
                            ),
                            fit: BoxFit.fitHeight,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(50),
                            ),
                            color: Colors.brown,
                          ),
                        )
                      ],
                    ),
                    margin: EdgeInsets.only(
                        left: _width * 0.05, right: _width * 0.05),
                  ),
                ),
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Cart()));
                },
              )
            ],
            title: Center(
              child: Text(
                appLocalizations.World_Stones,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: ScreenUtil().setSp(15.0),
                    fontFamily: lan == "en" ? "Poppins" : "Arabic",),
              ),
            ),
          ),
          body: productModel != null
              ? SingleChildScrollView(
                  child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: _height * 0.22,
                      child: Center(
                        child: Image.network(
                          mainImg,
                          height: _height * 0.22,
                          loadingBuilder: (BuildContext context, Widget child,
                              ImageChunkEvent loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CircularProgressIndicator(
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        loadingProgress.expectedTotalBytes
                                    : null,
                              ),
                            );
                          },
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    if (imgs.isNotEmpty)
                      IndicatorSLider((img) {
                        setState(() {
                          mainImg = img;
                        });
                      }, imgs, titles),
                    Row(
                      children: [
                        Expanded(
                            child: Align(
                          child: Reusable.flexibleText(
                              _height * 0.03,
                              lan == "en" ? productModel.data.title.en:productModel.data.title.ar,
                              EdgeInsets.only(
                                  top: _height * 0.01,
                                  right: _height * 0.01,
                                  left: _height * 0.01)),
                              alignment: lan == "en" ? Alignment.centerLeft:Alignment.centerRight,
                        )),
                        Reusable.flexibleText(
                            _height * 0.025,
                            productModel.data.itemNo,
                            EdgeInsets.only(
                                top: _height * 0.01,
                                right: _height * 0.01,
                                left: _height * 0.01)),
                      ],
                    ),
                    Container(
                      height: _height * 0.02,
                      margin: EdgeInsets.only(
                          top: _height * 0.01,
                          right: _height * 0.01,
                          left: _height * 0.01),
                      child: FittedBox(
                        child: AutoSizeText(
                          "${appLocalizations.Category_} ${lan == "en" ? productModel.data.category.name.en : productModel.data.category.name.ar}",
                          style: TextStyle(
                              fontSize: 15.0,
                              color: MyColors.appYellow,
                              fontFamily: lan == "en" ? "Poppins" : "Arabic",
                              decoration: TextDecoration.none),
                        ),
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    Reusable.flexibleText(
                        _height * 0.02,
                        appLocalizations.Description,
                        EdgeInsets.only(
                            top: _height * 0.01,
                            right: _height * 0.01,
                            left: _height * 0.01)),
                    Container(
                        child: AutoSizeText(
                          lan == "en" ? productModel.data.content.en : productModel.data.content.ar,
                          softWrap: true,
                          style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.black,
                              fontFamily: lan == "en" ? "Poppins" : "Arabic",
                              decoration: TextDecoration.none),
                        ),
                        margin: EdgeInsets.only(
                            top: _height * 0.01,
                            right: _height * 0.01,
                            left: _height * 0.01)),
                    Reusable.flexibleText(
                        _height * 0.02,
                        appLocalizations.Available_Sizes,
                        EdgeInsets.only(
                            top: _height * 0.01,
                            right: _height * 0.01,
                            left: _height * 0.01)),
                    Column(
                      children: sizes
                          .map((e) => new RadioListTile<Sizes>(
                                title: AutoSizeText(
                                  "${e.width.toStringAsFixed(2)}x${e.length.toStringAsFixed(2)} = ${e.totalArea}",
                                  style: TextStyle(
                                      fontFamily: "Arabic",
                                      fontSize: 15.0,
                                      color: Colors.black),
                                  textAlign: TextAlign.end,
                                ),
                                value: e,
                                activeColor: MyColors.appYellow,
                                onChanged: (Sizes value) {
                                  setState(() {
                                    singleSize = sizes.indexOf(e);
                                    calculateArea();
                                    print("Item: ${singleSize.toString()}");
                                  });
                                },
                                groupValue: sizes[singleSize],
                              ))
                          .toList(),
                    ),
                    Reusable.flexibleText(
                        _height * 0.02,
                        appLocalizations.Unit_Dimension,
                        EdgeInsets.only(
                            top: _height * 0.01,
                            right: _height * 0.01,
                            left: _height * 0.01)),
                    new RadioListTile<tiles>(
                      title: AutoSizeText(
                        appLocalizations.Horizontal,
                        style: TextStyle(
                            fontFamily: "Arabic",
                            fontSize: 15.0,
                            color: Colors.black),
                        textAlign: TextAlign.end,
                      ),
                      activeColor: MyColors.appYellow,
                      value: tiles.HORIZONTAL,
                      onChanged: (tiles value) {
                        setState(() {
                          tileVal = value;
                          print("Item: $tileVal");
                        });
                      },
                      groupValue: tileVal,
                    ),
                    new RadioListTile<tiles>(
                      title: AutoSizeText(
                        appLocalizations.Vertical,
                        style: TextStyle(
                            fontFamily: "Arabic",
                            fontSize: 15.0,
                            color: Colors.black),
                        textAlign: TextAlign.end,
                      ),
                      value: tiles.VERTICAL,
                      activeColor: MyColors.appYellow,
                      onChanged: (tiles value) {
                        setState(() {
                          tileVal = value;
                          print("Item: $tileVal");
                        });
                      },
                      groupValue: tileVal,
                    ),
                    Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Expanded(
                            child: AutoSizeText(
                              appLocalizations.Wall_Width,
                              style: TextStyle(
                                  fontFamily: "Arabic",
                                  fontSize: 12.0,
                                  color: Colors.grey),
                              textAlign: TextAlign.start,
                            ),
                            flex: 1,
                          ),
                          Expanded(
                            child: Container(
                              height: ScreenUtil().setHeight(35),
                              child: Padding(
                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  maxLines: 1,
                                  onChanged: (value) {
                                    setState(() {
                                      width = double.parse(value);
                                    });
                                    calculateArea();
                                  },
                                  decoration: InputDecoration(
                                    hintText: "11 m",
                                    border: InputBorder.none,
                                  ),
                                ),
                                padding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(5),
                                    right: ScreenUtil().setWidth(5)),
                              ),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: MyColors.appYellow, width: 0.0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0))),
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(
                        left: _width * 0.05,
                        right: _width * 0.05,
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Expanded(
                            child: AutoSizeText(
                              appLocalizations.Wall_Height,
                              style: TextStyle(
                                  fontFamily: "Arabic",
                                  fontSize: 12.0,
                                  color: Colors.grey),
                              textAlign: TextAlign.start,
                            ),
                            flex: 1,
                          ),
                          Expanded(
                            child: Container(
                              height: ScreenUtil().setHeight(35),
                              child: Padding(
                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  onChanged: (value) {
                                    setState(() {
                                      hight = double.parse(value);
                                    });
                                    calculateArea();
                                  },
                                  decoration: InputDecoration(
                                    hintText: "3 m",
                                    border: InputBorder.none,
                                  ),
                                ),
                                padding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(5),
                                    right: ScreenUtil().setWidth(5)),
                              ),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: MyColors.appYellow, width: 0.0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0))),
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(
                          left: _width * 0.05,
                          right: _width * 0.05,
                          top: _width * 0.02),
                    ),
                    Reusable.flexibleText(
                        _height * 0.016,
                        appLocalizations.Required_Quantity,
                        EdgeInsets.only(
                            top: _height * 0.01,
                            right: _height * 0.01,
                            left: _height * 0.01)),
                    Container(
                      width: double.infinity,
                      child: Center(
                        child: Padding(
                          child: AutoSizeText(
                            "$area m\u00B2",
                            softWrap: true,
                            style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                decoration: TextDecoration.none),
                          ),
                          padding: EdgeInsets.all(_width * 0.02),
                        ),
                      ),
                      margin: EdgeInsets.all(_width * 0.03),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(16.0)),
                    ),
                    Reusable.flexibleText(
                        _height * 0.016,
                        appLocalizations.Required_Quantity_Planning,
                        EdgeInsets.only(
                            top: _height * 0.01,
                            right: _height * 0.01,
                            left: _height * 0.01)),
                    Container(
                      width: double.infinity,
                      child: Center(
                        child: Padding(
                          child: AutoSizeText(
                            "$totalQty ${appLocalizations.item}",
                            softWrap: true,
                            style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black,
                                fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                decoration: TextDecoration.none),
                          ),
                          padding: EdgeInsets.all(_width * 0.02),
                        ),
                      ),
                      margin: EdgeInsets.all(_width * 0.03),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(16.0)),
                    ),
                    Reusable.flexibleText(
                        _height * 0.016,
                        appLocalizations.Required_Quantity_Enter,
                        EdgeInsets.only(
                            top: _height * 0.01,
                            right: _height * 0.01,
                            left: _height * 0.01)),
                    Container(
                      width: double.infinity,
                      child: Row(
                        children: [
                          GestureDetector(
                            child: Container(
                              child: Icon(
                                Icons.remove,
                                size: _width * 0.05,
                              ),
                              margin: EdgeInsets.only(
                                  right: _height * 0.01, left: _height * 0.01),
                            ),
                            onTap: () {
                              setState(() {
                                if (workQty != 1) workQty--;
                              });
                            },
                          ),
                          Container(
                            color: MyColors.appYellow,
                            width: 1.0,
                            height: _height * 0.05,
                          ),
                          Expanded(
                            child: Padding(
                              child: AutoSizeText(
                                workQty.toString(),
                                softWrap: true,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 15.0,
                                    color: Colors.black,
                                    fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                    decoration: TextDecoration.none),
                              ),
                              padding: EdgeInsets.all(_width * 0.02),
                            ),
                          ),
                          Container(
                            color: MyColors.appYellow,
                            width: 1.0,
                            height: _height * 0.05,
                          ),
                          GestureDetector(
                            child: Container(
                              child: Icon(
                                Icons.add,
                                size: _width * 0.05,
                              ),
                              margin: EdgeInsets.only(
                                  right: _height * 0.01, left: _height * 0.01),
                            ),
                            onTap: () {
                              setState(() {
                                workQty++;
                              });
                            },
                          ),
                        ],
                      ),
                      margin: EdgeInsets.all(_width * 0.03),
                      decoration: BoxDecoration(
                          border: Border.all(color: MyColors.appYellow),
                          borderRadius: BorderRadius.circular(16.0)),
                    ),
                    Center(
                      child: Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Icon(
                              Icons.local_offer,
                              color: MyColors.appYellow,
                            ),
                            Reusable.flexibleText(
                                _height * 0.016,
                                appLocalizations.Total_Price_,
                                EdgeInsets.only(
                                    top: _height * 0.01,
                                    right: _height * 0.01,
                                    left: _height * 0.01)),
                            if (productModel.data.priceAfterDis <
                                productModel.data.price)
                              Reusable.flexibleText(
                                  _height * 0.016,
                                  "${productModel.data.priceAfterDis * area} SR"
                                      .toString(),
                                  EdgeInsets.only(
                                      top: _height * 0.01,
                                      right: _height * 0.01,
                                      left: _height * 0.01)),
                            Container(
                              height: _height * 0.016,
                              margin: EdgeInsets.only(
                                  top: _height * 0.01,
                                  right: _height * 0.01,
                                  left: _height * 0.01),
                              child: FittedBox(
                                child: AutoSizeText(
                                  "${productModel.data.price * area} SR",
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      color: Colors.black,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      fontWeight: FontWeight.bold,
                                      decoration:
                                          productModel.data.priceAfterDis <
                                                  productModel.data.price
                                              ? TextDecoration.lineThrough
                                              : TextDecoration.none),
                                ),
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                          ],
                        ),
                        width: _width * 0.7,
                        margin: EdgeInsets.all(_width * 0.01),
                      ),
                    ),
                    (int.parse(productModel.data.stock) < 10)
                        ? Center(
                            child: Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Icon(
                                    Icons.info,
                                    color: MyColors.appYellow,
                                  ),
                                  Container(
                                    height: _height * 0.048,
                                    width: _width * 0.6,
                                    margin: EdgeInsets.only(
                                        top: _height * 0.01,
                                        right: _height * 0.01,
                                        left: _height * 0.01),
                                    child: AutoSizeText(
                                      "${appLocalizations.Available_now} ${productModel.data.stock} m ${appLocalizations.Days_Info}",
                                      softWrap: true,
                                      maxLines: 3,
                                      style: TextStyle(
                                          fontSize: 15.0,
                                          color: Colors.black,
                                          fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                          fontWeight: FontWeight.bold,
                                          decoration: TextDecoration.none),
                                    ),
                                  ),
                                ],
                              ),
                              width: _width * 0.7,
                              margin: EdgeInsets.all(_width * 0.01),
                            ),
                          )
                        : Container(),
                    GestureDetector(
                      child: Container(
                        width: double.infinity,
                        height: _height * 0.07,
                        margin: EdgeInsets.only(
                            left: _width * 0.08,
                            right: _width * 0.08,
                            top: _height * 0.03),
                        decoration: BoxDecoration(
                            color: MyColors.primary,
                            borderRadius:
                                BorderRadius.all(Radius.circular(16.0))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              child: AutoSizeText(appLocalizations.Share,
                                  textAlign: TextAlign.center,
                                  maxFontSize: 16.0,
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.white,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.none)),
                              padding: EdgeInsets.only(left: 3.0, right: 3.0),
                            ),
                            Icon(
                              Icons.share,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                      onTap: _share,
                    ),
                    GestureDetector(
                      child: Container(
                        width: double.infinity,
                        height: _height * 0.07,
                        margin: EdgeInsets.only(
                            left: _width * 0.08,
                            right: _width * 0.08,
                            bottom: _height * 0.03,
                            top: _height * 0.01),
                        decoration: BoxDecoration(
                            color: MyColors.appYellow,
                            borderRadius:
                                BorderRadius.all(Radius.circular(16.0))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              child: AutoSizeText(appLocalizations.AddToCart,
                                  textAlign: TextAlign.center,
                                  maxFontSize: 16.0,
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.white,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.none)),
                              padding: EdgeInsets.only(left: 3.0, right: 3.0),
                            ),
                            Icon(
                              Icons.shopping_cart,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                      onTap: _addtocart,
                    ),
                  ],
                ))
              : Reusable.showLoader(_load),
        ));
  }

  void calculateArea() {
    setState(() {
      area = width * hight;
      print("Area $area");
      totalQty =
          (area / double.parse(productModel.data.sizes[singleSize].totalArea))
              .roundToDouble();
      workQty = area;
    });
  }

  void getProduct(int productId) async {
    var request = await get("${Urls.products}/$productId");
    if (mounted)
      setState(() {
        productModel = SingleProductModel.fromJson(json.decode(request.body));
        sizes = productModel.data.sizes;
        mainImg = productModel.data.photo;
      });
    for (var color in productModel.data.colors) {
      imgs.add(color.image);
      titles.add(lan == "en" ? color.name.en:color.name.ar);
    }

    setState(() {
      commentWidgets;
      imgs;
      titles;
      _load = false;
    });
  }

  void _share() {}

  void _addtocart() {
    if (user != null && user.id != 0) {
      if(width > 1.0 && hight > 1.0) {
        CartModel model = new CartModel();
        cartItems.add(Inner(
            productModel.data.id,
            productModel.data.photo,
            lan == "en" ? productModel.data.title.en : productModel.data.title
                .ar,
            lan == "en" ? productModel.data.content.en : productModel.data
                .content.ar,
            width.toString(),
            hight.toString(),
            area.toString(),
            productModel.data.price,
            area.toInt()));
        model.inner = cartItems;
        print("CartItems : ${jsonEncode(model.toJson())}");
        saveCartItems(jsonEncode(model.toJson()));
      }else{
        Toast.show(appLocalizations.Enter_Wall, context,duration: Toast.LENGTH_LONG);
      }
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Reusable.loginDialog(
                context,appLocalizations, screenWidth, screenHight, (val) => {});
          });
    }
  }

  void saveCartItems(String cartItems) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("items", cartItems);
    updateCartIcon();
    Toast.show(appLocalizations.Added, context, duration: Toast.LENGTH_LONG);
  }

  void updateCartIcon() async {
    var prefs = await SharedPreferences.getInstance();
    CartModel cartModel =
        new CartModel.fromJson(json.decode(prefs.getString("items")));
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        cartItemsCount = cartModel.inner.length;
      });
  }
}
