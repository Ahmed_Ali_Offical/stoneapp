import 'package:flutter/material.dart';

class NoItemsScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NoItemsScreenState();
  }
}

class NoItemsScreenState extends State<NoItemsScreen>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(""
                  "assets/images/cart2.png",
              fit: BoxFit.fitWidth,
                color: Colors.brown,
              ),
              Padding(
                child: Text(
                  "لا يوجد طلبات هنا",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 30.0,
                    color: Colors.black,
                    fontWeight: FontWeight.w300,
                    fontFamily: "Arabic",
                  ),
                ), padding: EdgeInsets.all(8.0),
              )
            ],
          ),
        ),
      ),
    );
  }
}