import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/colors.dart';

class RegisterScreen extends  StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterState ();
  }
}

class RegisterState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
        theme: ThemeData(primarySwatch: MyColors.primary),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.amber,
            title: Center(
              child: Text(
                "تسجيل كشركة",
                style: TextStyle(fontFamily: lan == "en" ? "Poppins" : "Arabic",color: Colors.black),
              ),
            ),
          ),
          body: Container(
            width: double.infinity,
            height: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Image.asset(
                  "assets/images/lang.png",
                  fit: BoxFit.fitWidth,),
              ],
            ),
          ),
        )
    );
  }
}