import 'package:flutter/material.dart';

class Intro3Screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Intro3State();
  }
}

class Intro3State extends State<Intro3Screen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Image.asset(
                "assets/images/intro.png",
                fit: BoxFit.fitWidth,
              ),
              Padding(
                child: Text(
                  "نقدم افضل الاسعار",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: 45.0,
                    color: Colors.black,
                    fontFamily: "Arabic",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(0, 68.0, 52.0, 0)
              ),
              Padding(
                child: Text(
                  "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام",
                    textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                    fontFamily: "Arabic",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(64.0, 18.0, 55.0, 0),
              )
            ],
          ),
        ),
      ),
    );
  }
}