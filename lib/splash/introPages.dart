import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/SettingsModel.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';

import '../LocaleHelper.dart';
import '../applocalizations.dart';

class IntroPages extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return IntroPagesState();
  }
}

class IntroPagesState extends State<IntroPages> {
  List<PageViewModel> listPagesViewModel;
  SettingsModel settingsModel;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  bool _privacyCheck = false;

  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getSettings();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));
    ScreenUtil.init(context, height: _height, width: _width);
    listPagesViewModel = [
      PageViewModel(
        title: "",
        bodyWidget: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Image.asset(
                "assets/images/intro3.png",
                fit: BoxFit.fitWidth,
              ),
              Padding(
                child: Text(
                  " .مرحبا بكم",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(15.0),
                    color: Colors.brown,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Arabic",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(
                    _width * 0.1, _height * 0.04, _width * 0.1, 0),
              ),
              Padding(
                child: Text(
                  "بعالم الصفائح الحجرية",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(14.0),
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontFamily: "Arabic",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(
                    _width * 0.1, _height * 0.02, _width * 0.1, 0),
              ),
              Padding(
                child: Text(
                  settingsModel != null
                      ? settingsModel.data.introEn
                      : appLocalizations.Loading_Text,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(12.0),
                    color: Colors.black,
                    fontFamily: "Arabic",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(
                    _width * 0.2, _height * 0.02, _width * 0.2, 0),
              ),
            ],
          ),
        ),
      ),
      PageViewModel(
        title: "",
        bodyWidget: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Image.asset(
                "assets/images/intro2.png",
                fit: BoxFit.fitWidth,
              ),
              Padding(
                child: Text(
                  "تعرف علي افضل الخدمات",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(15.0),
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Arabic",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(
                    _width * 0.1, _height * 0.04, _width * 0.1, 0),
              ),
              Padding(
                child: Text(
                  settingsModel != null
                      ? settingsModel.data.intro2En
                      : appLocalizations.Loading_Text,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(12.0),
                    color: Colors.black,
                    fontFamily: "Arabic",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(
                    _width * 0.2, _height * 0.02, _width * 0.2, 0),
              )
            ],
          ),
        ),
      ),
      PageViewModel(
        title: "",
        bodyWidget: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Image.asset(
                "assets/images/intro.png",
                fit: BoxFit.fitWidth,
              ),
              Padding(
                  child: Text(
                    "نقدم افضل الاسعار",
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(15.0),
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Arabic",
                    ),
                  ),
                  padding: EdgeInsets.fromLTRB(
                      _width * 0.1, _height * 0.04, _width * 0.1, 0)),
              Padding(
                child: Text(
                  settingsModel != null
                      ? settingsModel.data.intro3En
                      : appLocalizations.Loading_Text,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(12.0),
                    color: Colors.black,
                    fontFamily: "Arabic",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(
                    _width * 0.2, _height * 0.02, _width * 0.2, 0),
              )
            ],
          ),
        ),
      )
    ];

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        new FallbackCupertinoLocalisationsDelegate(),
        //app-specific localization
        _specificLocalizationDelegate
      ],
      supportedLocales: [Locale('en'), Locale('ar')],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
        body: Stack(
          children: [
            IntroductionScreen(
              pages: listPagesViewModel,
              onDone: () {
                // When done button is press
                SharedPreferences.getInstance()
                    .then((value) => {value.setBool("firstLaunch", true)});

                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => HomeScreen()));
              },
//              dotsDecorator: DotsDecorator(activeColor: MyColors.appYellow),
              dotsDecorator: DotsDecorator(
                  activeColor: Colors.white.withOpacity(0),
                  color: Colors.white.withOpacity(0)),
              showSkipButton: true,
              skip: CircleAvatar(
                backgroundColor: MyColors.appYellow,
                child: Icon(
                  Icons.chevron_right,
                  color: Colors.white,
                ),
              ),
              done: _privacyCheck
                  ? Text(appLocalizations.Done,
                      style: TextStyle(
                          fontFamily: lan == "en" ? "Poppins" : "Arabic",
                          fontWeight: FontWeight.w600))
                  : Container(),
            ),
            Container(
              child: Align(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Checkbox(
                        value: _privacyCheck,
                        onChanged: (bool value) {
                          setState(() {
                            _privacyCheck = value;
                          });
                        }),
                    GestureDetector(
                      child: Text(
                        appLocalizations.Accept,
                        style: TextStyle(
                          color: Colors.blue,
                          fontSize: ScreenUtil().setSp(15.0),
                          fontFamily: "Arabic",
                        ),
                      ),
                      onTap: () {
                        showDialog(context: context,builder: (BuildContext context){
                          return Reusable.showPrivacy(context, settingsModel != null
                              ? settingsModel.data.privacyAr
                              : appLocalizations.Loading_Text,);
                        });
                      },
                    )
                  ],
                ),
                alignment: Alignment.bottomCenter,
              ),
              margin: EdgeInsets.only(bottom: _width * 0.05),
            ),
          ],
        ),
      ),
    );
  }

  void getSettings() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
    var response = await get(Urls.settings);
    setState(() {
      settingsModel = new SettingsModel.fromJson(json.decode(response.body));
      saveLinks(settingsModel);
    });
  }

  void saveLinks(SettingsModel settingsModel) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("facebook", settingsModel.data.facebook);
    prefs.setString("twitter", settingsModel.data.twitter);
    prefs.setString("instagram", settingsModel.data.instagram);
    prefs.setString("privacy", settingsModel.data.privacyAr);
  }
}
