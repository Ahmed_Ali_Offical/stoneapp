import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/lang/lang.dart';
import 'package:stone_app/model/SettingsModel.dart';
import 'package:stone_app/splash/introPages.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';

import '../LocaleHelper.dart';
import '../applocalizations.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SplashState();
  }
}

 String lan = "";

class SplashState extends State<SplashScreen> {

  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
    new Future.delayed(new Duration(seconds: 3), () {
      getFirstScreen();
    });
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan == null || lan.isEmpty ? "en":lan, ''));

    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      new FallbackCupertinoLocalisationsDelegate(),
      //app-specific localization
      _specificLocalizationDelegate
    ],
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      theme: ThemeData(primarySwatch: MyColors.primary),
      locale: _specificLocalizationDelegate.overriddenLocale,
      home: Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Image.asset(
              "assets/images/lang.png",
              fit: BoxFit.fill,
            ),
            Align(
              child: Container(child: Wrap(
                children: <Widget>[CircularProgressIndicator()],
              ),margin: EdgeInsets.all(_height*0.1),),
              alignment: Alignment.bottomCenter,
            ),
          ],
        ),
      ),
    ),);
  }

  void getFirstScreen() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getBool("firstLaunch") ?? false) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) { return HomeScreen();}));
    } else {
      if(lan == null || lan.isEmpty) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LangScreen()));
      }else{
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => IntroPages()));
      }
    }
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en");
    });
  }
}
