import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/itemDetailes/ItemDetails.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/model/singleCategoryModel.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class InnerProducts extends StatefulWidget {
  String title;
  String id;

  InnerProducts(this.title, this.id);

  @override
  State<StatefulWidget> createState() {
    return InnerProductsState();
  }
}

SingleCategoryModel sigleCate;

class InnerProductsState extends State<InnerProducts> {
  int cartItemsCount = 0;
  int pageSize = 6;
  bool empty = false;

  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    updateCartIcon();
//    getChilds(1, widget.id);
  }

  List<Inner> items = [];

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    return MaterialApp(localizationsDelegates: [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      new FallbackCupertinoLocalisationsDelegate(),
      //app-specific localization
      _specificLocalizationDelegate
    ],
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
      appBar: Reusable.getAppBar(
          context, _height, _width, cartItemsCount, widget.title),
      body:  !empty? Container(
          width: double.infinity,
          height: double.infinity,
          child:
          PagewiseGridView.count(
              pageSize: pageSize,
              crossAxisCount: 2,
              mainAxisSpacing: 8,
              crossAxisSpacing: 8,
              childAspectRatio: 0.7,
              addAutomaticKeepAlives: true,
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, IData entry, index) {
                return GestureDetector(
                  child: Card(
                    child: Wrap(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Container(
                              width: double.infinity,
                              height: _height * 0.2,
                              child: ClipRRect(
                                child: Image.network(
                                  entry.photo,
                                  height: double.infinity,
                                  fit: BoxFit.cover,
                                  cacheWidth: 500,
                                  cacheHeight: 500,
                                  loadingBuilder: (BuildContext context, Widget child,
                                      ImageChunkEvent loadingProgress) {
                                    if (loadingProgress == null) return child;
                                    return Center(
                                      child: CircularProgressIndicator(
                                        value: loadingProgress.expectedTotalBytes != null
                                            ? loadingProgress.cumulativeBytesLoaded /
                                            loadingProgress.expectedTotalBytes
                                            : null,
                                      ),
                                    );
                                  },
                                ),
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(8.0),
                                    topRight: Radius.circular(8.0)),
                              ),
                            ),
                            Reusable.flexibleText(
                                _height * 0.018,
                                lan == "en" ? entry.title.en:entry.title.ar,
                                EdgeInsets.all(_height * 0.01)),
                            Reusable.flexibleText(
                                _height * 0.018,
                                "${appLocalizations.Item_No} ${entry.itemNo}",
                                null),
                            Container(
                              child: Padding(
                                child: RaisedButton(
                                  color: MyColors.appYellow,
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    children: <Widget>[
                                      AutoSizeText(
                                        appLocalizations.AddToCart,
                                        style: TextStyle(
                                            fontSize: 14.0,
                                            color: Colors.white,
                                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none),
                                      ),
                                      Padding(
                                        child: Icon(
                                          Icons.shopping_cart,
                                          color: Colors.white,
                                        ),
                                        padding:
                                        EdgeInsets.all(_width * 0.001),
                                      )
                                    ],
                                  ),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(9.0),
                                        bottomRight: Radius.circular(9.0)),
                                  ),
                                  onPressed: () {
                                    var item = entry;
                                    CartModel model = new CartModel();
                                    items.add(Inner(
                                        item.id,
                                        item.photo,
                                        lan == "en" ? item.title.en:item.title.ar,
                                        lan == "en" ? item.content.en:item.content.ar,
                                        "1.0",
                                        "1.0",
                                        "1.0",
                                        item.price,
                                        1));
                                    model.inner = items;
                                    saveCartItem(jsonEncode(model.toJson()));
                                  },
                                ),
                                padding:
                                EdgeInsets.only(bottom: _height * 0.005),
                              ),
                              height: _height * 0.04,
                              margin: EdgeInsets.only(top: _height * 0.015),
                            )
                          ],
                        )
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(9.0)),
                    ),
                  ),
                  onTap: () {
                    var item = entry;
                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
                      return ItemDetailes(item.id);
                    }));
                  },
                );
              },
              pageFuture: (pageIndex) {
                print("Index ${pageIndex+1}");
                return getChilds(pageIndex+1, widget.id);
              }

          )
//        GridView.count(
//                crossAxisCount: 2,
//                crossAxisSpacing: 8,
//                mainAxisSpacing: 8,
//                childAspectRatio: 0.7,
//                shrinkWrap: true,
//                scrollDirection: Axis.vertical,
//                children: List.generate(widget.innerProducts.length, (index) {
//                  return GestureDetector(
//                    child: Card(
//                      child: Wrap(
//                        children: <Widget>[
//                          Column(
//                            crossAxisAlignment: CrossAxisAlignment.stretch,
//                            mainAxisSize: MainAxisSize.max,
//                            children: <Widget>[
//                              Container(
//                                width: double.infinity,
//                                height: _height * 0.2,
//                                child: ClipRRect(
//                                  child: Image.network(
//                                    widget.innerProducts[index].photo,
//                                    height: double.infinity,
//                                    fit: BoxFit.cover,
//                                    loadingBuilder: (BuildContext context, Widget child,
//                                        ImageChunkEvent loadingProgress) {
//                                      if (loadingProgress == null) return child;
//                                      return Center(
//                                        child: CircularProgressIndicator(
//                                          value: loadingProgress.expectedTotalBytes != null
//                                              ? loadingProgress.cumulativeBytesLoaded /
//                                              loadingProgress.expectedTotalBytes
//                                              : null,
//                                        ),
//                                      );
//                                    },
//                                  ),
//                                  borderRadius: BorderRadius.only(
//                                      topLeft: Radius.circular(8.0),
//                                      topRight: Radius.circular(8.0)),
//                                ),
//                              ),
//                              Reusable.flexibleText(
//                                  _height * 0.018,
//                                  widget.innerProducts[index].title.en,
//                                  EdgeInsets.all(_height * 0.01)),
//                              Reusable.flexibleText(
//                                  _height * 0.018,
//                                  "Item No : ${widget.innerProducts[index].itemNo}",
//                                  null),
//                              Container(
//                                child: Padding(
//                                  child: RaisedButton(
//                                    color: MyColors.appYellow,
//                                    child: Row(
//                                      mainAxisAlignment:
//                                          MainAxisAlignment.center,
//                                      children: <Widget>[
//                                        AutoSizeText(
//                                          "Add to Cart",
//                                          style: TextStyle(
//                                              fontSize: 14.0,
//                                              color: Colors.white,
//                                              fontFamily: lan == "en" ? "Poppins" : "Arabic",
//                                              fontWeight: FontWeight.bold,
//                                              decoration: TextDecoration.none),
//                                        ),
//                                        Padding(
//                                          child: Icon(
//                                            Icons.shopping_cart,
//                                            color: Colors.white,
//                                          ),
//                                          padding:
//                                              EdgeInsets.all(_width * 0.001),
//                                        )
//                                      ],
//                                    ),
//                                    shape: RoundedRectangleBorder(
//                                      borderRadius: BorderRadius.only(
//                                          bottomLeft: Radius.circular(9.0),
//                                          bottomRight: Radius.circular(9.0)),
//                                    ),
//                                    onPressed: () {
//                                      var item = widget.innerProducts[index];
//                                      CartModel model = new CartModel();
//                                      items.add(Inner(
//                                          item.id,
//                                          item.photo,
//                                          item.title.en,
//                                          item.content.en,
//                                          "1.0",
//                                          "1.0",
//                                          "1.0",
//                                          item.price,
//                                          1));
//                                      model.inner = items;
//                                      saveCartItem(jsonEncode(model.toJson()));
//                                    },
//                                  ),
//                                  padding:
//                                      EdgeInsets.only(bottom: _height * 0.005),
//                                ),
//                                height: _height * 0.04,
//                                margin: EdgeInsets.only(top: _height * 0.015),
//                              )
//                            ],
//                          )
//                        ],
//                      ),
//                      shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.all(Radius.circular(9.0)),
//                      ),
//                    ),
//                    onTap: () {
//                      var item = widget.innerProducts[index];
//                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
//                        return ItemDetailes(item.id);
//                      }));
//                    },
//                  );
//                }),
//              )
//            : Center(
//                child: Reusable.flexibleText(
//                    _height * 0.02, "No Products in this Category", null),
//              ),
        //margin: EdgeInsets.fromLTRB(
        //  _width * 0.01, _height * 0.001, _width * 0.01, 0),
      ) : Center(
                child: Reusable.flexibleText(
                    _height * 0.02, appLocalizations.No_Products, null),
              ),
    ),);
  }

  Future<List<IData>> getChilds(offset, id) async {
    var childsReques = await get("${Urls.categories}/${id}?page=$offset");
    print("Url: ${Urls.categories}/${id}?page=$offset");
    // The response body is an array of items
    sigleCate = SingleCategoryModel.fromJson(jsonDecode(childsReques.body));
    setState(() {
      if(sigleCate.data.products.to != null)
      pageSize = sigleCate.data.products.to;
      empty = sigleCate.data.products.data.isEmpty;
      print("Size $pageSize");
    });
    return sigleCate.data.products.data;
  }

  void updateCartIcon() async {
    var prefs = await SharedPreferences.getInstance();
    CartModel cartModel =
        new CartModel.fromJson(json.decode(prefs.getString("items")));
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        cartItemsCount = cartModel.inner.length;
      });
  }

  void saveCartItem(String cartItems) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("items", cartItems);
    Toast.show(appLocalizations.Added, context, duration: Toast.LENGTH_LONG);
    updateCartIcon();
  }
}
