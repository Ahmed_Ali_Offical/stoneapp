import 'dart:convert';
import 'dart:io';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/citiesModel.dart';
import 'package:stone_app/model/countriesModel.dart';
import 'package:stone_app/model/user.dart';
import 'package:stone_app/payment/paymentScreen.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class AddAddress extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddAddressState();
  }
}

class AddAddressState extends State<AddAddress> {
  String name;
  String address = "";
  String phoneNum = "";
  CountriesModel countriesModel;
  Data countryValue;
  User user;
  CitiesModel citiesModel;

  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  Cities cityValue;
  bool _load = true;
  String choosenAddress;
  double lat,lang;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
    getCountries();
  }

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    return MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          new FallbackCupertinoLocalisationsDelegate(),
          //app-specific localization
          _specificLocalizationDelegate
        ],
        supportedLocales: [Locale('en'), Locale('ar')],
        locale: _specificLocalizationDelegate.overriddenLocale,
        theme: ThemeData(primarySwatch: MyColors.primary),
        home:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: new IconThemeData(color: Colors.black),
          brightness: Platform.isAndroid? Brightness.dark:Brightness.light,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Center(
            child: Text(
              appLocalizations.Add_Address,
              style: TextStyle(color: Colors.black, fontFamily: lan == "en" ? "Poppins" : "Arabic",),
            ),
          ),
        ),
        body: countriesModel != null ? Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(child: Card(
              child: Column(
                children: [
                  Padding(
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<Data>(
                        isExpanded: true,
                        value: countryValue,
                        elevation: 16,
                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black,
                          fontFamily: lan == "en" ? "Poppins" : "Arabic",
                        ),
                        onChanged: (Data value) {
                          setState(() {
                            countryValue = value;
                          });
                          getCities(value.id);
                        },
                        items: countriesModel.data
                            .map<DropdownMenuItem<Data>>((Data e) {
                          return DropdownMenuItem<Data>(
                            value: e,
                            child: Container(
                              height: _height * 0.03,
                              width: 90.0,
                              child: AutoSizeText(lan == "en" ? e.name.en:e.name.ar,
                                  maxFontSize: 12.0,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      color: Colors.black,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      decoration: TextDecoration.none)),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                    padding: EdgeInsets.only(
                        top: _height * 0.01,
                        left: _width * 0.05,
                        right: _width * 0.05),
                  ),
                  if (cityValue != null)
                    Padding(
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<Cities>(
                          isExpanded: true,
                          value: cityValue,
                          elevation: 16,
                          style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.black,
                            fontFamily: lan == "en" ? "Poppins" : "Arabic",
                          ),
                          onChanged: (Cities value) {
                            setState(() {
                              cityValue = value;
                            });
                          },
                          items: citiesModel.data.cities
                              .map<DropdownMenuItem<Cities>>((Cities e) {
                            return DropdownMenuItem<Cities>(
                              value: e,
                              child: Container(
                                height: _height * 0.03,
                                width: 90.0,
                                child: AutoSizeText(lan == "en" ? e.name.en:e.name.ar,
                                    maxFontSize: 12.0,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.black,
                                        fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                        decoration: TextDecoration.none)),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                      padding: EdgeInsets.only(
                          top: _height * 0.01,
                          left: _width * 0.05,
                          right: _width * 0.05),
                    )
                  else
                    Container(),
                  GestureDetector(child: Reusable.flexibleText(_height*0.02, appLocalizations.Choose_Address, EdgeInsets.all(8.0)),onTap: pickAddress,),
                  if(choosenAddress != null) Reusable.flexibleText(_height*0.02, choosenAddress, EdgeInsets.all(8.0)),
                  Container(
                    child: TextField(
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      textAlign: TextAlign.start,

                      decoration: InputDecoration(
                        hintText: appLocalizations.Address,
                        labelText: choosenAddress,
                      ),
                      onChanged: (value) {
                        if(value != null){
                          address = value;
                        }else{
                          Toast.show(appLocalizations.Address_Required, context,duration: Toast.LENGTH_LONG);
                        }
                      },
                    ),
                    margin: EdgeInsets.only(
                        top: _height * 0.01,
                        left: _width * 0.05,
                        right: _width * 0.05),
                  ),Container(
                    child: TextField(
                      keyboardType: TextInputType.phone,
                      maxLines: 1,
                      textAlign: TextAlign.start,
                      decoration: InputDecoration(
                        hintText: appLocalizations.Phone,
                      ),
                      onChanged: (value) {
                        phoneNum = value;
                      },
                    ),
                    margin: EdgeInsets.only(
                        top: _height * 0.01,
                        left: _width * 0.05,
                        right: _width * 0.05),
                  )
                ],
              ),
            ),flex: 1,),
            GestureDetector(
              child: Container(
                color: MyColors.appYellow,
                height: _height * 0.06,
                child: Center(
                  child: AutoSizeText(
                    appLocalizations.Complete_Purchase,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.white,
                        fontFamily: lan == "en" ? "Poppins" : "Arabic",
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none),
                  ),
                ),
                width: double.infinity,
              ),
              onTap: addAddress,
            ),
          ],
        ) : Reusable.showLoader(_load)));
  }

  void getCountries() async {
    var countries = await get(Urls.countries);
    countriesModel = CountriesModel.fromJson(json.decode(countries.body));
    getCities(countriesModel.data[0].id);
    setState(() {
      _load = false;
      countryValue = countriesModel.data[0];
      countriesModel;
    });
  }

  void getCities(int id) async {
    var cities = await get("${Urls.countries}/$id");
    citiesModel = new CitiesModel.fromJson(json.decode(cities.body));
    setState(() {
      cityValue = null;
      if (citiesModel.data.cities.isNotEmpty)
        cityValue = citiesModel.data.cities[0];
      citiesModel;
    });
  }

  void addAddress() async{
    if(address == "Undefiend" || address.isEmpty){
      Toast.show(appLocalizations.address_msg, context,duration: Toast.LENGTH_LONG);
    }else {
      var addAddress = await post(
          Urls.addAddress, headers: {'Authorization': "Bearer ${user.token}"}
          ,
          body: {
            'additional_phone': phoneNum,
            'address': address,
            'lat': lat.toString(),
            'lng': lang.toString(),
            'city_id': cityValue.id.toString(),
            'country_id': countryValue.id.toString()
          });
      print("Bearer ${user.token}");
      print("body ${addAddress.body}");
      print("Params $phoneNum - ${lat.toString()} - ");
      if (json.decode(addAddress.body)['success']) {
        Navigator.push(
            context, MaterialPageRoute(builder: (BuildContext context) {
          return PaymentScreen(
              cityValue.id, json.decode(addAddress.body)['data']['id']);
        }));
      }
    }
  }

  void pickAddress() async {
    LocationResult result = await showLocationPicker(context, "AIzaSyBeRZ6ReAs_VrBM-_QHil-qC5__ol944lw");
    setState(() {
      choosenAddress = result.address ?? "Undefiend";
      lat = result.latLng.latitude;
      lang = result.latLng.longitude;
    });
  }

  void getUser() async{
    var prefs = await SharedPreferences.getInstance();
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        user = User(
            prefs.getInt("id") ?? 0,
            prefs.getString("name") ?? "",
            prefs.getString("email") ?? "",
            prefs.getString("phone") ?? "",
            prefs.getString("level") ?? "",
            prefs.getString("token") ?? "",
            prefs.getString("image") ?? "");
      });
  }
}
