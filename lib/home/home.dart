import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/cart/cart.dart';
import 'package:stone_app/categories/Categories.dart';
import 'package:stone_app/companyEdit/CompanyEdit.dart';
import 'package:stone_app/contactus/ContactUs.dart';
import 'package:stone_app/home/fragments/login.dart';
import 'package:stone_app/home/fragments/productsfrag.dart';
import 'package:stone_app/inidividualEdit/indiEdit.dart';
import 'package:stone_app/itemDetailes/ItemDetails.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/model/draweritem.dart';
import 'package:stone_app/model/user.dart';
import 'package:stone_app/settings/Settings.dart';
import 'package:stone_app/singleCategory/SingleCategory.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/clickes.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:toast/toast.dart';

import '../LocaleHelper.dart';
import '../applocalizations.dart';
import 'fragments/CatalogCatagories.dart';
import 'fragments/history.dart';
import 'fragments/mainfrag.dart';

class HomeScreen extends StatefulWidget {
  var drawerItems = [];

  var loggeddrawerItems = [];

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeState();
  }
}

User user;
List<Inner> cartItems = [];

class HomeState extends State<HomeScreen> with WidgetsBindingObserver {
  int _selectedDrawerIndex = 0;
  int cartItemsCount = 0;
  String barTitle;

  var _height;
  var _width;

  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;

//  String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    super.initState();
    getUser();
    WidgetsBinding.instance.addObserver(this);
    updateCartIcon();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("StateLife ${state.toString()}");
    if (state == AppLifecycleState.resumed) {
      cartItems.clear();
      print("CartItems ${cartItems.length}");
      updateCartIcon();
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    _getDrawerItemWidget(int pos) {
      if (user != null && user.id != 0) {
        switch (pos) {
          case 0:
            return new MainFragment((val, cartItem) {
              setState(() {
                if (val == Clicks.SingleProduct) {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return ItemDetailes(cartItem.id);
                  })).then((val) {
                    updateCartIcon();
                  });
                } else if (val == Clicks.SingleCategory) {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return SingleCategory(
                        cartItem.name, cartItem.id.toString());
                  })).then((val) {
                    updateCartIcon();
                  });
                } else if (val == Clicks.MainScreen) {
                  barTitle = appLocalizations.World_Stones;
                  _selectedDrawerIndex = 0;
                } else if (val == Clicks.MoreCategories) {
                  barTitle = appLocalizations.Categories;
                  _selectedDrawerIndex = 4;
                } else if (user == null && user.id == 0) {
                  barTitle = val == 3
                      ? appLocalizations.Products
                      : appLocalizations.Login;
                  _selectedDrawerIndex = val;
                } else if (val == Clicks.MoreProducts) {
                  barTitle = appLocalizations.Products;
                  _selectedDrawerIndex = 3;
                } else if (user != null &&
                    user.id != 0 &&
                    val == Clicks.AddToCart) {
                  print("CartItems : $cartItem");
                  CartModel model = new CartModel();
                  cartItems.add(cartItem);
                  model.inner = cartItems;
                  print("CartItems : ${jsonEncode(model.toJson())}");
                  saveCartItems(jsonEncode(model.toJson()));
                }
              });
            });
          case 1:
            return new HistoryFragment();
          case 2:
            return new Cart();
          case 3:
            return ProductsFrag((val, item) {
              if (val == Clicks.AddToCart) {
                CartModel model = new CartModel();
                cartItems.add(item);
                model.inner = cartItems;
                print("CartItems : ${jsonEncode(model.toJson())}");
                saveCartItems(jsonEncode(model.toJson()));
              }
            });
          case 4:
            return Categories();
          case 5:
            return CatalogCatagories();
          case 6:
            return new ContactUs((val) {
              setState(() {
                barTitle = appLocalizations.Contact_Us;
              });
            });
          case 7:
            return new SettingsScreen((val) {
              setState(() {
                barTitle = "Settings";
              });
            });
            break;
          case 9:
            clearUser();
            break;
          default:
            return new Text("Error");
        }
      } else {
        switch (pos) {
          case 0:
            return new MainFragment((val, cartItem) {
              setState(() {
                if (val == Clicks.SingleProduct) {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return ItemDetailes(cartItem.id);
                  })).then((val) {
                    updateCartIcon();
                  });
                } else if (val == Clicks.SingleCategory) {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return SingleCategory(
                        cartItem.name, cartItem.id.toString());
                  })).then((val) {
                    updateCartIcon();
                  });
                } else if (val == Clicks.MoreCategories) {
                  _selectedDrawerIndex = 2;
                  barTitle = appLocalizations.Categories;
                } else if (val == Clicks.MainScreen) {
                  _selectedDrawerIndex = 0;
                  barTitle = appLocalizations.World_Stones;
                } else if (user == null && user.id == 0) {
                  print("ShowDialog : $cartItem");
                  _selectedDrawerIndex = val;
                  barTitle = val == 3
                      ? appLocalizations.Products
                      : appLocalizations.Login;
                } else if (val == Clicks.MoreProducts) {
                  _selectedDrawerIndex = 3;
                  barTitle = appLocalizations.Products;
                } else if (val == Clicks.AddToCart) {
                  print("CartItems : $cartItem");
                  createLoginDialog();
                }
              });
            });
          case 2:
            return new Categories();
          case 3:
            return CatalogCatagories();
          case 4:
            return new ContactUs((val) {
              setState(() {
                barTitle = appLocalizations.Contact_Us;
              });
            });
          case 6:
            return LoginScreen((value) {
              setState(() {
                if (value == Clicks.MainScreen) {
                  _selectedDrawerIndex = 0;
                  barTitle = appLocalizations.World_Stones;
                }
              });
            });
            break;
          default:
            return new Text("Error");
        }
      }
    }

    _onSelectItem(int index) {
       setState(() {
         if(index == 0)
         barTitle = appLocalizations.World_Stones;
         _selectedDrawerIndex = index;
       });
      Navigator.of(context).pop(); // close the drawer
    }

    widget.drawerItems = [
      new DrawerItem(appLocalizations.Home, Icons.home),
      new DrawerItem("d", Icons.account_circle),
      new DrawerItem(appLocalizations.Categories, Icons.dashboard),
      new DrawerItem(appLocalizations.Catalogs, Icons.content_copy),
      new DrawerItem(appLocalizations.Contact_Us, Icons.call),
      new DrawerItem("d", Icons.account_circle),
      new DrawerItem(appLocalizations.Login, Icons.exit_to_app),
    ];
    widget.loggeddrawerItems = [
      new DrawerItem(appLocalizations.Home, Icons.home),
      new DrawerItem(appLocalizations.History, Icons.history),
      new DrawerItem(appLocalizations.Cart, Icons.shopping_cart),
      new DrawerItem("d", Icons.account_circle),
      new DrawerItem(appLocalizations.Categories, Icons.dashboard),
      new DrawerItem(appLocalizations.Catalogs, Icons.content_copy),
      new DrawerItem(appLocalizations.Contact_Us, Icons.call),
      new DrawerItem(appLocalizations.Settings, Icons.settings),
      new DrawerItem("d", Icons.account_circle),
      new DrawerItem(appLocalizations.Signout, Icons.exit_to_app),
    ];

    this._width = _width;
    this._height = _height;

    List<Widget> drawerOptions = [];
    if (user != null && user.id != 0) {
      drawerOptions.clear();
      for (var i = 0; i < widget.loggeddrawerItems.length; i++) {
        var d = widget.loggeddrawerItems[i];
        if (d.title == "d") {
          drawerOptions.add(Container(
            child: new Divider(
              color: Colors.white,
            ),
            margin: EdgeInsets.only(left: 8.0, right: 8.0),
          ));
        } else {
          drawerOptions.add(new ListTile(
            leading: new Icon(
              d.icon,
              color: Colors.white,
            ),
            title: new Text(
              d.title ?? "",
              style: TextStyle(
                color: Colors.white,
                fontFamily: lan == "en" ? "Poppins" : "Arabic",
              ),
            ),
            selected: i == _selectedDrawerIndex,
            onTap: () => _onSelectItem(i),
          ));
        }
      }
    }
    else {
      drawerOptions.clear();
      for (var i = 0; i < widget.drawerItems.length; i++) {
        var d = widget.drawerItems[i];
        if (d.title == "d") {
          drawerOptions.add(Container(
            child: new Divider(
              color: Colors.white,
            ),
            margin: EdgeInsets.only(left: 8.0, right: 8.0),
          ));
        } else {
          drawerOptions.add(new ListTile(
            leading: new Icon(
              d.icon,
              color: Colors.white,
            ),
            title: new Text(
              d.title ?? "",
              style: TextStyle(
                color: Colors.white,
                fontFamily: lan == "en" ? "Poppins" : "Arabic",
              ),
            ),
            selected: i == _selectedDrawerIndex,
            onTap: () => _onSelectItem(i),
          ));
        }
      }
    }

    // TODO: implement build
    return Directionality(
      textDirection: lan == "ar" ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
            brightness: Platform.isAndroid? Brightness.dark:Brightness.light,
            backgroundColor: Colors.white,
            iconTheme: new IconThemeData(color: Colors.black),
            actions: <Widget>[
              GestureDetector(
                child: Center(
                  child: Container(
                    child: new Stack(
                      children: <Widget>[
                        Icon(
                          Icons.shopping_cart,
                          color: MyColors.appGray,
                        ),
                        Container(
                          width: _width * 0.035,
                          height: _width * 0.035,
                          child: FittedBox(
                            child: AutoSizeText(
                              cartItemsCount.toString(),
                            ),
                            fit: BoxFit.fitHeight,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(50),
                            ),
                            color: Colors.brown,
                          ),
                        )
                      ],
                    ),
                    margin: EdgeInsets.only(
                        left: _width * 0.05, right: _width * 0.05),
                  ),
                ),
                onTap: () {
                  Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Cart()))
                      .then((val) {
                    if (cartItemsCount > 0) updateCartIcon();
                  });
                },
              )
            ],
            title: Center(
              child: Reusable.flexibleText(_height * 0.025,
                  barTitle ?? appLocalizations.World_Stones, null),
            )),
        body: _getDrawerItemWidget(_selectedDrawerIndex),
        drawer: Drawer(
          child: Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  if (user != null && user.id != 0)
                    GestureDetector(
                      child: UserAccountsDrawerHeader(
                        accountEmail: Text(user.email ?? ""),
                        accountName: Text(user.name ?? ""),
                        currentAccountPicture: CircleAvatar(
                          backgroundColor: Colors.indigo,
                          backgroundImage: user.image != null
                              ? NetworkImage(user.image)
                              : AssetImage("assets/images/person.png"),
                        ),
                      ),
                      onTap: () {
                        user.level == "company"
                            ? Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => CompanyEdit()))
                                .then((value) => getUser())
                            : Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => IndiEdit()))
                                .then((value) => getUser());
                      },
                    )
                  else
                    Container(
                      margin: EdgeInsets.only(top: _height * 0.08),
                    ),
                  Column(
                    children: drawerOptions,
                  )
                ],
              ),
            ),
            color: MyColors.primary,
          ),
        ),
      ),
    );
  }

  void createLoginDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            Reusable.loginDialog(context,appLocalizations, _width, _height, (value) {
              setState(() {
                barTitle = appLocalizations.Login;
                _selectedDrawerIndex = 6;
              });
            }));
  }

  void getUser() async {
    var prefs = await SharedPreferences.getInstance();
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        user = User(
                prefs.getInt("id") ?? 0,
                prefs.getString("name") ?? "",
                prefs.getString("email") ?? "",
                prefs.getString("phone") ?? "",
                prefs.getString("level") ?? "",
                prefs.getString("token") ?? "",
                prefs.getString("image")) ??
            "";
        print(user.token);
      });
  }

  void saveCartItems(String cartItems) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("items", cartItems);
    Toast.show(appLocalizations.Added, context, duration: Toast.LENGTH_LONG);
    updateCartIcon();
  }

  void updateCartIcon() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString("items") != null) {
      CartModel cartModel =
          new CartModel.fromJson(json.decode(prefs.getString("items")));
      print("ItemsHome ${prefs.getString("items")}");
      if (mounted)
        setState(() {
          cartItemsCount = cartModel.inner.length;
        });
    } else {
      setState(() {
        cartItemsCount = 0;
      });
    }
    print("CartItems $cartItemsCount");
  }

  void clearUser() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.clear();
    prefs.setString('en', lan);
    setState(() {
      user = null;
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (BuildContext context) {
        return HomeScreen();
      })).then((val) {
        updateCartIcon();
      });
    });
  }
}
