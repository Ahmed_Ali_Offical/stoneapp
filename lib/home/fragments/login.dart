import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/forget/ForgetPass.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/register/mainRegister.dart';
import 'package:stone_app/splash/splash.dart';
import 'package:stone_app/values/clickes.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';
import 'package:stone_app/virification/validation.dart';

import '../../LocaleHelper.dart';
import '../../applocalizations.dart';

class LoginScreen extends StatefulWidget {
  var callback = Reusable.loginCallBack;

  LoginScreen(this.callback);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginState();
  }
}

class LoginState extends State<LoginScreen> {
  String email = "";
  String password = "";
  double screenWidth;
  double screenheight;
  var loading = false;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;

  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    screenWidth = _width;
    screenheight = _height;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    // TODO: implement build
    return GestureDetector(child:  WillPopScope(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Image.asset(
            'assets/images/user.png',
            width: _width * 0.3,
            height: _width * 0.3,
          ),
          Container(
            child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(
                fontFamily: lan == "en" ? "Poppins" : "Arabic",
              ),
              decoration: InputDecoration(
                  labelText: appLocalizations.Email,
                  hintText: appLocalizations.Email_Enter,
                  border: InputBorder.none,
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0)),
                    borderSide:
                    const BorderSide(color: MyColors.appYellow, width: 0.0),
                  ),
                  prefixIcon: Icon(
                    Icons.email,
                    color: MyColors.appYellow,
                  ),
                  suffixIcon: Icon(
                    Icons.edit,
                    color: MyColors.appYellow,
                  )),
              onChanged: (value) {
                email = value;
              },
            ),
            margin: EdgeInsets.only(
                top: _height * 0.08, left: _width * 0.08, right: _width * 0.08),
          ),
          Container(
            child: TextFormField(
              keyboardType: TextInputType.text,
              obscureText: true,
              style: TextStyle(
                fontFamily: lan == "en" ? "Poppins" : "Arabic",
              ),
              decoration: InputDecoration(
                  labelText: appLocalizations.Password,
                  hintText: appLocalizations.Password_Enter,
                  border: InputBorder.none,
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16.0)),
                    borderSide:
                    const BorderSide(color: MyColors.appYellow, width: 0.0),
                  ),
                  prefixIcon: Icon(
                    Icons.lock_open,
                    color: MyColors.appYellow,
                  ),
                  suffixIcon: Icon(
                    Icons.edit,
                    color: MyColors.appYellow,
                  )),
              onChanged: (value) {
                password = value;
              },
            ),
            margin: EdgeInsets.only(
                left: _width * 0.08, right: _width * 0.08, top: _height * 0.03),
          ),
          loading
              ? Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
            margin: EdgeInsets.all(8.0),
          )
              : GestureDetector(
            child: Container(
              width: double.infinity,
              height: _height * 0.07,
              margin: EdgeInsets.only(
                  left: _width * 0.08,
                  right: _width * 0.08,
                  top: _height * 0.03),
              decoration: BoxDecoration(
                  color: MyColors.appYellow,
                  borderRadius: BorderRadius.all(Radius.circular(16.0))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    child: AutoSizeText(appLocalizations.Login,
                        textAlign: TextAlign.center,
                        maxFontSize: 16.0,
                        style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                            fontFamily:
                            lan == "en" ? "Poppins" : "Arabic",
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.none)),
                    padding: EdgeInsets.only(left: 3.0, right: 3.0),
                  ),
                  Icon(
                    Icons.exit_to_app,
                    color: Colors.white,
                  )
                ],
              ),
            ),
            onTap: _login,
          ),
          GestureDetector(
            child: Center(
                child: Container(
                  child: AutoSizeText(
                    appLocalizations.No_Account,
                    style: TextStyle(
                        fontSize: 13.0,
                        fontFamily: lan == "en" ? "Poppins" : "Arabic",
                        decoration: TextDecoration.none),
                  ),
                  margin: EdgeInsets.all(8.0),
                )),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RegisterScreen()));
            },
          ),
          GestureDetector(
            child: Center(
                child: Container(
                  child: AutoSizeText(
                    appLocalizations.No_PASS,
                    style: TextStyle(
                        fontSize: 13.0,
                        fontFamily: lan == "en" ? "Poppins" : "Arabic",
                        decoration: TextDecoration.none),
                  ),
                  margin: EdgeInsets.all(8.0),
                )),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ForgetPass()));
            },
          )
        ],
      ),
      onWillPop: _onBackPressed,
    ),onTap: (){
      FocusScope.of(context).requestFocus(new FocusNode());
    });
  }

  Future<bool> _onBackPressed() {
    return widget.callback(Clicks.MainScreen);
  }

  void _login() async {
    print("email $email");
    setState(() {
      loading = true;
    });
    var loginRequst =
        await post(Urls.login, body: {'email': email, 'password': password});
    setState(() {
      loading = false;
    });
    var user = json.decode(loginRequst.body)['user'];
    if (user != null) {
      var prefs = await SharedPreferences.getInstance();
      prefs.setInt("id", user['id']);
      prefs.setString("name", user['name']);
      prefs.setString("email", user['email']);
      prefs.setString("phone", user['phone']);
      prefs.setString("level", user['level']);
      prefs.setString("token", json.decode(loginRequst.body)['token']);
      prefs.setString("image", user['avatar']);
      if (user['phone_verified_at'] == null) {
        Navigator.push(
            this.context,
            MaterialPageRoute(
                builder: (context) =>
                    VerificationScreen(user['phone'], user['id'])));
      } else {
        Navigator.pushReplacement(this.context,
            MaterialPageRoute(builder: (context) => HomeScreen()));
      }
    } else {
      List<String> error = [json.decode(loginRequst.body)['error']];
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return Reusable.errorDialog(
                context, screenWidth, screenheight, error);
          });
    }
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }
}
