import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/model/productmodel.dart';
import 'package:stone_app/values/clickes.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';

import '../../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../../applocalizations.dart';

class ProductsFrag extends StatefulWidget {
  var callback = Reusable.cartCallBack;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProductsState();
  }

  ProductsFrag(this.callback);
}

List<ProductModel> productsModel = [];

class ProductsState extends State<ProductsFrag> {
  bool _load = false;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLan();
    if (productsModel.isEmpty) {
      _load = true;
      fetchProducts();
    }
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));

    // TODO: implement build
    return WillPopScope(
        child: MaterialApp(
          debugShowCheckedModeBanner: false,

          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            new FallbackCupertinoLocalisationsDelegate(),
            //app-specific localization
            _specificLocalizationDelegate
          ],
          supportedLocales: [Locale('en'), Locale('ar')],
          locale: _specificLocalizationDelegate.overriddenLocale,
          theme: ThemeData(primarySwatch: MyColors.primary),
          home: Scaffold(
            body: Stack(
              children: <Widget>[
                getProducts(_height, _width, productsModel, widget.callback),
                Reusable.showLoader(_load)
              ],
            ),
          ),
        ),
        onWillPop: () {
          return Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return HomeScreen();
          }));
        });
  }

  void fetchProducts() async {
    var request = await get(Urls.products);
    bool status = json.decode(request.body)['success'];
    print(request.body);
    if (status) {
      List<dynamic> data = json.decode(request.body)['data'];
      for (var i in data) {
        productsModel.add(new ProductModel.fromJson(i));
      }
      if (mounted) {
        setState(() {
          _load = false;
        });
      }
    }
  }

  Widget getProducts(_height, _width, List<ProductModel> productsModel,
      Function(int, Inner) callBack) {
    return Container(
      width: double.infinity,
      child: GridView.count(
        crossAxisCount: 2,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
        childAspectRatio: 0.7,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        children: List.generate(productsModel.length, (index) {
          return GestureDetector(
            child: Card(
              child: Wrap(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: _height * 0.2,
                        child: ClipRRect(
                          child: Image.network(
                            productsModel[index].photo,
                            height: double.infinity,
                            fit: BoxFit.cover,
                            loadingBuilder: (BuildContext context, Widget child,
                                ImageChunkEvent loadingProgress) {
                              if (loadingProgress == null) return child;
                              return Center(
                                child: CircularProgressIndicator(
                                  value: loadingProgress.expectedTotalBytes !=
                                      null
                                      ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes
                                      : null,
                                ),
                              );
                            },
                          ),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0)),
                        ),
                      ),
                      Reusable.flexibleText(
                          _height * 0.018,
                          lan == "en" ? productsModel[index].title.en:productsModel[index].title.ar,
                          EdgeInsets.all(_height * 0.01)),
                      Reusable.flexibleText(_height * 0.018,
                          "${appLocalizations.id} ${productsModel[index].itemNo}", null),
                      Container(
                        child: Padding(
                          child: RaisedButton(
                            color: MyColors.appYellow,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                AutoSizeText(
                                  appLocalizations.AddToCart,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.white,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.none),
                                ),
                                Padding(
                                  child: Icon(
                                    Icons.shopping_cart,
                                    color: Colors.white,
                                  ),
                                  padding: EdgeInsets.all(_width * 0.001),
                                )
                              ],
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(9.0),
                                  bottomRight: Radius.circular(9.0)),
                            ),
                            onPressed: () {
                              var item = productsModel[index];
                              callBack(
                                  Clicks.AddToCart,
                                  Inner(
                                      item.id,
                                      item.photo,
                                      lan == "en" ? item.title.en:item.title.ar,
                                      lan == "en" ? item.content.en:item.content.ar,
                                      "1.0",
                                      "1.0",
                                      "1.0",
                                      item.price,
                                      1));
                            },
                          ),
                          padding: EdgeInsets.only(bottom: _height * 0.005),
                        ),
                        height: _height * 0.04,
                        margin: EdgeInsets.only(top: _height * 0.015),
                      )
                    ],
                  )
                ],
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(9.0)),
              ),
            ),
            onTap: () {
              var item = productsModel[index];
              callBack.call(
                  14,
                  Inner(item.id, item.photo, lan == "en" ? item.title.en:item.title.ar,
                      lan == "en" ? item.content.en:item.content.ar,
                      "1.0", "1.0", "1.0", item.price, 1));
            },
          );
        }),
      ),
      margin:
      EdgeInsets.fromLTRB(_width * 0.01, _height * 0.001, _width * 0.01, 0),
    );
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }
}


