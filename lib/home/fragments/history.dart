import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/model/historyModel.dart';
import 'package:stone_app/model/user.dart';
import 'package:stone_app/reciet/ReceitScreen.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';

import '../../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../../applocalizations.dart';
import '../home.dart';

class HistoryFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HistoryState();
  }
}

class HistoryState extends State<HistoryFragment> {
  HistoryModel historyModel;
  User user;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";
  bool _load = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
  }

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));
    ScreenUtil.init(context, width: _width, height: _height);
    return WillPopScope(child: MaterialApp(
      debugShowCheckedModeBanner: false,

      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        new FallbackCupertinoLocalisationsDelegate(),
        //app-specific localization
        _specificLocalizationDelegate
      ],
      theme: ThemeData(primarySwatch: MyColors.primary),
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      locale: _specificLocalizationDelegate.overriddenLocale,
      home: Scaffold(
      body: historyModel != null
          ? Container(
        width: double.infinity,
        child: historyModel.orderHistory.isNotEmpty
            ? ListView.builder(
          itemCount: historyModel.orderHistory.length,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemBuilder: (item,int index){
            return GestureDetector(
              child: Wrap(
                children: <Widget>[
//                Text(historyModel.orderHistory[index].createdAt),
                  Card(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.only(left:8.0,right: 8.0,top: 8.0),
                                child: AutoSizeText(
                                  "${appLocalizations.Order_Number} ${historyModel.orderHistory[index].id}",
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(14.0),
                                      color: Colors.black,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      fontWeight: FontWeight.bold,
                                      decoration:
                                      TextDecoration.none),
                                ),
                              ),
                              flex: 1,
                            ),
                            Container(
                              height: _height * 0.02,
                              margin: EdgeInsets.only(left:8.0,right: 8.0,top: 8.0),
                              child: FittedBox(
                                child: AutoSizeText(
                                  "${historyModel.orderHistory[index].totalPrice} SR",
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(15.0),
                                      color: MyColors.appYellow,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      decoration:
                                      TextDecoration.none),
                                ),
                                fit: BoxFit.fitHeight,
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: _height * 0.02,
                                margin: EdgeInsets.all(8.0),
                                child: AutoSizeText(
                                  "${historyModel.orderHistory[index].createdAt}",
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(15.0),
                                      color: MyColors.appGray,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      decoration:
                                      TextDecoration.none),
                                ),
                              ),
                              flex: 1,
                            ),
                            Container(
                              height: _height * 0.02,
                              margin: EdgeInsets.all(8.0),
                              child: FittedBox(
                                child: AutoSizeText(
                                  "${historyModel.orderHistory[index].orderStatus}",
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(15.0),
                                      color: MyColors.appGray,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      decoration:
                                      TextDecoration.none),
                                ),
                                fit: BoxFit.fitHeight,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) { return RecietScreen.history(historyModel.orderHistory[index]); }));
              },
            );
          },
        )
            : Center(
          child: Reusable.flexibleText(
              _height * 0.02, appLocalizations.No_Orders, EdgeInsets.all(8.0)),
        ),
        margin: EdgeInsets.fromLTRB(
            _width * 0.01, _height * 0.001, _width * 0.01, 0),
      )
          : Reusable.showLoader(_load),
    ),), onWillPop: (){
      return Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (BuildContext context) {
            return HomeScreen();
          }));
    });
  }

  void getUser() async {
    var prefs = await SharedPreferences.getInstance();
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        user = User(
                prefs.getInt("id") ?? 0,
                prefs.getString("name") ?? "",
                prefs.getString("email") ?? "",
                prefs.getString("phone") ?? "",
                prefs.getString("level") ?? "",
                prefs.getString("token") ?? "",
                prefs.getString("image")) ??
            "";
        getHistory();
        print("Token: ${user.token}");
      });
  }

  void getHistory() async {
    var historyRequest = await get(Urls.history,
        headers: {'Authorization': "Bearer ${user.token}"});
    historyModel = new HistoryModel.fromJson(jsonDecode(historyRequest.body));
    setState(() {
      _load = false;
      historyModel;
    });
  }
}
