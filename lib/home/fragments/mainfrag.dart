import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/model/productmodel.dart';
import 'package:stone_app/model/catemodel.dart';
import 'package:stone_app/values/clickes.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/slider.dart';
import 'package:stone_app/views/reusable.dart';

import '../../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../../applocalizations.dart';

class MainFragment extends StatefulWidget {
  var callBack = Reusable.cartCallBack;

  MainFragment(this.callBack);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MainState();
  }
}

List<String> sliderModel = [];
List<CategoryModel> cateModel = [];
List<ProductModel> productsModel = [];

class MainState extends State<MainFragment> {
  bool _load = false;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getLan();
    if (cateModel.isEmpty) {
      _load = true;
      fetchCategories();
      fetchProducts();
      fetchSlider();
    }else{
      cateModel.clear();
      fetchCategories();
    }
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));
    // TODO: implement build
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
              child: Container(
                child: Column(
                  children: <Widget>[
                    CarouselWithIndicator(sliderModel),
                    getTitle(_width, _height, appLocalizations.Categories, widget.callBack),
                    getCateList(_width, _height, cateModel, widget.callBack),
                    getTitle(_width, _height, appLocalizations.Products, widget.callBack),
                    getProducts(_height, _width, productsModel, widget.callBack)
                  ],
                ),
              )),
          Reusable.showLoader(_load)
        ],
      ),
    );
  }

  void fetchCategories() async {
    var request = await get(Urls.categories);
    bool status = json.decode(request.body)['success'];
    if (status) {
      List<dynamic> data = json.decode(request.body)['data'];
      for (var i in data) {
        cateModel
            .add(new CategoryModel(i['id'], i['image'], i['name'][lan] ?? ""));
      }
      if (mounted) {
        setState(() {
          _load = false;
        });
      }
    }
  }

  void fetchProducts() async {
    var request = await get(Urls.products);
    bool status = json.decode(request.body)['success'];
    if (status) {
      List<dynamic> data = json.decode(request.body)['data'];
      if (data.length > 10) {
        for (var i = 0; i <= 10; i++) {
          print("Products $i");
          productsModel.add(new ProductModel.fromJson(data[i]));
        }
      } else {
        for (var i = 0; i <= data.length; i++) {
          print("Products $i");
          productsModel.add(new ProductModel.fromJson(data[i]));
        }
      }
      if (mounted) {
        setState(() {
          _load = false;
        });
      }
    }
  }

  void fetchSlider() async {
    var request = await get(Urls.sliders);
    bool status = json.decode(request.body)['success'];
    if (status) {
      List<dynamic> data = json.decode(request.body)['data'];
      for (var i in data) {
        sliderModel.add(i['image']);
      }
      if (mounted) {
        setState(() {
          sliderModel;
        });
      }
    }
  }

  Widget getTitle(width, height, text, Function(int, Inner) callBack) {
    return Padding(
      padding: EdgeInsets.fromLTRB(width * 0.03, height * 0.02, width * 0.03, 0),
      child: GestureDetector(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: Align(
                child: Reusable.flexibleText(height * 0.02, text, null),
                alignment: lan == "en" ? Alignment.centerLeft:Alignment.centerRight,
              ),
              flex: 1,
            ),
            Container(
              height: height * 0.015,
              child: FittedBox(
                child: AutoSizeText(
                  appLocalizations.More,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                      fontSize: 15.0,
                      color: MyColors.appYellow,
                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none),
                ),
                fit: BoxFit.fitHeight,
              ),
            ),
            lan == "en" ?Icon(
              Icons.keyboard_arrow_right,
              color: Colors.brown,
            ):Icon(
              Icons.keyboard_arrow_left,
              color: Colors.brown,
            )
          ],
        ),
        onTap: () {
          if (text == appLocalizations.Products) {
            callBack(Clicks.MoreProducts, null);
          } else {
            callBack(Clicks.MoreCategories, null);
          }
        },
      ),
    );
  }

  Widget getCateList(
      _height, _width, cateModel, Function(int p1, Inner p2) callBack) {
    return Container(
        width: double.infinity,
        height: _height * 0.35,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: cateModel.length,
          shrinkWrap: true,
          itemBuilder: (context, int index) {
            return GestureDetector(
              child: Container(
                width: _width * 0.18,
                height: _height * 0.35,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8.0))),
                margin: EdgeInsets.all(_width * 0.01),
                child: Stack(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Image.network(
                        cateModel[index].img,
                        width: double.infinity,
                        height: double.infinity,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: _height * 0.06,
                      decoration: BoxDecoration(
                          color: MyColors.appGrayDark.withOpacity(0.9),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0))),
                      child: Center(
                        child: AutoSizeText(cateModel[index].title,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 15.0,
                                color: Colors.white,
                                fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.none)),
                      ),
                    )
                  ],
                ),
              ),
              onTap: () {
                print("Type ${Clicks.SingleCategory}");
                callBack.call(
                    Clicks.SingleCategory,
                    Inner(cateModel[index].id, "", cateModel[index].title, "",
                        "1.0", "1.0", "1.0", 0.0, 1));
              },
            );
          },
        ));
  }

  Widget getProducts(_height, _width, List<ProductModel> productsModel,
      Function(int, Inner) callBack) {
    return Container(
      width: double.infinity,
      child: GridView.count(
        crossAxisCount: 2,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
        childAspectRatio: 0.7,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        children: List.generate(productsModel.length, (index) {
          return GestureDetector(
            child: Card(
              child: Wrap(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: _height * 0.2,
                        child: ClipRRect(
                          child: Image.network(
                            productsModel[index].photo,
                            height: double.infinity,
                            loadingBuilder: (BuildContext context, Widget child,
                                ImageChunkEvent loadingProgress) {
                              if (loadingProgress == null) return child;
                              return Center(
                                child: CircularProgressIndicator(
                                  value: loadingProgress.expectedTotalBytes !=
                                      null
                                      ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes
                                      : null,
                                ),
                              );
                            },
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0)),
                        ),
                      ),
                      Reusable.flexibleText(
                          _height * 0.018,
                          lan == "en" ? productsModel[index].title.en : productsModel[index].title.ar,
                          EdgeInsets.all(_height * 0.01)),
                      Reusable.flexibleText(_height * 0.018,
                          "${appLocalizations.Item_No} ${productsModel[index].itemNo}", null),
                      Container(
                        child: Padding(
                          child: RaisedButton(
                            color: MyColors.appYellow,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                AutoSizeText(
                                  appLocalizations.AddToCart,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.white,
                                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.none),
                                ),
                                Padding(
                                  child: Icon(
                                    Icons.shopping_cart,
                                    color: Colors.white,
                                  ),
                                  padding: EdgeInsets.all(_width * 0.001),
                                )
                              ],
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(9.0),
                                  bottomRight: Radius.circular(9.0)),
                            ),
                            onPressed: () {
                              var item = productsModel[index];
                              print("Clicked");
                              callBack(
                                  Clicks.AddToCart,
                                  Inner(
                                      item.id,
                                      item.photo,
                                      lan == "en" ? item.title.en:item.title.ar,
                                      lan == "en" ? item.content.en : item.content.ar,
                                      "1.0",
                                      "1.0",
                                      "1.0",
                                      item.price,
                                      1));
                            },
                          ),
                          padding: EdgeInsets.only(bottom: _height * 0.005),
                        ),
                        height: _height * 0.04,
                        margin: EdgeInsets.only(top: _height * 0.015),
                      )
                    ],
                  )
                ],
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(9.0)),
              ),
            ),
            onTap: () {
              var item = productsModel[index];
              callBack.call(
                  Clicks.SingleProduct,
                  Inner(item.id, item.photo, lan == "en" ? item.title.en:item.title.ar , lan == "en" ? item.content.en:item.content.ar,
                      "1.0", "1.0", "1.0", item.price, 1));
            },
          );
        }),
      ),
      margin:
      EdgeInsets.fromLTRB(_width * 0.01, _height * 0.001, _width * 0.01, 0),
    );
  }

  void getLan() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      lan = prefs.getString("en") ?? "en";
    });
  }
}


