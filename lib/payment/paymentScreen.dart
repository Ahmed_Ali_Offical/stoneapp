import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stone_app/home/home.dart';
import 'package:stone_app/model/cartmodel.dart';
import 'package:stone_app/model/receiptModel.dart';
import 'package:stone_app/model/shipsModel.dart';
import 'package:stone_app/model/user.dart';
import 'package:stone_app/reciet/ReceitScreen.dart';
import 'package:stone_app/values/colors.dart';
import 'package:stone_app/values/urls.dart';
import 'package:stone_app/views/reusable.dart';

import '../LocaleHelper.dart';
import 'package:stone_app/splash/splash.dart';
import '../applocalizations.dart';

class PaymentScreen extends StatefulWidget {
  int cityId;
  int shipId;

  PaymentScreen(this.cityId, this.shipId);

  @override
  State<StatefulWidget> createState() {
    return PaymentState();
  }
}

enum paymentTypes { ON_DELIVERY, ONLINE }

class PaymentState extends State<PaymentScreen> {
  ShipsModel shipsModel;
  int singleShip = 0;
  paymentTypes paymentType = paymentTypes.ON_DELIVERY;
  bool _load = true;
  String _privacy = "";
  HashMap<String, String> params = new HashMap();
  User user;
  SpecificLocalizationDelegate _specificLocalizationDelegate;
  AppLocalizations appLocalizations;
  // String lan = "";

  onLocaleChange(Locale locale) {
    setState(() {
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }
  static const platform = const MethodChannel('flutter.native/helper');

  String _responseFromNativeCode;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    params.putIfAbsent('city_id', () => widget.cityId.toString());
    params.putIfAbsent('ship_id', () => widget.shipId.toString());
    getUser();
    getShips();
    getInnerModels();
  }

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    ScreenUtil.init(context,height: _height,width: _width);
    appLocalizations = AppLocalizations();
    helper.onLocaleChanged = onLocaleChange;
    _specificLocalizationDelegate =
        SpecificLocalizationDelegate(new Locale(lan, ''));
    return MaterialApp(localizationsDelegates: [
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      new FallbackCupertinoLocalisationsDelegate(),
      //app-specific localization
      _specificLocalizationDelegate
    ],
      supportedLocales: [
        Locale('en'),
        Locale('ar')
      ],
      locale: _specificLocalizationDelegate.overriddenLocale,
      theme: ThemeData(primarySwatch: MyColors.primary),
      home: Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.black),
        brightness: Platform.isAndroid? Brightness.dark:Brightness.light,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Center(
          child: Text(
            appLocalizations.Payment_Enter,
            style: TextStyle(color: Colors.black, fontFamily: lan == "en" ? "Poppins" : "Arabic",),
          ),
        ),
      ),
      body: _load
          ? Reusable.showLoader(_load)
          : Column(
        children: [
          Container(
            child: Card(
              child: Column(
                children: shipsModel.data
                    .map((e) => new RadioListTile<Data>(
                    value: e,
                    title: AutoSizeText(
                      e.shipPrices[0].price > 0 ? "${lan == "en" ? e.name.en:e.name.ar} \n ${e.shipPrices[0].price} SR" : "${lan == "en" ? e.name.en:e.name.ar}",
                      style: TextStyle(
                          fontFamily: "Arabic",
                          fontSize: 15.0,
                          color: Colors.black),
                      textAlign: TextAlign.end,
                    ),
                    groupValue: shipsModel.data[singleShip],
                    onChanged: (Data value) {
                      setState(() {
                        singleShip = shipsModel.data.indexOf(e);
                      });
                    }))
                    .toList(),
              ),
              margin: EdgeInsets.only(
                  left: _width * 0.02,
                  right: _width * 0.02,
                  top: _height * 0.01),
            ),
          ),
          Row(
            children: [
              Container(
                child: new RadioListTile<paymentTypes>(
                  title: AutoSizeText(
                    appLocalizations.On_Delivery,
                    style: TextStyle(
                        fontFamily: "Arabic",
                        fontSize: ScreenUtil().setSp(15.0),
                        color: Colors.black),
                    textAlign: TextAlign.end,
                  ),
                  dense: false,
                  value: paymentTypes.ON_DELIVERY,
                  onChanged: (paymentTypes value) {
                    setState(() {
                      paymentType = value;
                    });
                  },
                  groupValue: paymentType,
                ),
                width: _width * 0.4,
              ),
              Container(
                child: new RadioListTile<paymentTypes>(
                  title: AutoSizeText(
                    appLocalizations.Online,
                    style: TextStyle(
                        fontFamily: "Arabic",
                        fontSize: ScreenUtil().setSp(14.0),
                        color: Colors.black),
                    textAlign: TextAlign.end,
                  ),
                  value: paymentTypes.ONLINE,
                  dense: false,
                  onChanged: (paymentTypes value) {
                    setState(() {
//                            paymentType = value;
//                            callNativePayment();
                    });
                  },
                  groupValue: paymentType,
                ),
                width: _width * 0.6,
              ),
            ],
          ),
          Expanded(
            child: Container(),
            flex: 1,
          ),
          GestureDetector(child: Text(
            appLocalizations.Accept_Privacy,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 14.0,
                color: MyColors.primary,
                fontFamily: lan == "en" ? "Poppins" : "Arabic",
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.none),
          ),onTap: (){
            showDialog(context: context,builder: (BuildContext context){
              return Reusable.showPrivacy(context, _privacy);
            });
          },),
          GestureDetector(
            child: Container(
              color: MyColors.appYellow,
              height: _height * 0.06,
              child: Center(
                child: AutoSizeText(
                  appLocalizations.Finish,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.white,
                      fontFamily: lan == "en" ? "Poppins" : "Arabic",
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none),
                ),
              ),
              width: double.infinity,
            ),
            onTap: () {
              switch (paymentType) {
                case paymentTypes.ON_DELIVERY:
                  params.putIfAbsent("payment", () => "ondelivery");
                  break;
                case paymentTypes.ONLINE:

                  params.putIfAbsent("payment", () => "online");
                  break;
              }

              params.putIfAbsent("ship_company_id",
                      () => shipsModel.data[singleShip].id.toString());
              setState(() {
                _load = true;
              });
              createOrder();
            },
          ),
        ],
      ),
    ),);
  }

  void getShips() async {
    var shipsRequest =
        await post(Urls.ships, body: {'city_id': widget.cityId.toString()});
    shipsModel = ShipsModel.fromJson(json.decode(shipsRequest.body));
    print(shipsRequest.body);
    setState(() {
      shipsModel;
      _load = false;
    });
  }

  void getInnerModels() async {
    var prefs = await SharedPreferences.getInstance();
    CartModel cartModel =
        new CartModel.fromJson(json.decode(prefs.getString("items")));
    cartItems = cartModel.inner;

    for (var i = 0; i < cartModel.inner.length; i++) {
      params.putIfAbsent(
          "products[$i][product_id]", () => cartModel.inner[i].id.toString());
      params.putIfAbsent(
          "products[$i][qty]", () => cartModel.inner[i].area.toString());
    }

    setState(() {
      params;
    });
  }

  void getUser() async {
    var prefs = await SharedPreferences.getInstance();
    if (mounted)
      setState(() {
        lan = prefs.getString("en") ?? "en";
        _privacy = prefs.getString("privacy");
        user = User(
                prefs.getInt("id") ?? 0,
                prefs.getString("name") ?? "",
                prefs.getString("email") ?? "",
                prefs.getString("phone") ?? "",
                prefs.getString("level") ?? "",
                prefs.getString("token") ?? "",
                prefs.getString("image")) ??
            "";
      });
  }

  void createOrder() async {
    var orderRequest = await post(Urls.orders,
        headers: {'Authorization': "Bearer ${user.token}"}, body: params);
    print("Token ${user.token}");
    print("Response ${orderRequest.body}");
    ReceiptModel receiptModel =
        new ReceiptModel.fromJson(json.decode(orderRequest.body));
    setState(() {
      _load = false;
    });
    if (receiptModel.success) {
      clearCart();
      Navigator.push(context,
          MaterialPageRoute(builder: (BuildContext context) {
        return RecietScreen(receiptModel);
      }));
    }
  }

  void callNativePayment() async {
    String response = "";
    try {
      final String result = await  platform.invokeMethod('helloFromNativeCode');
      response = result;
    } on PlatformException catch (e) {
      response = "Failed to Invoke: '${e.message}'.";
    }
    setState(() {
      print(response);
      _responseFromNativeCode = response;
    });
  }

  void clearCart() async{
    var prefs = await SharedPreferences.getInstance();
    prefs.remove("items");
  }
}
